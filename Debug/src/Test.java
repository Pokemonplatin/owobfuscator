import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;

public class Test {

    public static void main(String[] args) throws Exception {

        JarFile file = new JarFile(new File("./dump/MetaClient (1).jar"));
        JarOutputStream out = new JarOutputStream(new java.io.FileOutputStream("./dump/MetaClient (1)-out.jar"));

        file.stream().forEach(it -> {
            String name = it.getName();
            try (InputStream jis = file.getInputStream(it)) {

                byte[] b = readBytes(jis);

                if (!name.contains("/")) {
                    out.putNextEntry(new ZipEntry(stringDecryptor(name) + ".class"));
                    out.write(fileDecryptor(b));
                    out.closeEntry();
                } else {
                    out.putNextEntry(new ZipEntry(name));
                    out.write(b);
                    out.closeEntry();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        file.close();
        out.close();
    }

    public static byte[] readBytes(InputStream is) throws IOException {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream();) {
            byte[] buffer = new byte[0xFFFF];

            for (int len; (len = is.read(buffer)) != -1; )
                os.write(buffer, 0, len);

            os.flush();

            return os.toByteArray();
        }
    }

    private static String stringDecryptor(String var1) {
        String var2 = new String();

        for (int var3 = 0; var3 < var1.length(); ++var3) {
            var2 = var2 + (char) (var1.charAt(var3) ^ 315);
        }

        return var2;
    }


    private static byte[] fileDecryptor(byte[] var1) {
        byte[] var2 = new byte[var1.length];

        for (int var3 = 0; var3 < var1.length; ++var3) {
            var2[var3] = (byte) (var1[var3] ^ 945);
        }

        return var2;
    }

}
