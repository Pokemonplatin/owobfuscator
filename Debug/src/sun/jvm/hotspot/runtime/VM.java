//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package sun.jvm.hotspot.runtime;

import java.io.BufferedInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import sun.jvm.hotspot.c1.Runtime1;
import sun.jvm.hotspot.code.CodeCache;
import sun.jvm.hotspot.code.VMRegImpl;
import sun.jvm.hotspot.debugger.Address;
import sun.jvm.hotspot.debugger.JVMDebugger;
import sun.jvm.hotspot.interpreter.Interpreter;
import sun.jvm.hotspot.memory.StringTable;
import sun.jvm.hotspot.memory.SymbolTable;
import sun.jvm.hotspot.memory.SystemDictionary;
import sun.jvm.hotspot.memory.Universe;
import sun.jvm.hotspot.oops.CIntField;
import sun.jvm.hotspot.oops.DefaultOopVisitor;
import sun.jvm.hotspot.oops.InstanceKlass;
import sun.jvm.hotspot.oops.ObjectHeap;
import sun.jvm.hotspot.oops.OopField;
import sun.jvm.hotspot.types.AddressField;
import sun.jvm.hotspot.types.CIntegerField;
import sun.jvm.hotspot.types.CIntegerType;
import sun.jvm.hotspot.types.Type;
import sun.jvm.hotspot.types.TypeDataBase;
import sun.jvm.hotspot.utilities.Assert;
import sun.jvm.hotspot.utilities.CStringUtilities;
import sun.jvm.hotspot.utilities.ObjectReader;
import sun.jvm.hotspot.utilities.PlatformInfo;
import sun.jvm.hotspot.utilities.ReversePtrs;

public class VM {
    private static VM soleInstance;
    private static List vmInitializedObservers = new ArrayList();
    private List vmResumedObservers = new ArrayList();
    private List vmSuspendedObservers = new ArrayList();
    private TypeDataBase db;
    private boolean isBigEndian;
    private JVMDebugger debugger;
    private long stackBias;
    private long logAddressSize;
    private Universe universe;
    private ObjectHeap heap;
    private SymbolTable symbols;
    private StringTable strings;
    private SystemDictionary dict;
    private Threads threads;
    private ObjectSynchronizer synchronizer;
    private JNIHandles handles;
    private Interpreter interpreter;
    private StubRoutines stubRoutines;
    private Bytes bytes;
    private boolean usingClientCompiler;
    private boolean usingServerCompiler;
    private boolean isLP64;
    private int bytesPerLong;
    private int bytesPerWord;
    private int objectAlignmentInBytes;
    private int minObjAlignmentInBytes;
    private int logMinObjAlignmentInBytes;
    private int heapWordSize;
    private int heapOopSize;
    private int klassPtrSize;
    private int oopSize;
    private CodeCache codeCache;
    private Runtime1 runtime1;
    private int invocationEntryBCI;
    private int invalidOSREntryBCI;
    private ReversePtrs revPtrs;
    private VMRegImpl vmregImpl;
    private int reserveForAllocationPrefetch;
    private Properties sysProps;
    private String vmRelease;
    private String vmInternalInfo;
    private VM.Flag[] commandLineFlags;
    private Map flagsMap;
    private static Type intxType;
    private static Type uintxType;
    private static CIntegerType boolType;
    private Boolean sharingEnabled;
    private Boolean compressedOopsEnabled;
    private Boolean compressedKlassPointersEnabled;
    private static final boolean disableDerivedPointerTableCheck;
    private static final Properties saProps = new Properties();

    private static void checkVMVersion(String vmRelease) {
        if (System.getProperty("sun.jvm.hotspot.runtime.VM.disableVersionCheck") == null) {
            String versionProp = "sun.jvm.hotspot.runtime.VM.saBuildVersion";
            String saVersion = saProps.getProperty(versionProp);
            if (saVersion == null) {
                throw new RuntimeException("Missing property " + versionProp);
            }

            String vmVersion = vmRelease.replaceAll("(-fastdebug)|(-debug)|(-jvmg)|(-optimized)|(-profiled)", "");
            if (saVersion.equals(vmVersion)) {
                return;
            }

            if (saVersion.indexOf(45) == saVersion.lastIndexOf(45) && vmVersion.indexOf(45) == vmVersion.lastIndexOf(45)) {
                //throw new VMVersionMismatchException(saVersion, vmRelease);
            }

            System.err.println("WARNING: Hotspot VM version " + vmRelease + " does not match with SA version " + saVersion + "." + " You may see unexpected results. ");
        } else {
            System.err.println("WARNING: You have disabled SA and VM version check. You may be using incompatible version of SA and you may see unexpected results.");
        }

    }

    private VM(TypeDataBase db, JVMDebugger debugger, boolean isBigEndian) {
        this.db = db;
        this.debugger = debugger;
        this.isBigEndian = isBigEndian;
        if (db.getAddressSize() == 4L) {
            this.logAddressSize = 2L;
        } else {
            if (db.getAddressSize() != 8L) {
                throw new RuntimeException("Address size " + db.getAddressSize() + " not yet supported");
            }

            this.logAddressSize = 3L;
        }

        Type type;
        try {
            type = db.lookupType("Abstract_VM_Version");
            Address releaseAddr = type.getAddressField("_s_vm_release").getValue();
            this.vmRelease = CStringUtilities.getString(releaseAddr);
            Address vmInternalInfoAddr = type.getAddressField("_s_internal_vm_info_string").getValue();
            this.vmInternalInfo = CStringUtilities.getString(vmInternalInfoAddr);
            CIntegerType intType = (CIntegerType)db.lookupType("int");
            CIntegerField reserveForAllocationPrefetchField = type.getCIntegerField("_reserve_for_allocation_prefetch");
            this.reserveForAllocationPrefetch = (int)reserveForAllocationPrefetchField.getCInteger(intType);
        } catch (Exception var9) {
            throw new RuntimeException("can't determine target's VM version : " + var9.getMessage());
        }

        checkVMVersion(this.vmRelease);
        this.stackBias = (long)db.lookupIntConstant("STACK_BIAS");
        this.invocationEntryBCI = db.lookupIntConstant("InvocationEntryBci");
        this.invalidOSREntryBCI = db.lookupIntConstant("InvalidOSREntryBci");
        type = db.lookupType("Method");
        if (type.getField("_from_compiled_entry", false, false) == null) {
            this.usingClientCompiler = false;
            this.usingServerCompiler = false;
        } else if (db.lookupType("Matcher", false) != null) {
            this.usingServerCompiler = true;
        } else {
            this.usingClientCompiler = true;
        }

        if (debugger != null) {
            this.isLP64 = debugger.getMachineDescription().isLP64();
        }

        this.bytesPerLong = db.lookupIntConstant("BytesPerLong");
        this.bytesPerWord = db.lookupIntConstant("BytesPerWord");
        this.heapWordSize = db.lookupIntConstant("HeapWordSize");
        this.oopSize = db.lookupIntConstant("oopSize");
        intxType = db.lookupType("intx");
        uintxType = db.lookupType("uintx");
        boolType = (CIntegerType)db.lookupType("bool");
        this.minObjAlignmentInBytes = this.getObjectAlignmentInBytes();
        if (this.minObjAlignmentInBytes == 8) {
            this.logMinObjAlignmentInBytes = 3;
        } else {
            if (this.minObjAlignmentInBytes != 16) {
                throw new RuntimeException("Object alignment " + this.minObjAlignmentInBytes + " not yet supported");
            }

            this.logMinObjAlignmentInBytes = 4;
        }

        if (this.isCompressedOopsEnabled()) {
            this.heapOopSize = (int)this.getIntSize();
        } else {
            this.heapOopSize = (int)this.getOopSize();
        }

        if (this.isCompressedKlassPointersEnabled()) {
            this.klassPtrSize = (int)this.getIntSize();
        } else {
            this.klassPtrSize = (int)this.getOopSize();
        }

    }

    public static void initialize(TypeDataBase db, boolean isBigEndian) {
        if (soleInstance != null) {
            throw new RuntimeException("Attempt to initialize VM twice");
        } else {
            soleInstance = new VM(db, (JVMDebugger)null, isBigEndian);
            Iterator iter = vmInitializedObservers.iterator();

            while(iter.hasNext()) {
                ((Observer)iter.next()).update((Observable)null, (Object)null);
            }

        }
    }

    public static void initialize(TypeDataBase db, JVMDebugger debugger) {
        if (soleInstance == null) {
            soleInstance = new VM(db, debugger, debugger.getMachineDescription().isBigEndian());
            Iterator iter = vmInitializedObservers.iterator();

            while(iter.hasNext()) {
                ((Observer)iter.next()).update((Observable)null, (Object)null);
            }

            debugger.putHeapConst((long)soleInstance.getHeapOopSize(), (long)soleInstance.getKlassPtrSize(), Universe.getNarrowOopBase(), Universe.getNarrowOopShift(), Universe.getNarrowKlassBase(), Universe.getNarrowKlassShift());
        }
    }

    public static void shutdown() {
        soleInstance = null;
    }

    public static void registerVMInitializedObserver(Observer o) {
        vmInitializedObservers.add(o);
        o.update((Observable)null, (Object)null);
    }

    public static VM getVM() {
        if (soleInstance == null) {
            throw new RuntimeException("VM.initialize() was not yet called");
        } else {
            return soleInstance;
        }
    }

    public void registerVMResumedObserver(Observer o) {
        this.vmResumedObservers.add(o);
    }

    public void registerVMSuspendedObserver(Observer o) {
        this.vmSuspendedObservers.add(o);
    }

    public void fireVMResumed() {
        Iterator iter = this.vmResumedObservers.iterator();

        while(iter.hasNext()) {
            ((Observer)iter.next()).update((Observable)null, (Object)null);
        }

    }

    public void fireVMSuspended() {
        Iterator iter = this.vmSuspendedObservers.iterator();

        while(iter.hasNext()) {
            ((Observer)iter.next()).update((Observable)null, (Object)null);
        }

    }

    public String getOS() {
        return this.debugger != null ? this.debugger.getOS() : PlatformInfo.getOS();
    }

    public String getCPU() {
        return this.debugger != null ? this.debugger.getCPU() : PlatformInfo.getCPU();
    }

    public Type lookupType(String cTypeName) {
        return this.db.lookupType(cTypeName);
    }

    public Integer lookupIntConstant(String name) {
        return this.db.lookupIntConstant(name);
    }

    public static long getAddressValue(Address addr) {
        return getVM().getDebugger().getAddressValue(addr);
    }

    public long getAddressSize() {
        return this.db.getAddressSize();
    }

    public long getOopSize() {
        return (long)this.oopSize;
    }

    public long getLogAddressSize() {
        return this.logAddressSize;
    }

    public long getIntSize() {
        return this.db.getJIntType().getSize();
    }

    public long getStackBias() {
        return this.stackBias;
    }

    public boolean isLP64() {
        if (Assert.ASSERTS_ENABLED) {
            Assert.that(this.isDebugging(), "Debugging system only for now");
        }

        return this.isLP64;
    }

    public int getBytesPerLong() {
        return this.bytesPerLong;
    }

    public int getBytesPerWord() {
        return this.bytesPerWord;
    }

    public int getMinObjAlignmentInBytes() {
        return this.minObjAlignmentInBytes;
    }

    public int getLogMinObjAlignmentInBytes() {
        return this.logMinObjAlignmentInBytes;
    }

    public int getHeapWordSize() {
        return this.heapWordSize;
    }

    public int getHeapOopSize() {
        return this.heapOopSize;
    }

    public int getKlassPtrSize() {
        return this.klassPtrSize;
    }

    public long alignUp(long size, long alignment) {
        return size + alignment - 1L & ~(alignment - 1L);
    }

    public long alignDown(long size, long alignment) {
        return size & ~(alignment - 1L);
    }

    public int buildIntFromShorts(short low, short high) {
        return high << 16 | low & '\uffff';
    }

    public long buildLongFromIntsPD(int oneHalf, int otherHalf) {
        return this.isBigEndian ? (long)otherHalf << 32 | (long)oneHalf & 4294967295L : (long)oneHalf << 32 | (long)otherHalf & 4294967295L;
    }

    public TypeDataBase getTypeDataBase() {
        return this.db;
    }

    public Universe getUniverse() {
        if (this.universe == null) {
            this.universe = new Universe();
        }

        return this.universe;
    }

    public ObjectHeap getObjectHeap() {
        if (this.heap == null) {
            this.heap = new ObjectHeap(this.db);
        }

        return this.heap;
    }

    public SymbolTable getSymbolTable() {
        if (this.symbols == null) {
            this.symbols = SymbolTable.getTheTable();
        }

        return this.symbols;
    }

    public StringTable getStringTable() {
        if (this.strings == null) {
            this.strings = StringTable.getTheTable();
        }

        return this.strings;
    }

    public SystemDictionary getSystemDictionary() {
        if (this.dict == null) {
            this.dict = new SystemDictionary();
        }

        return this.dict;
    }

    public Threads getThreads() {
        if (this.threads == null) {
            this.threads = new Threads();
        }

        return this.threads;
    }

    public ObjectSynchronizer getObjectSynchronizer() {
        if (this.synchronizer == null) {
            this.synchronizer = new ObjectSynchronizer();
        }

        return this.synchronizer;
    }

    public JNIHandles getJNIHandles() {
        if (this.handles == null) {
            this.handles = new JNIHandles();
        }

        return this.handles;
    }

    public Interpreter getInterpreter() {
        if (this.interpreter == null) {
            this.interpreter = new Interpreter();
        }

        return this.interpreter;
    }

    public StubRoutines getStubRoutines() {
        if (this.stubRoutines == null) {
            this.stubRoutines = new StubRoutines();
        }

        return this.stubRoutines;
    }

    public VMRegImpl getVMRegImplInfo() {
        if (this.vmregImpl == null) {
            this.vmregImpl = new VMRegImpl();
        }

        return this.vmregImpl;
    }

    public Bytes getBytes() {
        if (this.bytes == null) {
            this.bytes = new Bytes(this.debugger.getMachineDescription());
        }

        return this.bytes;
    }

    public boolean isBigEndian() {
        return this.isBigEndian;
    }

    public boolean isCore() {
        return !this.usingClientCompiler && !this.usingServerCompiler;
    }

    public boolean isClientCompiler() {
        return this.usingClientCompiler;
    }

    public boolean isServerCompiler() {
        return this.usingServerCompiler;
    }

    public boolean useDerivedPointerTable() {
        return !disableDerivedPointerTableCheck;
    }

    public CodeCache getCodeCache() {
        if (Assert.ASSERTS_ENABLED) {
            Assert.that(!this.isCore(), "noncore builds only");
        }

        if (this.codeCache == null) {
            this.codeCache = new CodeCache();
        }

        return this.codeCache;
    }

    public Runtime1 getRuntime1() {
        if (Assert.ASSERTS_ENABLED) {
            Assert.that(this.isClientCompiler(), "C1 builds only");
        }

        if (this.runtime1 == null) {
            this.runtime1 = new Runtime1();
        }

        return this.runtime1;
    }

    public boolean isDebugging() {
        return this.debugger != null;
    }

    public JVMDebugger getDebugger() {
        if (this.debugger == null) {
            throw new RuntimeException("Attempt to use debugger in runtime system");
        } else {
            return this.debugger;
        }
    }

    public boolean isJavaPCDbg(Address addr) {
        return this.getInterpreter().contains(addr) || this.getCodeCache().contains(addr);
    }

    public int getInvocationEntryBCI() {
        return this.invocationEntryBCI;
    }

    public int getInvalidOSREntryBCI() {
        return this.invalidOSREntryBCI;
    }

    public boolean wizardMode() {
        return true;
    }

    public ReversePtrs getRevPtrs() {
        return this.revPtrs;
    }

    public void setRevPtrs(ReversePtrs rp) {
        this.revPtrs = rp;
    }

    public String getVMRelease() {
        return this.vmRelease;
    }

    public String getVMInternalInfo() {
        return this.vmInternalInfo;
    }

    public int getReserveForAllocationPrefetch() {
        return this.reserveForAllocationPrefetch;
    }

    public boolean isSharingEnabled() {
        if (this.sharingEnabled == null) {
            VM.Flag flag = this.getCommandLineFlag("UseSharedSpaces");
            this.sharingEnabled = flag == null ? Boolean.FALSE : (flag.getBool() ? Boolean.TRUE : Boolean.FALSE);
        }

        return this.sharingEnabled;
    }

    public boolean isCompressedOopsEnabled() {
        if (this.compressedOopsEnabled == null) {
            VM.Flag flag = this.getCommandLineFlag("UseCompressedOops");
            this.compressedOopsEnabled = flag == null ? Boolean.FALSE : (flag.getBool() ? Boolean.TRUE : Boolean.FALSE);
        }

        return this.compressedOopsEnabled;
    }

    public boolean isCompressedKlassPointersEnabled() {
        if (this.compressedKlassPointersEnabled == null) {
            VM.Flag flag = this.getCommandLineFlag("UseCompressedClassPointers");
            this.compressedKlassPointersEnabled = flag == null ? Boolean.FALSE : (flag.getBool() ? Boolean.TRUE : Boolean.FALSE);
        }

        return this.compressedKlassPointersEnabled;
    }

    public int getObjectAlignmentInBytes() {
        if (this.objectAlignmentInBytes == 0) {
            VM.Flag flag = this.getCommandLineFlag("ObjectAlignmentInBytes");
            this.objectAlignmentInBytes = flag == null ? 8 : (int)flag.getIntx();
        }

        return this.objectAlignmentInBytes;
    }

    public boolean getUseTLAB() {
        VM.Flag flag = this.getCommandLineFlag("UseTLAB");
        return flag == null ? false : flag.getBool();
    }

    public VM.Flag[] getCommandLineFlags() {
        if (this.commandLineFlags == null) {
            this.readCommandLineFlags();
        }

        return this.commandLineFlags;
    }

    public VM.Flag getCommandLineFlag(String name) {
        if (this.flagsMap == null) {
            this.flagsMap = new HashMap();
            VM.Flag[] flags = this.getCommandLineFlags();

            for(int i = 0; i < flags.length; ++i) {
                this.flagsMap.put(flags[i].getName(), flags[i]);
            }
        }

        return (VM.Flag)this.flagsMap.get(name);
    }

    private void readCommandLineFlags() {
        TypeDataBase db = this.getTypeDataBase();
        Type flagType = db.lookupType("Flag");
        int numFlags = (int)flagType.getCIntegerField("numFlags").getValue();
        this.commandLineFlags = new VM.Flag[numFlags - 1];
        Address flagAddr = flagType.getAddressField("flags").getValue();
        AddressField typeFld = flagType.getAddressField("_type");
        AddressField nameFld = flagType.getAddressField("_name");
        AddressField addrFld = flagType.getAddressField("_addr");
        CIntField flagsFld = new CIntField(flagType.getCIntegerField("_flags"), 0L);
        long flagSize = flagType.getSize();

        for(int f = 0; f < numFlags - 1; ++f) {
            String type = CStringUtilities.getString(typeFld.getValue(flagAddr));
            String name = CStringUtilities.getString(nameFld.getValue(flagAddr));
            Address addr = addrFld.getValue(flagAddr);
            int flags = (int)flagsFld.getValue(flagAddr);
            this.commandLineFlags[f] = new VM.Flag(type, name, addr, flags);
            flagAddr = flagAddr.addOffsetTo(flagSize);
        }

        Arrays.sort(this.commandLineFlags, new Comparator() {
            public int compare(Object o1, Object o2) {
                VM.Flag f1 = (VM.Flag)o1;
                VM.Flag f2 = (VM.Flag)o2;
                return f1.getName().compareTo(f2.getName());
            }
        });
    }

    public String getSystemProperty(String key) {
        Properties props = this.getSystemProperties();
        return props != null ? props.getProperty(key) : null;
    }

    public Properties getSystemProperties() {
        if (this.sysProps == null) {
            this.readSystemProperties();
        }

        return this.sysProps;
    }

    private void readSystemProperties() {
        this.getSystemDictionary();
        InstanceKlass systemKls = SystemDictionary.getSystemKlass();
        systemKls.iterateStaticFields(new DefaultOopVisitor() {
            ObjectReader objReader = new ObjectReader();

            public void doOop(OopField field, boolean isVMField) {
                if (field.getID().getName().equals("props")) {
                    try {
                        VM.this.sysProps = (Properties)this.objReader.readObject(field.getValue(this.getObj()));
                    } catch (Exception var4) {
                        var4.printStackTrace();
                    }
                }

            }
        });
    }

    static {
        URL url = null;

        try {
            url = VM.class.getClassLoader().getResource("sa.properties");
            saProps.load(new BufferedInputStream(url.openStream()));
        } catch (Exception var2) {
            System.err.println("Unable to load properties  " + (url == null ? "null" : url.toString()) + ": " + var2.getMessage());
        }

        disableDerivedPointerTableCheck = System.getProperty("sun.jvm.hotspot.runtime.VM.disableDerivedPointerTableCheck") != null;
    }

    public static final class Flag {
        private String type;
        private String name;
        private Address addr;
        private int flags;

        private Flag(String type, String name, Address addr, int flags) {
            this.type = type;
            this.name = name;
            this.addr = addr;
            this.flags = flags;
        }

        public String getType() {
            return this.type;
        }

        public String getName() {
            return this.name;
        }

        public Address getAddress() {
            return this.addr;
        }

        public int getOrigin() {
            return this.flags & 15;
        }

        public boolean isBool() {
            return this.type.equals("bool");
        }

        public boolean getBool() {
            if (Assert.ASSERTS_ENABLED) {
                Assert.that(this.isBool(), "not a bool flag!");
            }

            return this.addr.getCIntegerAt(0L, VM.boolType.getSize(), VM.boolType.isUnsigned()) != 0L;
        }

        public boolean isIntx() {
            return this.type.equals("intx");
        }

        public long getIntx() {
            if (Assert.ASSERTS_ENABLED) {
                Assert.that(this.isIntx(), "not a intx flag!");
            }

            return this.addr.getCIntegerAt(0L, VM.intxType.getSize(), false);
        }

        public boolean isUIntx() {
            return this.type.equals("uintx");
        }

        public long getUIntx() {
            if (Assert.ASSERTS_ENABLED) {
                Assert.that(this.isUIntx(), "not a uintx flag!");
            }

            return this.addr.getCIntegerAt(0L, VM.uintxType.getSize(), true);
        }

        public String getValue() {
            if (this.isBool()) {
                return (new Boolean(this.getBool())).toString();
            } else if (this.isIntx()) {
                return (new Long(this.getIntx())).toString();
            } else {
                return this.isUIntx() ? (new Long(this.getUIntx())).toString() : null;
            }
        }
    }
}
