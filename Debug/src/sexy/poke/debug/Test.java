package sexy.poke.debug;

import sun.jvm.hotspot.HotSpotAgent;
import sun.jvm.hotspot.debugger.JVMDebugger;

import java.io.IOException;

public class Test {

    public static void dump(String file, String output) {
        HotSpotAgent agent = new HotSpotAgent();
        agent.attach("test", file);


        ClassDump dump = new ClassDump((JVMDebugger) agent.getDebugger(), "");
        try {
            dump.setJarOutput("D:\\Java\\GitProjects\\test.jar");
        } catch (IOException e) {
            e.printStackTrace();
        }
        dump.start();
    }

    public static void main(String[] args) {
    }
}
