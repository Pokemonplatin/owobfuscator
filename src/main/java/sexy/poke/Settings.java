package sexy.poke;

import org.objectweb.asm.tree.ClassNode;
import sexy.poke.util.ExcludeElement;
import sexy.poke.util.JarUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Flo on 15.08.2018
 */
public class Settings {

    public Settings() {
        libs.addAll(Arrays.asList(JarUtils.getRT()));
    }

    public List<File> libs = new ArrayList<>();
    public File inputFile;
    public File outputFile;

    public ArrayList<ClassNode> cleanNodes = new ArrayList<>();
    public ArrayList<ClassNode> nodes = new ArrayList<>();

    public RemoveState sourceFileRemoval = RemoveState.REMOVE;
    public RemoveState sourceDebugRemoval = RemoveState.REMOVE;
    public RemoveState lineNumberTableRemoval = RemoveState.OFF;
    public RemoveState localVariablesRemoval = RemoveState.REMOVE;
    public RemoveState clIdsRemoval = RemoveState.REMOVE;
    //public RemoveState localVariablesSignaturesRemoval = RemoveState.OFF;

    public RemoveState invisibleAnnotationsRemoval = RemoveState.OFF;
    public RemoveState visibleAnnotationsRemoval = RemoveState.OFF;
    public RemoveState signatureRemoval = RemoveState.OFF;
    //public RemoveState deprecationRemoval = RemoveState.OFF;
    public RemoveState innerClassesRemoval = RemoveState.OFF;
    public RemoveState outerClassesRemoval = RemoveState.OFF;
    public RemoveState outerMethodsRemoval = RemoveState.OFF;

    public ObfuscationDictionary dictionaryClasses = ObfuscationDictionary.OFF;
    public ObfuscationDictionary dictionaryFields = ObfuscationDictionary.OFF;
    public ObfuscationDictionary dictionaryMethods = ObfuscationDictionary.OFF;
    public boolean repackageClasses = false;
    public boolean fixReflection = false;
    public String customStringPool = "";

    public boolean syntheticClasses = false;
    public boolean syntheticFields = false;
    public boolean syntheticMethods = false;
    public boolean publicClasses = false;
    public boolean publicFields = false;
    public boolean publicMethods = false;

    public boolean shuffleFields = false;
    public boolean shuffleMethods = false;

    public boolean flowObfuscation = false;
    public boolean flowSwitchCase = false;
    public boolean flowObfuscation_ExtraAggressive = false;

    public boolean stringPool = false;
    public StringEncryption stringEncryption = StringEncryption.OFF;
    public int stringEncryptionDecrypters = 1;

    public boolean invokeDynamic_Methods = false;
    public boolean invokeDynamic_Fields = false;
    public int invokeDynamic_Methods_Percentage = 100;
    public int invokeDynamic_Fields_Percentage = 100;

    public boolean crasherOne = false;
    public boolean crasherTwo = false;
    public boolean crasherThree = false;
    public boolean crasherASM = false;

    public HashSet<ExcludeElement> excludeList = new HashSet<>();

    public enum StringEncryption {
        OFF, VERYLIGHT("VeryLight"), HEAVY("Heavy");

        StringEncryption() {
            index();
        }

        StringEncryption(String string) {
            index();
            customName = string;
        }

        String customName;

        private static Integer counter;
        private int id;

        private void index() {
            if (counter == null) {
                counter = -1;
            }
            id = counter;
            counter++;

        }

        public int getId() {
            return id;
        }

        public String getCustomName() {
            return customName;
        }
    }

    public enum ObfuscationDictionary {
        OFF, ABC_SMALL("abc"), ABC_BIG("ABC"), ABCABC("abcABC"), NUMERIC("Numeric"), ALPHANUMERIC("Alphanumeric"),
        SPACES("Spaces"), III("iii"), BLOCKS("Blocks"), CUSTOM("Custom");

        ObfuscationDictionary() {
            index();
        }

        ObfuscationDictionary(String string) {
            index();
            customName = string;
        }

        String customName;

        private static Integer counter;
        private int id;

        private void index() {
            if (counter == null) {
                counter = -1;
            }
            id = counter;
            counter++;

        }

        public int getId() {
            return id;
        }

        public String getCustomName() {
            return customName;
        }
    }

    public enum RemoveState {
        OFF, REMOVE, SHUFFLE, REPLACE;

        RemoveState() {
            index();
        }

        private static Integer counter;
        private int id;

        private void index() {
            if (counter == null) {
                counter = -1;
            }
            id = counter;
            counter++;

        }

        public int getId() {
            return id;
        }
    }
}
