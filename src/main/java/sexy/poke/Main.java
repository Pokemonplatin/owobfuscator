package sexy.poke;

import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.svg.SVGGlyph;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sexy.poke.gui.MainGui;

/**
 * Created by Flo on 11.08.2018
 */
public class Main extends Application {

    @FXMLViewFlowContext
    private ViewFlowContext flowContext;

    public static Settings settings;

    public static Stage stage;

    @Override
    public void start(Stage stage) throws Exception {

        settings = new Settings();

        Flow flow = new Flow(MainGui.class);
        DefaultFlowContainer container = new DefaultFlowContainer();
        flowContext = new ViewFlowContext();
        flowContext.register("Stage", stage);
        flow.createHandler(flowContext).start(container);

        JFXDecorator decorator = new JFXDecorator(stage, container.getView(), false, true, true);
        decorator.setCustomMaximize(true);
        decorator.setGraphic(new SVGGlyph(""));

        stage.setTitle("OwObfuscator");

        double width = 560;
        double height = 610;

        Scene scene = new Scene(decorator, width, height);
        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(Main.class.getResource("/css/jfoenix-design.css").toExternalForm(),
                Main.class.getResource("/css/jfoenix-main.css").toExternalForm());
        stage.setScene(scene);
        stage.show();

        Main.stage = stage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
