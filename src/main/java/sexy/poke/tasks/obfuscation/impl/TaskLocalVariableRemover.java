package sexy.poke.tasks.obfuscation.impl;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 07.08.2018
 */
public class TaskLocalVariableRemover extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {

        Platform.runLater(() -> body.setText(""));

        //TODO add more modes

        if(Main.settings.localVariablesRemoval == Settings.RemoveState.SHUFFLE) {
            nodes.parallelStream().forEach(node -> node.methods.parallelStream().forEach( mn -> {
                if(mn.localVariables != null) {
                    Collections.shuffle(mn.localVariables);
                }
            }));
        } else {
            nodes.parallelStream().forEach(node -> node.methods.parallelStream().forEach(methodNode ->  methodNode.localVariables = null));
        }

    }

    @Override
    public String getName() {
        return "LocalVariableRemover";
    }
}
