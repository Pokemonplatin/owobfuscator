package sexy.poke.tasks.obfuscation.impl.flow;

import javafx.scene.text.Text;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.CodeSizeEvaluator;
import org.objectweb.asm.tree.*;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;
import sexy.poke.util.StackEmulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskGotoObfu extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        for (ClassNode cn : nodes) {
            if ((cn.access & Opcodes.ACC_ENUM) != 0 ||
                    (cn.access & Opcodes.ACC_INTERFACE) != 0)
                continue;

            for (MethodNode mn : cn.methods) {
                if (mn.instructions.getFirst() == null)
                    continue;

                StackEmulator se = new StackEmulator(mn);

                int remaining = getRemainingSize(mn);

                List<LabelNode> labelNodes = new ArrayList<>();
                List<JumpInsnNode> jumps = new ArrayList<>();
                List<JumpInsnNode> fakeJumps = new ArrayList<>();
                List<List<AbstractInsnNode>> packages = new ArrayList<>();
                List<AbstractInsnNode> tempPackages = new ArrayList<>();
                List<AbstractInsnNode> firstPackages = new ArrayList<>();

                for (AbstractInsnNode ain : mn.instructions.toArray()) {
                    if(remaining <= 1000)
                        break;

                    if(se.getEmptyAt().contains(ain) && !(ain instanceof LineNumberNode) && !(ain instanceof LabelNode)) {

                        LabelNode l = new LabelNode();
                        labelNodes.add(l);

                        tempPackages.add(getIntPush(1));
                        tempPackages.add(getIntPush(5));
                        JumpInsnNode jump = new JumpInsnNode(Opcodes.IF_ACMPEQ,null);
                        JumpInsnNode jump2 = new JumpInsnNode(Opcodes.GOTO,l);
                        jumps.add(jump);
                        fakeJumps.add(jump2);

                        tempPackages.add(jump);
                        tempPackages.add(jump2);
                        tempPackages.add(l);
                        /*LabelNode l = new LabelNode();
                        mn.instructions.insertBefore(ain,l);
                        mn.instructions.insertBefore(ain, getIntPush(1));
                        mn.instructions.insertBefore(ain, getIntPush(5));
                        mn.instructions.insertBefore(ain, new JumpInsnNode(Opcodes.IF_ACMPEQ,l));*/
                        remaining-= 8;//goto
                        remaining-= 8;//goto
                        remaining-= 4;//2 ints

                        if(firstPackages.isEmpty()) {
                            firstPackages.addAll(tempPackages);
                        } else {
                            packages.add(tempPackages);
                        }

                        tempPackages = new ArrayList<>();
                        tempPackages.add(ain);
                    } else {
                        tempPackages.add(ain);
                    }
                }

                for(int i = 0; i < jumps.size(); i++) {
                    jumps.get(i).label = labelNodes.get(jumps.size() - i - 1);
                }

                for(int i = 0; i < fakeJumps.size(); i++) {
                    //fakeJumps.get(i).label = labelNodes.get(labelNodes.size() == 1 ? 0 :  rnd.nextInt(labelNodes.size() - 1));
                }

                InsnList insList = new InsnList();
                for(AbstractInsnNode ain : firstPackages)
                    insList.add(ain);
                for(List<AbstractInsnNode> list : packages)
                    for(AbstractInsnNode ain : list)
                        insList.add(ain);
                for(AbstractInsnNode ain : tempPackages)
                    insList.add(ain);

                mn.instructions = insList;
            }
        }
    }

    protected int getRemainingSize(MethodNode methodNode) {
        CodeSizeEvaluator cse = new CodeSizeEvaluator(null);
        methodNode.accept(cse);
        // Max allowed method size is 65534 (https://docs.oracle.com/javase/specs/jvms/se10/html/jvms-4.html#jvms-4.7.3)
        return 65534 - cse.getMaxSize();
    }

    @Override
    public String getName() {
        return "TaskGotoObfu";
    }
}
