package sexy.poke.tasks.obfuscation.impl.removeal;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 11.10.2018
 */
public class TaskAnnotationRemover extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        nodes.parallelStream().forEach(cn -> {

            Platform.runLater(() -> body.setText("Removing Annotations in class " + cn.name));

            if(Main.settings.invisibleAnnotationsRemoval != Settings.RemoveState.OFF) {
                cn.invisibleAnnotations = null;
            }
            if(Main.settings.visibleAnnotationsRemoval != Settings.RemoveState.OFF) {
                cn.visibleAnnotations = null;
            }

            cn.methods.parallelStream().forEach(mn -> {

                Platform.runLater(() -> body.setText("Removing Method Annotations in class " + cn.name));

                if(Main.settings.invisibleAnnotationsRemoval != Settings.RemoveState.OFF) {
                    mn.invisibleAnnotations = null;
                }
                if(Main.settings.visibleAnnotationsRemoval != Settings.RemoveState.OFF) {
                    mn.visibleAnnotations = null;
                }
            });

            cn.fields.parallelStream().forEach(fn -> {

                Platform.runLater(() -> body.setText("Removing Field Annotations in class " + cn.name));

                if(Main.settings.invisibleAnnotationsRemoval != Settings.RemoveState.OFF) {
                    fn.invisibleAnnotations = null;
                }
                if(Main.settings.visibleAnnotationsRemoval != Settings.RemoveState.OFF) {
                    fn.visibleAnnotations = null;
                }
            });
        });
    }

    @Override
    public String getName() {
        return "AnnotationRemover";
    }
}
