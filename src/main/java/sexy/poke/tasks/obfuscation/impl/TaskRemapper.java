package sexy.poke.tasks.obfuscation.impl;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.ClassRemapper;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.mappings.SmallFieldNode;
import sexy.poke.mappings.SmallMethodNode;
import sexy.poke.mappings.SmallPackageNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.*;

/**
 * Created by Flo on 04.08.2018
 */
public class TaskRemapper extends AbstractObfuscationTask {

    public TaskRemapper() {
    }


    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mapping, Text body) {
        if (Main.settings.repackageClasses)
            for (SmallPackageNode packageNode : SmallClassNode.packeges) {
                if (validPackage(packageNode))
                    packageNode.setName(generateString(rnd, 10, true, Main.settings.dictionaryClasses));
            }

        for (ListIterator<ClassNode> iterator = nodes.listIterator(); iterator.hasNext(); ) {
            ClassNode c = iterator.next();

            Platform.runLater(() -> body.setText("Renaming " + c.name));

            ClassNode newNode = new ClassNode();

            //transform class and write in new Class
            c.accept(new ClassRemapper(new ClassVisitor(Opcodes.ASM6, newNode) {
            }, new Remapper() {
                @Override
                public String map(String typeName) {

                    if (Main.settings.dictionaryClasses == Settings.ObfuscationDictionary.OFF)
                        return typeName;

                    if (!validClass(typeName))
                        return typeName;

                    //get ClassNode from owner
                    SmallClassNode scn = mapping.get(typeName);

                    //If class is not loaded or is lib keeps original name
                    if (scn != null && !scn.isLib()) {

                        //if (scn.getNewName() == null) {
                        if (!scn.hasNewName()) {
                            scn.setNewName(generateString(rnd, 10, true, Main.settings.dictionaryClasses));
                        }

                        return scn.getNewName();
                    }

                    return typeName;
                }

                @Override
                public String mapFieldName(String owner, String name, String descriptor) {

                    if (Main.settings.dictionaryFields == Settings.ObfuscationDictionary.OFF)
                        return name;

                    //get ClassNode from owner
                    SmallClassNode scn = mapping.get(owner);

                    if (scn == null)
                        return name;

                    SmallFieldNode node = scn.getField(name + descriptor);

                    //field not present is it in superclass/interfaces`?
                    if (node == null) {
                        //get method from superclass/interface
                        node = scn.checkField(name + descriptor, false);
                    }

                    if (node == null || !validField(node.getOwner().getClassName(), node.getName())/* || !validClass(node.getOwner().getClassName())*/)
                        return name;

                    //If class is not loaded or is lib keeps original name
                    if (!node.getOwner().isLib()) {

                        if (node.getNewName() == null) {
                            node.setNewName(generateString(rnd, 10, true, Main.settings.dictionaryFields));
                        }

                        return node.getNewName();
                    }
                    return name;
                }

                public String mapInvokeDynamicMethodName(String name, String descriptor) {

                    if (Main.settings.dictionaryMethods == Settings.ObfuscationDictionary.OFF)
                        return name;

                    //get class from descriptor
                    Type clazz = Type.getReturnType(descriptor);

                    //Get ClassNode from name
                    SmallClassNode scn = mapping.get(clazz.getClassName().replace(".", "/"));
                    if (scn == null)
                        return name;

                    //Get method from target class
                    //SmallMethodNode node = scn.getMethodWithoutDesk(name);

                    //method not present is it in superclass/interfaces?
                    //if (node == null) {
                    //get method from superclass/interface
                    SmallMethodNode node = scn.checkMethodWithoutDesk(name, false);
                    //}

                    //Class of method not loaded?
                    if (node == null) {
                        System.err.println("Superclass/Interface not present " + clazz.getClassName() + " " + name + descriptor);
                    }

                    if (node == null || !validMethod(node.getOwner().getClassName(), node.getName(), node.getDesk())/* || !validClass(node.getOwner().getClassName())*/)
                        return name;

                    if (node.getOwner().isEnum() && node.isStatic()) {
                        if (node.getCombined().startsWith("values()[") || node.getCombined().startsWith("valueOf(Ljava/lang/String;)")) {
                            return name;
                        }
                    }

                    //If class is not loaded or is lib method keeps original name
                    if (!node.getOwner().isLib()) {

                        //Check if new name is already set
                        if (node.getNewName() == null) {
                            node.setNewName(generateString(rnd, 10, true, Main.settings.dictionaryMethods, node.getCombined()));
                        }

                        return node.getNewName();
                    }

                    return name;
                }

                public String mapMethodName(String owner, String name, String descriptor) {

                    if (Main.settings.dictionaryMethods == Settings.ObfuscationDictionary.OFF)
                        return name;

                    if (owner.startsWith("[") || name.contains("<")/* || name.equals("main")*/)
                        return name;

                    //get class from name
                    SmallClassNode scn = mapping.get(owner);
                    if (scn == null)
                        return name;

                    //get method from target class
                    SmallMethodNode node = scn.getMethod(name + descriptor);


                    //method not present is it in superclass/interfaces`?
                    if (node == null) {
                        //get method from superclass/interface
                        node = scn.checkMethod(name + descriptor, false);
                    }

                    //Class of method not loaded?
                    if (node == null) {
                        System.err.println("Superclass/Interface not present " + owner + " " + name + descriptor);
                    }

                    if (node == null || !validMethod(node.getOwner().getClassName(), node.getName(), node.getDesk())/* || !validClass(node.getOwner().getClassName())*/)
                        return name;

                    if (node.getOwner().isEnum() && node.isStatic()) {
                        if (node.getCombined().startsWith("values()[") || node.getCombined().startsWith("valueOf(Ljava/lang/String;)")) {
                            return name;
                        }
                    }

                    //If class is not loaded or is lib method keeps original name
                    if (!node.getOwner().isLib()) {

                        //check if new name is already set
                        if (node.getNewName() == null) {
                            node.setNewName(generateString(rnd, 10, true, Main.settings.dictionaryMethods, node.getCombined()));
                        }

                        return node.getNewName();
                    }

                    return name;
                }
            }));

            //replace ClassNode
            iterator.set(newNode);
        }

        if (Main.settings.fixReflection) {
            for (ClassNode cn : nodes) {
                Platform.runLater(() -> body.setText("Fixing Reflection " + cn.name));
                for (MethodNode mn : cn.methods) {
                    for (AbstractInsnNode ain : mn.instructions.toArray()) {
                        if (ain instanceof LdcInsnNode) {
                            String str = ((LdcInsnNode) ain).cst.toString();

                            if (str.contains("/")) {
                                SmallClassNode scn = mapping.get(str);
                                if (scn != null) {
                                    String nn = scn.getNewName();
                                    if (nn != null) {
                                        System.out.println("Found reflection: " + str + " -> " + nn);
                                        ((LdcInsnNode) ain).cst = nn;
                                    }
                                } else {
                                /*for (SmallPackageNode packageNode : SmallClassNode.packeges) {
                                    if (packageNode.hasNewName() && packageNode.getCompleteName().equals(str)) {
                                        ((LdcInsnNode) ain).cst = packageNode.getCompleteNewName();
                                    }
                                }*/
                                }
                            } else if (str.contains(".")) {
                                SmallClassNode scn = mapping.get(str.replace(".", "/"));
                                if (scn != null) {
                                    String nn = scn.getNewName();
                                    if (nn != null) {
                                        System.out.println("Found reflection: " + str + " -> " + nn.replace("/", "."));
                                        ((LdcInsnNode) ain).cst = nn.replace("/", ".");
                                    }
                                } else {
                                /*String str2 = str.replace(".","/");
                                for (SmallPackageNode packageNode : SmallClassNode.packeges) {
                                    if (packageNode.hasNewName() && packageNode.getCompleteName().equals(str2)) {
                                        ((LdcInsnNode) ain).cst = packageNode.getCompleteNewName().replace("/", "-");
                                    }
                                }*/
                                }
                            } else {
                                Optional<SmallClassNode> opt = mapping.values().stream().filter(it -> it.getSimpleName().equals(str)).findFirst();
                                if (opt.isPresent() && opt.get().hasNewName()) {
                                    ((LdcInsnNode) ain).cst = opt.get().getSimpleNewName();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public String getName() {
        return "Remapper";
    }
}
