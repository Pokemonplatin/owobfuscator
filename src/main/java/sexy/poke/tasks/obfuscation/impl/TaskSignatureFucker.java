package sexy.poke.tasks.obfuscation.impl;

import javafx.scene.text.Text;
import jdk.nashorn.api.scripting.NashornScriptEngine;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 05.08.2018
 */
public class TaskSignatureFucker extends AbstractObfuscationTask {

    public TaskSignatureFucker() {
        for (int i = 0; i < 20; i++) {
            strings.add((char) rnd.nextInt() + "");
        }
    }

    List<String> strings = new ArrayList<>();

    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        for (ClassNode cn : nodes) {
            if (cn.signature == null) {
                cn.signature = "(L" + generateString(rnd, strings, 50, false) + ";)V";
            }
        }
    }

    @Override
    public String getName() {
        return "SignatureFucker";
    }
}
