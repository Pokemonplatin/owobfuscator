package sexy.poke.tasks.obfuscation.impl.string.decrypters;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import sexy.poke.Main;
import sexy.poke.tasks.obfuscation.impl.string.AbstractDecrypter;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * Created by Flo on 08.08.2018
 */
public class VeryLightDecrypter extends AbstractDecrypter {
    public VeryLightDecrypter(ClassNode node) {
        super(node);

        methodName = generateString(rnd, 10, true, Main.settings.dictionaryMethods);
        methodDesk = "(Ljava/lang/" + (rnd.nextBoolean() ? "String" : "Object") + ";"+("I" )+")Ljava/lang/" + (rnd.nextBoolean() ? "String" : "String") + ";";

        offset = getByte(rnd);

        addMethod(node);
    }

    private int offset;

    @Override
    protected void process(ClassNode node, MethodNode methodNode, LabelNode prevLabel, LdcInsnNode ldc) {
        try {

            int offset = getByte(rnd);
            String str = getString(ldc.cst.toString(), offset);

            if (str.getBytes().length >= 65535)
                return;

            ldc.cst = str;

            AbstractInsnNode prev;

            methodNode.instructions.set(ldc,new VarInsnNode(ALOAD, ++methodNode.maxLocals));

            methodNode.instructions.insertBefore(methodNode.instructions
                    .getFirst(), new VarInsnNode(ASTORE, methodNode.maxLocals));
            methodNode.instructions.insertBefore(methodNode.instructions
                    .getFirst(), new MethodInsnNode(INVOKESTATIC, this.node.name, methodName, methodDesk, false));
            methodNode.instructions.insertBefore(methodNode.instructions
                    .getFirst(), getIntPush(offset));
            methodNode.instructions.insertBefore(methodNode.instructions
                    .getFirst(), ldc);
            methodNode.instructions.insertBefore(methodNode.instructions
                    .getFirst(), new LabelNode());


            /*methodNode.instructions.insert(ldc, prev = new LabelNode());
            methodNode.instructions.insert(prev, prev = new FrameNode(F_SAME,0,null,0,null));
            methodNode.instructions.insert(prev, prev = getIntPush(offset));
            methodNode.instructions.insert(prev, prev = new MethodInsnNode(INVOKESTATIC, this.node.name, methodName, methodDesk, false));

            if (methodDesk.endsWith("Ljava/lang/Object;"))
                methodNode.instructions.insert(prev, new MethodInsnNode(INVOKEVIRTUAL, "java/lang/Object", "toString", "()Ljava/lang/String;", false));*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addMethod(ClassNode node) {

        String fieldOne = "0";
        String fieldTwo = "1";

        fieldOne = generateString(rnd, 10, true);
        fieldTwo = generateString(rnd, 10, true);

        //node.fields.add(new FieldNode(Opcodes.ACC_STATIC | Opcodes.ACC_PRIVATE, fieldOne, "I", null, 0));
        //node.fields.add(new FieldNode(Opcodes.ACC_STATIC | Opcodes.ACC_PRIVATE, fieldTwo, "I", null, null));


        /*MethodVisitor mv1 = node.visitMethod(ACC_PUBLIC + ACC_STATIC, methodName, methodDesk, null, null);
        mv1.visitCode();
        Label l01 = new Label();
        mv1.visitLabel(l01);
        mv1.visitLineNumber(31, l01);
        mv1.visitVarInsn(ALOAD, 0);
        mv1.visitInsn(ARETURN);
        Label l11 = new Label();
        mv1.visitLabel(l11);
        mv1.visitLocalVariable("in", "Ljava/lang/Object;", null, l01, l11, 0);
        mv1.visitLocalVariable("offset", "I", null, l01, l11, 1);
        mv1.visitMaxs(1, 2);
        mv1.visitEnd();

        if(true)
            return;*/
        MethodVisitor mv = node.visitMethod(ACC_PUBLIC + ACC_STATIC, methodName, methodDesk, null, null);
        mv.visitCode();
        Label l0 = new Label();
        Label l1 = new Label();
        Label l2 = new Label();
        Label l3 = new Label();
        Label l4 = new Label();
        Label l5 = new Label();
        Label l6 = new Label();


        mv.visitLabel(l0);
        mv.visitVarInsn(ALOAD, 0);

        if(!methodDesk.startsWith("(Ljava/lang/String;"))
            mv.visitTypeInsn(CHECKCAST, "java/lang/String");

        mv.visitLdcInsn("ISO-8859-1");
        mv.visitMethodInsn(INVOKESTATIC, "java/nio/charset/Charset", "forName", "(Ljava/lang/String;)Ljava/nio/charset/Charset;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "(Ljava/nio/charset/Charset;)[B", false);
        mv.visitVarInsn(ASTORE, 2);

        mv.visitLabel(l1);
        mv.visitVarInsn(ALOAD, 2);
        mv.visitInsn(ARRAYLENGTH);
        mv.visitIntInsn(NEWARRAY, T_BYTE);
        mv.visitVarInsn(ASTORE, 3);

        //added
        /*mv.visitFieldInsn(GETSTATIC, node.name, fieldOne, "I");
        mv.visitIntInsn(BIPUSH, getByte(rnd));
        mv.visitJumpInsn(rnd.nextBoolean() ? IF_ICMPGT : IF_ICMPGE , l1);*/
        //

        mv.visitLabel(l2);
        mv.visitInsn(ICONST_0);
        mv.visitVarInsn(ISTORE, 4);


        //added
        /*Label ExcludeGui = new Label();
        mv.visitJumpInsn(GOTO,ExcludeGui);

        Label custom_L2 = new Label();
        mv.visitLabel(custom_L2);
        mv.visitFieldInsn(GETSTATIC, node.name, fieldTwo, "I");
        mv.visitIntInsn(BIPUSH, -getByte(rnd));
        mv.visitJumpInsn(rnd.nextBoolean() ? IF_ICMPLT : IF_ICMPLE, l2);
        mv.visitJumpInsn(GOTO,l3);

        mv.visitLabel(ExcludeGui);*/
        //

        //added
        /*mv.visitFieldInsn(GETSTATIC, node.name, fieldTwo, "I");
        mv.visitIntInsn(BIPUSH, getByte(rnd));
        mv.visitJumpInsn(rnd.nextBoolean() ? IF_ICMPGT : IF_ICMPGE, l1);*/
        //

        mv.visitLabel(l3);
        mv.visitFrame(F_APPEND, 3, new Object[]{"[B", "[B", INTEGER}, 0, null);
        mv.visitVarInsn(ILOAD, 4);
        mv.visitVarInsn(ALOAD, 3);
        mv.visitInsn(ARRAYLENGTH);

        mv.visitJumpInsn(IF_ICMPGE, l4);

        //added
        /*mv.visitFieldInsn(GETSTATIC, node.name, fieldOne, "I");
        mv.visitIntInsn(BIPUSH, getByte(rnd));

        Label custom_L1 = new Label();
        mv.visitJumpInsn(rnd.nextBoolean() ? IF_ICMPGT : IF_ICMPGE, custom_L1);
        mv.visitJumpInsn(GOTO,l5);*/
        //


        //added
       /* mv.visitLabel(custom_L1);
        mv.visitFieldInsn(GETSTATIC, node.name, fieldOne, "I");
        mv.visitIntInsn(BIPUSH, getByte(rnd));

        mv.visitJumpInsn(rnd.nextBoolean() ? IF_ICMPGT : IF_ICMPGE, custom_L2);
        mv.visitJumpInsn(GOTO,l1);*/
        //





        mv.visitLabel(l5);
        mv.visitVarInsn(ALOAD, 3);
        mv.visitVarInsn(ILOAD, 4);
        mv.visitVarInsn(ALOAD, 2);
        mv.visitVarInsn(ILOAD, 4);
        mv.visitInsn(BALOAD);

        //added
        /*mv.visitFieldInsn(GETSTATIC, node.name, fieldOne, "I");
        mv.visitIntInsn(BIPUSH, getByte(rnd));
        mv.visitJumpInsn(rnd.nextBoolean() ? IF_ICMPGT : IF_ICMPGE, l1);*/
        //



        //added
        /*mv.visitFieldInsn(GETSTATIC, node.name, fieldTwo, "I");
        mv.visitIntInsn(BIPUSH, getByte(rnd));
        mv.visitJumpInsn(rnd.nextBoolean() ? IF_ICMPGT : IF_ICMPGE, l3);*/
        //
        /*if(!methodDesk.contains("B")) {
            mv.visitVarInsn(ALOAD, 1);
            mv.visitTypeInsn(CHECKCAST, "java/lang/Byte");
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Byte", "byteValue", "()B", false);
        } else {*/
            mv.visitVarInsn(ILOAD, 1);
        //}

        mv.visitIntInsn(offset <= 127 ? BIPUSH : SIPUSH, offset);
        mv.visitInsn(IADD);
        mv.visitInsn(IXOR);
        mv.visitInsn(I2B);
        mv.visitInsn(BASTORE);

        mv.visitLabel(l6);
        mv.visitIincInsn(4, 1);
        mv.visitJumpInsn(GOTO, l3);
        mv.visitLabel(l4);
        mv.visitFrame(F_CHOP, 1, null, 0, null);
        mv.visitTypeInsn(NEW, "java/lang/String");
        mv.visitInsn(DUP);
        mv.visitVarInsn(ALOAD, 3);
        mv.visitMethodInsn(INVOKESPECIAL, "java/lang/String", "<init>", "([B)V", false);
        mv.visitInsn(ARETURN);
        mv.visitMaxs(4, 5);
        mv.visitEnd();
    }

    private String getString(Object in, int b) throws UnsupportedEncodingException {
        byte[] array = ((String) in).getBytes(Charset.forName("ISO-8859-1"));
        byte[] out = new byte[array.length];

        for (int i = 0; i < out.length; i++) {
            out[i] = (byte) (array[i] ^ (b + offset));
        }

        return new String(out,"ISO-8859-1");
    }

    public AbstractInsnNode getIntPush(int i) {
        if (i <= 5 && i >= -1) {
            return new InsnNode(i + 3);
        } else if (i <= 127 && i >= -128) {
            return new IntInsnNode(BIPUSH, i);
        } else if (i <= 32767 && i >= -32768) {
            return new IntInsnNode(SIPUSH, i);
        }
        return new LdcInsnNode(i);
    }
}
