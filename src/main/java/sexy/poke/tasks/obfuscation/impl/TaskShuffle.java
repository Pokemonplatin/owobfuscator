package sexy.poke.tasks.obfuscation.impl;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 11.08.2018
 */
public class TaskShuffle extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        nodes.parallelStream().forEach(n -> {
            Platform.runLater(() -> body.setText("Shuffling members " + n.name));

            if (Main.settings.shuffleMethods)
                Collections.shuffle(n.methods);
            if (Main.settings.shuffleFields)
                Collections.shuffle(n.fields);
        });
    }

    @Override
    public String getName() {
        return "Shuffler";
    }
}
