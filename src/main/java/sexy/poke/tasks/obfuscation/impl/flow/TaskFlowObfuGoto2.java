package sexy.poke.tasks.obfuscation.impl.flow;

import javafx.scene.text.Text;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.CodeSizeEvaluator;
import org.objectweb.asm.tree.*;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;
import sexy.poke.util.StackEmulator;

import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Created by Flo on 09.08.2018
 */
public class TaskFlowObfuGoto2 extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodesIn, HashMap<String, SmallClassNode> mappings, Text body) {


        /*long current = System.currentTimeMillis();
        nodesIn.forEach(classNode -> {
            FieldNode field = new FieldNode(ACC_PUBLIC + ACC_STATIC +
                    ACC_FINAL, generateString(rnd,10,true), "Z", null, null);
            classNode.fields.add(field);
            classNode.methods.stream().filter(this::hasInstructions).forEach(methodNode -> {
                int varIndex = methodNode.maxLocals;
                methodNode.maxLocals++;
                AbstractInsnNode[] untouchedList = methodNode.instructions.toArray();
                LabelNode labelNode = exitLabel(methodNode);
                boolean calledSuper = false;
                for (AbstractInsnNode insn : untouchedList) {
                    if (this.methodSize(methodNode) > 60000) break;

                    if (methodNode.name.equals("<init>")) {
                        if (insn instanceof MethodInsnNode) {
                            if (insn.getOpcode() == INVOKESPECIAL
                                    && insn.getPrevious() instanceof VarInsnNode
                                    && ((VarInsnNode) insn.getPrevious()).var == 0) {
                                calledSuper = true;
                            }
                        }
                    }
                    if (insn != methodNode.instructions.getFirst()
                            && !(insn instanceof LineNumberNode)) {
                        if (methodNode.name.equals("<init>") && !calledSuper)
                            continue;
                        StackAnalyzer sa = new StackAnalyzer(methodNode, insn);
                        Stack<Object> stack = sa.returnStackAtBreak();
                        if (stack.isEmpty()) { // We need to make sure stack is empty before making jumps
                            methodNode.instructions.insertBefore(insn, new VarInsnNode(ILOAD, varIndex));
                            methodNode.instructions.insertBefore(insn,
                                    new JumpInsnNode(IFNE, labelNode));
                        }
                    }

                    if (insn instanceof JumpInsnNode && false) {
                        if (insn.getOpcode() == GOTO) {
                            methodNode.instructions.insertBefore(insn,
                                    new VarInsnNode(ILOAD, varIndex));
                            methodNode.instructions.insertBefore(insn,
                                    new InsnNode(ICONST_0));
                            methodNode.instructions.insert(insn,
                                    new InsnNode(ATHROW));
                            methodNode.instructions.insert(insn,
                                    new InsnNode(ACONST_NULL));
                            methodNode.instructions.set(insn,
                                    new JumpInsnNode(IF_ICMPEQ,
                                            ((JumpInsnNode) insn).label));
                        }
                    }
                }

                methodNode.instructions.insertBefore(methodNode.instructions
                        .getFirst(), new VarInsnNode(ISTORE, varIndex));
                methodNode.instructions.insertBefore(methodNode.instructions
                        .getFirst(), new FieldInsnNode(GETSTATIC,
                        classNode.name, field.name, "Z"));
            });
        });*/



        HashMap<String, String> staticFields = new HashMap<>();

        for (ClassNode cn : nodesIn) {

            if ((cn.access & Opcodes.ACC_ENUM) != 0 ||
                    (cn.access & Opcodes.ACC_INTERFACE) != 0 ||
                    cn.name.contains("$"))
                continue;


            String name = generateString(rnd, 10, true);
            cn.fields.add(new FieldNode(ACC_PUBLIC | ACC_STATIC, name, "I", null, null));

            staticFields.put(cn.name, name);
        }

        for (ClassNode cn : nodesIn) {

            if ((cn.access & Opcodes.ACC_ENUM) != 0 ||
                    (cn.access & Opcodes.ACC_INTERFACE) != 0)
                continue;

            int fields = rnd.nextInt(4) + 3;

            //List<String> available = new ArrayList<>();

            for (int i = 0; i < fields; i++) {
                //String name = generateString(rnd, 10, true);
                //cn.fields.add(new FieldNode((rnd.nextBoolean() ? ACC_PUBLIC : ACC_PRIVATE) | ACC_STATIC, name, "I", null, null));
                //available.add(name);
            }


            for (MethodNode mn : cn.methods) {

                int varIndex = mn.maxLocals;
                //



                // List<LabelNode> origNodes = Stream.of(mn.instructions.toArray()).filter(o -> o instanceof LabelNode).map(o -> (LabelNode) o).collect(Collectors.toList());
                /*List<LabelNode> framenodes = new ArrayList<>();
                 Stream.of(mn.instructions.toArray()).filter(o -> o instanceof FrameNode).map(o -> (FrameNode) o).forEach(f -> {

                     AbstractInsnNode ain = f;
                     while (!(ain.getPrevious() instanceof LabelNode)) {
                         ain = ain.getPrevious();
                     }
                     framenodes.add((LabelNode)ain.getPrevious());
                });*/


                //int flowPoints = origNodes.size() * 4;

                //List<Integer> ints = rnd.ints(flowPoints, 0, mn.instructions.size() + 1).boxed().collect(Collectors.toList());

                //LabelNode labelNode = exitLabel(mn);
                LabelNode first = new LabelNode();

                boolean calledSuper = false;

                List<String> usedFields = new ArrayList<>();

                if(mn.instructions.size() <= 0)
                    continue;

                StackEmulator se = new StackEmulator(mn);


                for (AbstractInsnNode ain : mn.instructions.toArray()) {
                    if (this.methodSize(mn) > 60000) break;

                    if (mn.name.equals("<init>")) {
                        if (ain instanceof MethodInsnNode) {
                            if (ain.getOpcode() == INVOKESPECIAL
                                    && ain.getPrevious() instanceof VarInsnNode
                                    && ((VarInsnNode) ain.getPrevious()).var == 0) {
                                calledSuper = true;
                            }
                        }
                    }

                    if (ain != mn.instructions.getFirst()
                            && !(ain instanceof LineNumberNode)) {
                        if (mn.name.equals("<init>") && !calledSuper)
                            continue;

                        if (se.getEmptyAt().contains(ain)) {

                            InsnList list = new InsnList();

                            LabelNode node1 = new LabelNode();
                            list.add(node1);
                            //list.add(new FrameNode(F_FULL,1, new Object[] {INTEGER}, 0, new Object[]{}));


                            //list.add(new FieldInsnNode(GETSTATIC, cn.name, available.get(rnd.nextInt(available.size())), "I"));

                            int i = rnd.nextInt(fields);

                            //usedFields.add(available.get(i));

                            //list.add(new VarInsnNode(ILOAD, varIndex + i));
                            //list.add(new VarInsnNode(ISTORE, varIndex + i));
                            //list.add(new FieldInsnNode(GETSTATIC, cn.name, available.get(i), "I"));
                            list.add(new VarInsnNode(ILOAD, varIndex + i));

                            LabelNode gotosyso = new LabelNode();
                            LabelNode gotobehindsyso = new LabelNode();

                            list.add(new IntInsnNode(BIPUSH, getByte(rnd)));
                            list.add(new JumpInsnNode(rnd.nextBoolean() ? IF_ICMPGT : IF_ICMPGE, gotosyso));

                            list.add(new JumpInsnNode(GOTO,gotobehindsyso));

                            list.add(gotosyso);

                            /*list.add(new FieldInsnNode(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
                            list.add(new LdcInsnNode(cn.name + " " + mn.name));
                            list.add(new MethodInsnNode(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));*/

                            list.add(new JumpInsnNode(GOTO, first));

                            list.add(gotobehindsyso);

                            mn.instructions.insertBefore(ain, list);
                            //retry = false;
                        }
                    }

                    if (ain instanceof JumpInsnNode) {
                        if (ain.getOpcode() == GOTO) {

                            int i = rnd.nextInt(fields);

                            mn.instructions.insertBefore(ain,
                                    new VarInsnNode(ILOAD, varIndex + i));
                            mn.instructions.insertBefore(ain,
                                    new InsnNode(ICONST_0));
                            mn.instructions.insert(ain,
                                    new InsnNode(ATHROW));
                            mn.instructions.insert(ain,
                                    new InsnNode(ACONST_NULL));
                            mn.instructions.set(ain,
                                    new JumpInsnNode(IF_ICMPEQ,
                                            ((JumpInsnNode) ain).label));
                        } else if (ain.getOpcode() >= IFEQ
                                || ain.getOpcode() <= IF_ICMPLE) {
                            int i = rnd.nextInt(fields);
                            mn.instructions.insert(ain, new JumpInsnNode(IFNE, ((JumpInsnNode) ain).label));
                            mn.instructions.insert(ain, new VarInsnNode(ILOAD, varIndex +i));
                            //counter.incrementAndGet();
                        }
                    }


                }

                //int index = 0;
                Set<Map.Entry<String, String>> tmp = staticFields.entrySet();
                Object[] thisFileds = tmp.toArray();

                for(int i = 0; i < fields; i++) {

                    int index =  rnd.nextInt(thisFileds.length);

                    mn.instructions.insertBefore(mn.instructions
                            .getFirst(), new VarInsnNode(ISTORE, varIndex + i));
                    mn.instructions.insertBefore(mn.instructions
                            .getFirst(), new FieldInsnNode(GETSTATIC,
                            ((Map.Entry)thisFileds[index]).getKey().toString(), ((Map.Entry)thisFileds[index]).getValue().toString(), "I"));
                    mn.instructions.insertBefore(mn.instructions
                            .getFirst(), new LabelNode());
                }

               /* if(false)
                for(String str : available) {
                    if(usedFields.contains(str) || true) {
                        mn.instructions.insertBefore(mn.instructions
                                .getFirst(), new VarInsnNode(ISTORE, varIndex + index++));
                        mn.instructions.insertBefore(mn.instructions
                                .getFirst(), new FieldInsnNode(GETSTATIC,
                                cn.name, str, "I"));
                    }
                }*/

                mn.maxLocals += usedFields.size();

                mn.instructions.insertBefore(mn.instructions.getFirst(),first);
            }
        }
    }

    @Override
    public String getName() {
        return null;
    }

    protected int methodSize(MethodNode methodNode) {
        CodeSizeEvaluator cse = new CodeSizeEvaluator(null);
        methodNode.accept(cse);
        return cse.getMaxSize();
    }

    protected boolean hasInstructions(MethodNode methodNode) {
        return (!Modifier.isNative(methodNode.access) && !Modifier.isAbstract(methodNode.access));
    }

    /**
     * Inserts an "exit" label into the start of the method node which is used
     * by the transformer to branch the stack into a false jump.
     *
     * @param methodNode current {@link MethodNode} to insert an exit code
     *                   block into.
     * @return a {@link LabelNode} used to branch the stack with a false
     * conditional.
     */
    private LabelNode exitLabel(MethodNode methodNode) {
        LabelNode lb = new LabelNode();
        LabelNode escapeNode = new LabelNode();
        AbstractInsnNode target = methodNode.instructions.getFirst();
        methodNode.instructions.insertBefore(target, new JumpInsnNode(GOTO, escapeNode));
        methodNode.instructions.insertBefore(target, lb);
        Type returnType = Type.getReturnType(methodNode.desc);
        switch (returnType.getSort()) {
            case Type.VOID:
                methodNode.instructions.insertBefore(target, new InsnNode(RETURN));
                break;
            case Type.BOOLEAN:
            case Type.CHAR:
            case Type.BYTE:
            case Type.SHORT:
            case Type.INT:
                methodNode.instructions.insertBefore(target, new InsnNode(ICONST_0));
                methodNode.instructions.insertBefore(target, new InsnNode(IRETURN));
                break;
            case Type.LONG:
                methodNode.instructions.insertBefore(target, new InsnNode(LCONST_0));
                methodNode.instructions.insertBefore(target, new InsnNode(LRETURN));
                break;
            case Type.FLOAT:
                methodNode.instructions.insertBefore(target, new InsnNode(FCONST_0));
                methodNode.instructions.insertBefore(target, new InsnNode(FRETURN));
                break;
            case Type.DOUBLE:
                methodNode.instructions.insertBefore(target, new InsnNode(DCONST_0));
                methodNode.instructions.insertBefore(target, new InsnNode(DRETURN));
                break;
            default:
                methodNode.instructions.insertBefore(target, new InsnNode(ACONST_NULL));
                methodNode.instructions.insertBefore(target, new InsnNode(ARETURN));
                break;
        }
        methodNode.instructions.insertBefore(target, escapeNode);

        return lb;
    }
}
