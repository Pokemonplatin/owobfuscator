package sexy.poke.tasks.obfuscation.impl;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.*;
import org.objectweb.asm.tree.*;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;
import sexy.poke.tasks.obfuscation.impl.flow.TaskFlowObfu3;
import sexy.poke.tasks.obfuscation.impl.string.TaskStringEncryption;

import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class TaskInvokeDynamic extends AbstractObfuscationTask {

    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        MemberNames names = new MemberNames(generateString(rnd, 10, true));

        Handle bsmHandle = new Handle(H_INVOKESTATIC, names.className, names.bootstrapMethodName, "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", false);

        List<String> finalFields = new CopyOnWriteArrayList<>();

        nodes.forEach(cn -> {
            cn.fields.forEach(fn -> {
                if ((fn.access & ACC_FINAL) != 0) {
                    finalFields.add(cn.name + ":" + fn.name);
                }
            });
        });

        for(String str : finalFields)
            System.out.println(str);


        nodes.parallelStream().forEach(n -> n.fields.parallelStream().forEach(node -> {
                    boolean finalMod = (node.access & Opcodes.ACC_FINAL) != 0;
                    boolean publicMod = (node.access & Opcodes.ACC_PUBLIC) != 0;
                    boolean staticMod = (node.access & Opcodes.ACC_STATIC) != 0;

                    if (finalMod) {
                        if (!(publicMod && staticMod)) {
                            //node.access &= ~Opcodes.ACC_FINAL;

                            //System.out.println(n.name + "$" + node.name + " " + node.access);

                        }
                    }
                }
        ));
        //nodes.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_FINAL) != 0 ? node.access & ~Opcodes.ACC_FINAL : node.access);


        nodes.parallelStream().forEach(cn -> {
            Platform.runLater(() -> body.setText("Hiding calls " + cn.name));

            if ((cn.access & Opcodes.ACC_ENUM) != 0 ||
                    (cn.access & Opcodes.ACC_INTERFACE) != 0)
                return;

            cn.version = V1_8;


            cn.methods.forEach(mn -> {

                if (mn.instructions.size() > 0) {
                    for (AbstractInsnNode ain : mn.instructions.toArray()) {
                        if (ain instanceof MethodInsnNode /*&& Main.settings.invokeDynamic_Methods && ThreadLocalRandom.current().nextInt(100) <= Main.settings.invokeDynamic_Methods_Percentage*/) {
                            MethodInsnNode min = (MethodInsnNode) ain;

                            //if(min.name.contains("<"))
                            //    continue;
                            if (min.name.equals("<init>"))
                                continue;

                            if (min.owner.contains("["))
                                continue;

                            String methodDesk = min.desc;

                            String desk = min.owner.replace("/", ".") + "HURE" + min.name + "HURE" + min.desc + "HURE";

                            MethodHandles.Lookup lookup;

                            if (min.getOpcode() == INVOKESTATIC) {
                                desk += "0";
                            } else if (min.getOpcode() == INVOKEVIRTUAL || min.getOpcode() == INVOKEINTERFACE) {
                                desk += "1";
                            } else if (min.getOpcode() == INVOKESPECIAL) {
                                desk += "2" + "HURE" + cn.name.replace("/", ".");
                            }

                            if(min.getOpcode() == INVOKEVIRTUAL) {
                                //continue;
                            }

                            if (min.getOpcode() != INVOKESTATIC) {
                                methodDesk = methodDesk.replace("(", "(Ljava/lang/Object;");
                            }

                            Type returnType = Type.getReturnType(methodDesk);
                            Type[] args = Type.getArgumentTypes(methodDesk);

                            int i = 0;
                            Type object = Type.getType("Ljava/lang/Object;");
                            for (Type t : args) {
                                if (t.getSort() == Type.OBJECT) {
                                    args[i] = object;
                                }
                                i++;
                            }


                            methodDesk = Type.getMethodDescriptor(returnType, args);
                            //desk = getString(desk,0);

                            //System.out.println(desk + " -> " + getString(desk, "abc"));

                            mn.instructions.set(ain, ain = new InvokeDynamicInsnNode(getString(desk, "abc"), methodDesk, bsmHandle));

                            // if(returnType.getSort() == Type.ARRAY)
                            //     mn.instructions.insert(ain, new TypeInsnNode(CHECKCAST, returnType.getInternalName()));
                        }

                        if (ain instanceof FieldInsnNode && Main.settings.invokeDynamic_Fields && ThreadLocalRandom.current().nextInt(100) <= Main.settings.invokeDynamic_Fields_Percentage) {
                            FieldInsnNode fin = (FieldInsnNode) ain;

                            if (mn.name.contains("<"))
                                continue;


                            Optional<ClassNode> c = nodes.stream().filter(c1 -> c1.name.equals(fin.owner)).findFirst();

                            if(c.isPresent()) {
                                Optional<FieldNode> f = c.get().fields.stream().filter(f1 -> f1.name.equals(fin.name)).findFirst();
                                if(f.isPresent()) {
                                    if((f.get().access & Opcodes.ACC_FINAL) != 0)
                                        continue;
                                }
                            } else {
                                continue;
                            }

                            if(finalFields.contains(fin.owner + ":" + fin.name)) {
                                    System.out.println("IGNORED " + fin.owner + ":" + fin.name);
                                continue;
                            }


                            String desk = fin.owner.replace("/", ".") + "HURE" + fin.name + "HUREHURE";

                            String fieldDesk = "()" + fin.desc;

                            if (fin.getOpcode() == PUTFIELD || fin.getOpcode() == PUTSTATIC) {
                                fieldDesk = "(" + fin.desc + ")V";
                            }

                            if (fin.getOpcode() != GETSTATIC && fin.getOpcode() != PUTSTATIC) {
                                fieldDesk = fieldDesk.replace("(", "(Ljava/lang/Object;");
                            }

                            if (fin.getOpcode() == GETSTATIC) {
                                desk += "3";
                            } else if (fin.getOpcode() == GETFIELD) {
                                desk += "4";
                            } else if (fin.getOpcode() == PUTSTATIC) {
                                desk += "5";
                            } else if (fin.getOpcode() == PUTFIELD) {
                                desk += "6";
                            }

                            desk = getString(desk, "abc");

                            mn.instructions.set(ain, new InvokeDynamicInsnNode(desk, fieldDesk, bsmHandle));

                        }
                    }
                    //list.add(new MethodInsnNode(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));
                }
            });
        });



        ClassNode cn = createBootstrap(names);

        Settings.StringEncryption last = Main.settings.stringEncryption;
        Main.settings.stringEncryption = Settings.StringEncryption.VERYLIGHT;


        //new TaskStringEncryption(1).runTask(Collections.singletonList(cn),null);

        //new TaskFlowObfu3(true).runTask(Collections.singletonList(cn),null);

        Main.settings.stringEncryption = last;
        //nodes.add(JarUtils.getNode(JarUtils.getNodeBytes(cn,new HashMap<>())));
        nodes.add(cn);
    }

    private static String getString(Object in, String b) {
        byte[] array = ((String) in).getBytes(StandardCharsets.ISO_8859_1);
        byte[] out = new byte[array.length];
        byte[] key = b.getBytes();

        for (int i = 0; i < out.length; i++) {
            out[i] = (byte) (array[i] ^ (key[i % key.length]));
        }

        return new String(out, StandardCharsets.ISO_8859_1);
    }

    @Override
    public String getName() {
        return "Invoke Dynamic";
    }

    public MemberNames getDefault() {
        return new MemberNames("aaaaaaa");
    }

    public static ClassNode createBootstrap(MemberNames memberNames) {
        ClassNode cw = new ClassNode();
        MethodVisitor mv;
        FieldVisitor fv;

        cw.visit(V1_8, ACC_PUBLIC + ACC_SUPER, memberNames.className, null, "java/lang/Object", null);

        cw.visitInnerClass("java/lang/invoke/MethodHandles$Lookup", "java/lang/invoke/MethodHandles", "Lookup", ACC_PUBLIC + ACC_FINAL + ACC_STATIC);
        {
            fv = cw.visitField(ACC_PUBLIC + ACC_STATIC, "list", "Ljava/util/HashMap;", "Ljava/util/HashMap<Ljava/lang/Object;Ljava/lang/Object;>;", null);
            fv.visitEnd();
        }
        {
            fv = cw.visitField(ACC_FINAL + ACC_STATIC + ACC_SYNTHETIC, "$assertionsDisabled", "Z", null, null);
            fv.visitEnd();
        }
        /*{
            mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(17, l0);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            mv.visitInsn(RETURN);
            Label l1 = new Label();
            mv.visitLabel(l1);
            mv.visitLocalVariable("this", "L" + memberNames.className + ";", null, l0, l1, 0);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }*/
        {
            mv = cw.visitMethod(ACC_PRIVATE + ACC_STATIC, memberNames.decryptorMethodName, "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(51, l0);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitTypeInsn(CHECKCAST, "java/lang/String");
            mv.visitFieldInsn(GETSTATIC, "java/nio/charset/StandardCharsets", "ISO_8859_1", "Ljava/nio/charset/Charset;");
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "(Ljava/nio/charset/Charset;)[B", false);
            mv.visitVarInsn(ASTORE, 2);
            Label l1 = new Label();
            mv.visitLabel(l1);
            mv.visitLineNumber(52, l1);
            mv.visitVarInsn(ALOAD, 2);
            mv.visitInsn(ARRAYLENGTH);
            mv.visitIntInsn(NEWARRAY, T_BYTE);
            mv.visitVarInsn(ASTORE, 3);
            Label l2 = new Label();
            mv.visitLabel(l2);
            mv.visitLineNumber(53, l2);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "()[B", false);
            mv.visitVarInsn(ASTORE, 4);
            Label l3 = new Label();
            mv.visitLabel(l3);
            mv.visitLineNumber(55, l3);
            mv.visitInsn(ICONST_0);
            mv.visitVarInsn(ISTORE, 5);
            Label l4 = new Label();
            mv.visitLabel(l4);
            //mv.visitFrame(Opcodes.F_FULL, 6, new Object[]{"java/lang/Object", "java/lang/String", "[B", "[B", "[B", Opcodes.INTEGER}, 0, new Object[]{});
            mv.visitVarInsn(ILOAD, 5);
            mv.visitVarInsn(ALOAD, 3);
            mv.visitInsn(ARRAYLENGTH);
            Label l5 = new Label();
            mv.visitJumpInsn(IF_ICMPGE, l5);
            Label l6 = new Label();
            mv.visitLabel(l6);
            mv.visitLineNumber(56, l6);
            mv.visitVarInsn(ALOAD, 3);
            mv.visitVarInsn(ILOAD, 5);
            mv.visitVarInsn(ALOAD, 2);
            mv.visitVarInsn(ILOAD, 5);
            mv.visitInsn(BALOAD);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitVarInsn(ILOAD, 5);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ARRAYLENGTH);
            mv.visitInsn(IREM);
            mv.visitInsn(BALOAD);
            mv.visitInsn(IXOR);
            mv.visitInsn(I2B);
            mv.visitInsn(BASTORE);
            Label l7 = new Label();
            mv.visitLabel(l7);
            mv.visitLineNumber(55, l7);
            mv.visitIincInsn(5, 1);
            mv.visitJumpInsn(GOTO, l4);
            mv.visitLabel(l5);
            mv.visitLineNumber(59, l5);
            //mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
            mv.visitTypeInsn(NEW, "java/lang/String");
            mv.visitInsn(DUP);
            mv.visitVarInsn(ALOAD, 3);
            mv.visitFieldInsn(GETSTATIC, "java/nio/charset/StandardCharsets", "ISO_8859_1", "Ljava/nio/charset/Charset;");
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/String", "<init>", "([BLjava/nio/charset/Charset;)V", false);
            mv.visitInsn(ARETURN);
            Label l8 = new Label();
            mv.visitLabel(l8);
            mv.visitLocalVariable("i", "I", null, l4, l5, 5);
            mv.visitLocalVariable("in", "Ljava/lang/Object;", null, l0, l8, 0);
            mv.visitLocalVariable("b", "Ljava/lang/String;", null, l0, l8, 1);
            mv.visitLocalVariable("array", "[B", null, l1, l8, 2);
            mv.visitLocalVariable("out", "[B", null, l2, l8, 3);
            mv.visitLocalVariable("key", "[B", null, l3, l8, 4);
            mv.visitMaxs(6, 6);
            mv.visitEnd();
        }
        {
            mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, memberNames.bootstrapMethodName, "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", null, null);
            mv.visitCode();
            Label l0 = new Label();
            Label l1 = new Label();
            Label l2 = new Label();
            mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
            mv.visitLabel(l0);
            mv.visitLineNumber(66, l0);
            mv.visitFieldInsn(GETSTATIC, memberNames.className, "list", "Ljava/util/HashMap;");
            mv.visitVarInsn(ALOAD, 1);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/HashMap", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", false);
            mv.visitTypeInsn(CHECKCAST, "java/lang/invoke/MethodHandle");
            mv.visitVarInsn(ASTORE, 3);
            Label l3 = new Label();
            mv.visitLabel(l3);
            mv.visitLineNumber(68, l3);
            mv.visitVarInsn(ALOAD, 3);
            Label l4 = new Label();
            mv.visitJumpInsn(IFNONNULL, l4);
            Label l5 = new Label();
            mv.visitLabel(l5);
            mv.visitLineNumber(69, l5);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitLdcInsn("abc");
            mv.visitMethodInsn(INVOKESTATIC, memberNames.className, memberNames.decryptorMethodName, "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "toString", "()Ljava/lang/String;", false);
            mv.visitLdcInsn("HURE");
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "split", "(Ljava/lang/String;)[Ljava/lang/String;", false);
            mv.visitVarInsn(ASTORE, 4);
            Label l6 = new Label();
            mv.visitLabel(l6);
            mv.visitLineNumber(71, l6);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ICONST_0);
            mv.visitInsn(AALOAD);
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/Class", "forName", "(Ljava/lang/String;)Ljava/lang/Class;", false);
            mv.visitVarInsn(ASTORE, 5);
            Label l7 = new Label();
            mv.visitLabel(l7);
            mv.visitLineNumber(73, l7);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ICONST_1);
            mv.visitInsn(AALOAD);
            mv.visitVarInsn(ASTORE, 6);
            Label l8 = new Label();
            mv.visitLabel(l8);
            mv.visitLineNumber(75, l8);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitTypeInsn(CHECKCAST, "java/lang/invoke/MethodHandles$Lookup");
            mv.visitVarInsn(ASTORE, 7);
            Label l9 = new Label();
            mv.visitLabel(l9);
            mv.visitLineNumber(113, l9);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ICONST_3);
            mv.visitInsn(AALOAD);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "hashCode", "()I", false);
            Label l10 = new Label();
            Label l11 = new Label();
            Label l12 = new Label();
            Label l13 = new Label();
            Label l14 = new Label();
            Label l15 = new Label();
            Label l16 = new Label();
            Label l17 = new Label();
            mv.visitTableSwitchInsn(48, 54, l17, new Label[]{l10, l11, l12, l13, l14, l15, l16});
            mv.visitLabel(l10);
            mv.visitLineNumber(115, l10);
            //mv.visitFrame(Opcodes.F_FULL, 8, new Object[]{"java/lang/Object", "java/lang/Object", "java/lang/Object", "java/lang/invoke/MethodHandle", "[Ljava/lang/String;", "java/lang/Class", "java/lang/String", "java/lang/invoke/MethodHandles$Lookup"}, 0, new Object[]{});
            mv.visitVarInsn(ALOAD, 7);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ICONST_2);
            mv.visitInsn(AALOAD);
            mv.visitLdcInsn(Type.getType("L" + memberNames.className + ";"));
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getClassLoader", "()Ljava/lang/ClassLoader;", false);
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/invoke/MethodType", "fromMethodDescriptorString", "(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/invoke/MethodType;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/invoke/MethodHandles$Lookup", "findStatic", "(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/MethodHandle;", false);
            mv.visitVarInsn(ASTORE, 3);
            Label l18 = new Label();
            mv.visitLabel(l18);
            mv.visitLineNumber(116, l18);
            mv.visitJumpInsn(GOTO, l17);
            mv.visitLabel(l11);
            mv.visitLineNumber(118, l11);
            //mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            mv.visitVarInsn(ALOAD, 7);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ICONST_2);
            mv.visitInsn(AALOAD);
            mv.visitLdcInsn(Type.getType("L" + memberNames.className + ";"));
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getClassLoader", "()Ljava/lang/ClassLoader;", false);
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/invoke/MethodType", "fromMethodDescriptorString", "(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/invoke/MethodType;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/invoke/MethodHandles$Lookup", "findVirtual", "(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/MethodHandle;", false);
            mv.visitVarInsn(ASTORE, 3);
            Label l19 = new Label();
            mv.visitLabel(l19);
            mv.visitLineNumber(119, l19);
            mv.visitJumpInsn(GOTO, l17);
            mv.visitLabel(l12);
            mv.visitLineNumber(121, l12);
            //mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            mv.visitVarInsn(ALOAD, 7);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ICONST_2);
            mv.visitInsn(AALOAD);
            mv.visitLdcInsn(Type.getType("L" + memberNames.className + ";"));
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getClassLoader", "()Ljava/lang/ClassLoader;", false);
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/invoke/MethodType", "fromMethodDescriptorString", "(Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/invoke/MethodType;", false);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ICONST_4);
            mv.visitInsn(AALOAD);
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/Class", "forName", "(Ljava/lang/String;)Ljava/lang/Class;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/invoke/MethodHandles$Lookup", "findSpecial", "(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/Class;)Ljava/lang/invoke/MethodHandle;", false);
            mv.visitVarInsn(ASTORE, 3);
            Label l20 = new Label();
            mv.visitLabel(l20);
            mv.visitLineNumber(122, l20);
            mv.visitJumpInsn(GOTO, l17);
            mv.visitLabel(l13);
            mv.visitLineNumber(124, l13);
            //mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitMethodInsn(INVOKESTATIC, memberNames.className, memberNames.publicFieldMethodName, "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;", false);
            mv.visitVarInsn(ASTORE, 8);
            Label l21 = new Label();
            mv.visitLabel(l21);
            mv.visitLineNumber(125, l21);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitJumpInsn(IFNULL, l17);
            Label l22 = new Label();
            mv.visitLabel(l22);
            mv.visitLineNumber(126, l22);
            mv.visitVarInsn(ALOAD, 7);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/reflect/Field", "getType", "()Ljava/lang/Class;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/invoke/MethodHandles$Lookup", "findStaticGetter", "(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/invoke/MethodHandle;", false);
            mv.visitVarInsn(ASTORE, 3);
            mv.visitJumpInsn(GOTO, l17);
            mv.visitLabel(l14);
            mv.visitLineNumber(130, l14);
            //mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitMethodInsn(INVOKESTATIC, memberNames.className, memberNames.publicFieldMethodName, "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;", false);
            mv.visitVarInsn(ASTORE, 8);
            Label l23 = new Label();
            mv.visitLabel(l23);
            mv.visitLineNumber(131, l23);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitJumpInsn(IFNULL, l17);
            Label l24 = new Label();
            mv.visitLabel(l24);
            mv.visitLineNumber(132, l24);
            mv.visitVarInsn(ALOAD, 7);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/reflect/Field", "getType", "()Ljava/lang/Class;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/invoke/MethodHandles$Lookup", "findGetter", "(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/invoke/MethodHandle;", false);
            mv.visitVarInsn(ASTORE, 3);
            mv.visitJumpInsn(GOTO, l17);
            mv.visitLabel(l15);
            mv.visitLineNumber(136, l15);
            //mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitMethodInsn(INVOKESTATIC, memberNames.className, memberNames.publicFieldMethodName, "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;", false);
            mv.visitVarInsn(ASTORE, 8);
            Label l25 = new Label();
            mv.visitLabel(l25);
            mv.visitLineNumber(137, l25);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitJumpInsn(IFNULL, l17);
            Label l26 = new Label();
            mv.visitLabel(l26);
            mv.visitLineNumber(138, l26);
            mv.visitVarInsn(ALOAD, 7);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/reflect/Field", "getType", "()Ljava/lang/Class;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/invoke/MethodHandles$Lookup", "findStaticSetter", "(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/invoke/MethodHandle;", false);
            mv.visitVarInsn(ASTORE, 3);
            mv.visitJumpInsn(GOTO, l17);
            mv.visitLabel(l16);
            mv.visitLineNumber(142, l16);
            //mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitMethodInsn(INVOKESTATIC, memberNames.className, memberNames.publicFieldMethodName, "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;", false);
            mv.visitVarInsn(ASTORE, 8);
            Label l27 = new Label();
            mv.visitLabel(l27);
            mv.visitLineNumber(143, l27);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitJumpInsn(IFNULL, l17);
            Label l28 = new Label();
            mv.visitLabel(l28);
            mv.visitLineNumber(144, l28);
            mv.visitVarInsn(ALOAD, 7);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/reflect/Field", "getType", "()Ljava/lang/Class;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/invoke/MethodHandles$Lookup", "findSetter", "(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/invoke/MethodHandle;", false);
            mv.visitVarInsn(ASTORE, 3);
            mv.visitLabel(l17);
            mv.visitLineNumber(150, l17);
            //mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            mv.visitFieldInsn(GETSTATIC, memberNames.className, "list", "Ljava/util/HashMap;");
            mv.visitVarInsn(ALOAD, 1);
            mv.visitVarInsn(ALOAD, 3);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/HashMap", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", false);
            mv.visitInsn(POP);
            mv.visitLabel(l4);
            mv.visitLineNumber(153, l4);
            //mv.visitFrame(Opcodes.F_FULL, 4, new Object[]{"java/lang/Object", "java/lang/Object", "java/lang/Object", "java/lang/invoke/MethodHandle"}, 0, new Object[]{});
            mv.visitTypeInsn(NEW, "java/lang/invoke/ConstantCallSite");
            mv.visitInsn(DUP);
            mv.visitVarInsn(ALOAD, 3);
            mv.visitMethodInsn(INVOKESTATIC, "java/util/Objects", "requireNonNull", "(Ljava/lang/Object;)Ljava/lang/Object;", false);
            mv.visitTypeInsn(CHECKCAST, "java/lang/invoke/MethodHandle");
            mv.visitVarInsn(ALOAD, 2);
            mv.visitTypeInsn(CHECKCAST, "java/lang/invoke/MethodType");
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/invoke/MethodHandle", "asType", "(Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/MethodHandle;", false);
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/invoke/ConstantCallSite", "<init>", "(Ljava/lang/invoke/MethodHandle;)V", false);
            mv.visitLabel(l1);
            mv.visitInsn(ARETURN);
            mv.visitLabel(l2);
            mv.visitLineNumber(154, l2);
            //mv.visitFrame(Opcodes.F_FULL, 3, new Object[]{"java/lang/Object", "java/lang/Object", "java/lang/Object"}, 1, new Object[]{"java/lang/Exception"});
            mv.visitVarInsn(ASTORE, 3);
            Label l29 = new Label();
            mv.visitLabel(l29);
            mv.visitLineNumber(155, l29);
            mv.visitVarInsn(ALOAD, 3);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Exception", "printStackTrace", "()V", false);
            Label l30 = new Label();
            mv.visitLabel(l30);
            mv.visitLineNumber(156, l30);
            mv.visitInsn(ACONST_NULL);
            mv.visitInsn(ARETURN);
            Label l31 = new Label();
            mv.visitLabel(l31);
            mv.visitLocalVariable("field", "Ljava/lang/reflect/Field;", null, l21, l14, 8);
            mv.visitLocalVariable("field", "Ljava/lang/reflect/Field;", null, l23, l15, 8);
            mv.visitLocalVariable("field", "Ljava/lang/reflect/Field;", null, l25, l16, 8);
            mv.visitLocalVariable("field", "Ljava/lang/reflect/Field;", null, l27, l17, 8);
            mv.visitLocalVariable("split", "[Ljava/lang/String;", null, l6, l4, 4);
            mv.visitLocalVariable("targetClass", "Ljava/lang/Class;", "Ljava/lang/Class<*>;", l7, l4, 5);
            mv.visitLocalVariable("name", "Ljava/lang/String;", null, l8, l4, 6);
            mv.visitLocalVariable("lookup", "Ljava/lang/invoke/MethodHandles$Lookup;", null, l9, l4, 7);
            mv.visitLocalVariable("methodHandle", "Ljava/lang/invoke/MethodHandle;", null, l3, l2, 3);
            mv.visitLocalVariable("t", "Ljava/lang/Exception;", null, l29, l31, 3);
            mv.visitLocalVariable("lookupIn", "Ljava/lang/Object;", null, l0, l31, 0);
            mv.visitLocalVariable("infos", "Ljava/lang/Object;", null, l0, l31, 1);
            mv.visitLocalVariable("methodtype", "Ljava/lang/Object;", null, l0, l31, 2);
            mv.visitMaxs(6, 9);
            mv.visitEnd();
        }
        {
            mv = cw.visitMethod(ACC_PRIVATE + ACC_STATIC, memberNames.publicFieldMethodName, "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;", "(Ljava/lang/Class<*>;Ljava/lang/String;)Ljava/lang/reflect/Field;", null);
            mv.visitCode();
            Label l0 = new Label();
            Label l1 = new Label();
            Label l2 = new Label();
            mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Exception");
            Label l3 = new Label();
            Label l4 = new Label();
            Label l5 = new Label();
            mv.visitTryCatchBlock(l3, l4, l5, "java/lang/NullPointerException");
            mv.visitLabel(l0);
            mv.visitLineNumber(162, l0);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getDeclaredField", "(Ljava/lang/String;)Ljava/lang/reflect/Field;", false);
            mv.visitVarInsn(ASTORE, 2);
            Label l6 = new Label();
            mv.visitLabel(l6);
            mv.visitLineNumber(163, l6);
            mv.visitVarInsn(ALOAD, 2);
            mv.visitInsn(ICONST_1);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/reflect/Field", "setAccessible", "(Z)V", false);
            Label l7 = new Label();
            mv.visitLabel(l7);
            mv.visitLineNumber(165, l7);
            mv.visitVarInsn(ALOAD, 2);
            mv.visitLabel(l1);
            mv.visitInsn(ARETURN);
            mv.visitLabel(l2);
            mv.visitLineNumber(166, l2);
            //mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{"java/lang/Exception"});
            mv.visitVarInsn(ASTORE, 2);
            mv.visitLabel(l3);
            mv.visitLineNumber(168, l3);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getSuperclass", "()Ljava/lang/Class;", false);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitMethodInsn(INVOKESTATIC, memberNames.className, memberNames.publicFieldMethodName, "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;", false);
            mv.visitLabel(l4);
            mv.visitInsn(ARETURN);
            mv.visitLabel(l5);
            mv.visitLineNumber(169, l5);
            //mv.visitFrame(Opcodes.F_FULL, 3, new Object[]{"java/lang/Class", "java/lang/String", "java/lang/Exception"}, 1, new Object[]{"java/lang/NullPointerException"});
            mv.visitVarInsn(ASTORE, 3);
            Label l8 = new Label();
            mv.visitLabel(l8);
            mv.visitLineNumber(170, l8);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getInterfaces", "()[Ljava/lang/Class;", false);
            mv.visitVarInsn(ASTORE, 4);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ARRAYLENGTH);
            mv.visitVarInsn(ISTORE, 5);
            mv.visitInsn(ICONST_0);
            mv.visitVarInsn(ISTORE, 6);
            Label l9 = new Label();
            mv.visitLabel(l9);
            //mv.visitFrame(Opcodes.F_FULL, 7, new Object[]{"java/lang/Class", "java/lang/String", "java/lang/Exception", "java/lang/NullPointerException", "[Ljava/lang/Class;", Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[]{});
            mv.visitVarInsn(ILOAD, 6);
            mv.visitVarInsn(ILOAD, 5);
            Label l10 = new Label();
            mv.visitJumpInsn(IF_ICMPGE, l10);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitVarInsn(ILOAD, 6);
            mv.visitInsn(AALOAD);
            mv.visitVarInsn(ASTORE, 7);
            Label l11 = new Label();
            mv.visitLabel(l11);
            mv.visitLineNumber(171, l11);
            mv.visitVarInsn(ALOAD, 7);
            mv.visitVarInsn(ALOAD, 1);
            mv.visitMethodInsn(INVOKESTATIC, memberNames.className, memberNames.publicFieldMethodName, "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Field;", false);
            mv.visitVarInsn(ASTORE, 8);
            Label l12 = new Label();
            mv.visitLabel(l12);
            mv.visitLineNumber(172, l12);
            mv.visitVarInsn(ALOAD, 8);
            Label l13 = new Label();
            mv.visitJumpInsn(IFNULL, l13);
            Label l14 = new Label();
            mv.visitLabel(l14);
            mv.visitLineNumber(173, l14);
            mv.visitVarInsn(ALOAD, 8);
            mv.visitInsn(ARETURN);
            mv.visitLabel(l13);
            mv.visitLineNumber(170, l13);
            //mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            mv.visitIincInsn(6, 1);
            mv.visitJumpInsn(GOTO, l9);
            mv.visitLabel(l10);
            mv.visitLineNumber(177, l10);
            //mv.visitFrame(Opcodes.F_FULL, 3, new Object[]{"java/lang/Class", "java/lang/String", "java/lang/Exception"}, 0, new Object[]{});
            mv.visitInsn(ACONST_NULL);
            mv.visitInsn(ARETURN);
            Label l15 = new Label();
            mv.visitLabel(l15);
            mv.visitLocalVariable("declaredField", "Ljava/lang/reflect/Field;", null, l6, l2, 2);
            mv.visitLocalVariable("f", "Ljava/lang/reflect/Field;", null, l12, l13, 8);
            mv.visitLocalVariable("anInterface", "Ljava/lang/Class;", null, l11, l13, 7);
            mv.visitLocalVariable("ex2", "Ljava/lang/NullPointerException;", null, l8, l10, 3);
            mv.visitLocalVariable("ex", "Ljava/lang/Exception;", null, l3, l15, 2);
            mv.visitLocalVariable("clazz", "Ljava/lang/Class;", "Ljava/lang/Class<*>;", l0, l15, 0);
            mv.visitLocalVariable("s", "Ljava/lang/String;", null, l0, l15, 1);
            mv.visitMaxs(2, 9);
            mv.visitEnd();
        }

        {
            mv = cw.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(62, l0);
            mv.visitTypeInsn(NEW, "java/util/HashMap");
            mv.visitInsn(DUP);
            mv.visitMethodInsn(INVOKESPECIAL, "java/util/HashMap", "<init>", "()V", false);
            mv.visitFieldInsn(PUTSTATIC, memberNames.className, "list", "Ljava/util/HashMap;");
            mv.visitInsn(RETURN);
            mv.visitMaxs(2, 0);
            mv.visitEnd();
        }
        cw.visitEnd();

        return cw;
    }

    private class MemberNames {
        private String className;
        private String decryptorMethodName;
        private String bootstrapMethodName;
        private String publicFieldMethodName;

        private MemberNames(String classname) {
            this.className = classname;
            this.decryptorMethodName = generateString(rnd, 10, true);
            this.bootstrapMethodName = generateString(rnd, 10, true);
            this.publicFieldMethodName = generateString(rnd, 10, true);
        }
    }
}
