package sexy.poke.tasks.obfuscation.impl.removeal;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 11.10.2018
 */
public class TaskInnerOuterRemover extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        nodes.parallelStream().forEach(cn -> {
            if(Main.settings.innerClassesRemoval != Settings.RemoveState.OFF) {

                Platform.runLater(() -> body.setText("Removing innerClasses in class " + cn.name));

                cn.innerClasses.clear();
            }
            if(Main.settings.outerClassesRemoval != Settings.RemoveState.OFF) {

                Platform.runLater(() -> body.setText("Removing outerClasses in class " + cn.name));

                cn.outerClass = null;
            }
            if(Main.settings.outerMethodsRemoval != Settings.RemoveState.OFF) {

                Platform.runLater(() -> body.setText("Removing outerMethods in class " + cn.name));

                cn.outerMethod = null;
                cn.outerMethodDesc = null;
            }
        });
    }

    @Override
    public String getName() {
        return "InnerOuterRemover";
    }
}
