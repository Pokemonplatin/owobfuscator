package sexy.poke.tasks.obfuscation.impl.flow;

import sexy.poke.tasks.obfuscation.*;
import sexy.poke.mappings.*;
import javafx.scene.text.*;
import org.objectweb.asm.*;

import java.util.*;

import org.objectweb.asm.tree.analysis.*;
import org.objectweb.asm.tree.*;

public class TaskSwitchCase extends AbstractObfuscationTask {
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        for (ClassNode cn : nodes) {
            for (MethodNode mn : cn.methods) {
                if (mn.instructions.getFirst() != null) {
                    if (mn.name.contains("<")) {
                        continue;
                    }
                    Analyzer<BasicValue> analyzer = new Analyzer<BasicValue>(new BasicInterpreter());
                    try {
                        analyzer.analyze(cn.name, mn);
                    } catch (AnalyzerException e) {
                        e.printStackTrace();
                    }
                    Frame<BasicValue>[] frames = analyzer.getFrames();
                    List<List<AbstractInsnNode>> packages = new ArrayList<List<AbstractInsnNode>>();
                    List<AbstractInsnNode> tempPackages = new ArrayList<AbstractInsnNode>();
                    List<LabelNode> labels = new ArrayList<LabelNode>();
                    int inst = -1;

                    int finalBlocks = 0;


                    List<TryCatchBlockNode>[] handlers = new List[mn.instructions.size()];

                    // For each exception handler, and each instruction within its range, record in 'handlers' the
                    // fact that execution can flow from this instruction to the exception handler.
                    for (int i = 0; i < mn.tryCatchBlocks.size(); ++i) {
                        TryCatchBlockNode tryCatchBlock = mn.tryCatchBlocks.get(i);
                        int startIndex = mn.instructions.indexOf(tryCatchBlock.start);
                        int endIndex = mn.instructions.indexOf(tryCatchBlock.end);
                        if (tryCatchBlock.type == null) {
                            endIndex = mn.instructions.indexOf(tryCatchBlock.handler) + 1;
                            finalBlocks++;
                        }
                        for (int j = startIndex; j < endIndex; ++j) {
                            List<TryCatchBlockNode> insnHandlers = handlers[j];
                            if (insnHandlers == null) {
                                insnHandlers = new ArrayList<>();
                                handlers[j] = insnHandlers;
                            }
                            insnHandlers.add(tryCatchBlock);
                        }
                    }

                    //final block idk what to do so skip whole method
                    if (finalBlocks != 0)
                        continue;

                    /*for (AbstractInsnNode ain : mn.instructions.toArray()) {
                        ++inst;
                        if (ain instanceof LabelNode && !this.isLabelInUse(ain, mn, true) || ain instanceof LineNumberNode || ain instanceof FrameNode)
                            continue;
                        if (ain instanceof LabelNode) {
                            labels.add((LabelNode) ain);
                        }


                        if (frames[Math.max(inst, 0)].getStackSize() == 0 && handlers[inst] == null && !this.isLabelInUse(ain, mn, true) && !(ain instanceof LineNumberNode)) {
                            packages.add(tempPackages);
                            tempPackages = new ArrayList<>();
                            tempPackages.add(ain);
                            continue;
                        }
                        tempPackages.add(ain);
                    }*/

                    for (AbstractInsnNode ain : mn.instructions.toArray()) {
                        ++inst;
                        if (ain instanceof FrameNode)
                            continue;

                        Frame f = frames[Math.max(inst, 0)];

                        if (f != null && f.getStackSize() == 0 && handlers[inst] == null && !(ain instanceof LabelNode) && !(ain instanceof LineNumberNode) && !(ain instanceof JumpInsnNode)) {
                            packages.add(tempPackages);
                            tempPackages = new ArrayList<>();
                            tempPackages.add(ain);
                            continue;
                        }
                        tempPackages.add(ain);
                    }

                    packages.add(tempPackages);
                    InsnList insList = new InsnList();

                    LabelNode end = new LabelNode();
                    LabelNode def = new LabelNode();
                    LabelNode exit = new LabelNode();
                    LabelNode beginning = new LabelNode();
                    LabelNode[] lNodes = new LabelNode[packages.size()];

                    int[] keys = new int[packages.size()];
                    for (int i = 0; i < packages.size(); ++i) {
                        lNodes[i] = new LabelNode();
                        keys[i] = i;
                    }

                    Type t = Type.getMethodType(mn.desc);
                    List<Integer> usedVars = new ArrayList<Integer>();
                    TableSwitchInsnNode node = new TableSwitchInsnNode(0, packages.size() - 1, def, lNodes);
                    int counter = ++mn.maxLocals;

                    insList.add(getIntPush(0));
                    insList.add(new VarInsnNode(Opcodes.ISTORE, counter));
                    insList.add(beginning);
                    insList.add(new VarInsnNode(Opcodes.ILOAD, counter));
                    insList.add(getIntPush(packages.size()));
                    insList.add(new JumpInsnNode(Opcodes.IF_ICMPGE, exit));
                    insList.add(new VarInsnNode(Opcodes.ILOAD, counter));
                    insList.add(node);

                    int nodeInc = 0;
                    for (List<AbstractInsnNode> list : packages) {
                        insList.add(lNodes[nodeInc++]);
                        boolean hasLabel = false;
                        for (AbstractInsnNode ain2 : list) {
                            if (ain2.getOpcode() == Opcodes.ASTORE || ain2.getOpcode() == Opcodes.ALOAD) {
                                usedVars.add(((VarInsnNode) ain2).var);
                            }
                            insList.add(ain2);
                            if (ain2 instanceof LabelNode) {
                                hasLabel = true;
                            }
                        }
                        if (hasLabel) {
                            insList.add(getIntPush(nodeInc - 1));
                            insList.add(new VarInsnNode(Opcodes.ISTORE, counter));
                        }
                        insList.add(new JumpInsnNode(Opcodes.GOTO, end));
                    }

                    insList.add(def);
                    insList.add(end);
                    insList.add(new IincInsnNode(counter, 1));
                    insList.add(new JumpInsnNode(Opcodes.GOTO, beginning));
                    insList.add(exit);
                    insList.add(this.exitList(mn));


                    HashMap<Integer, Integer> intMap = new HashMap<Integer, Integer>();
                    HashMap<Integer, Integer> longMap = new HashMap<Integer, Integer>();
                    HashMap<Integer, Integer> doubleMap = new HashMap<Integer, Integer>();
                    HashMap<Integer, Integer> floatMap = new HashMap<Integer, Integer>();

                    for (int j = 0; j < mn.maxLocals - 1; ++j) {
                        if (j >= this.getVarSpace(t.getArgumentTypes()) + (((mn.access & Opcodes.ACC_STATIC) == 0) ? 1 : 0)) {
                            if (usedVars.contains(j)) {
                                insList.insertBefore(insList.getFirst(), new VarInsnNode(Opcodes.ASTORE, j));
                                insList.insertBefore(insList.getFirst(), new InsnNode(Opcodes.ACONST_NULL));
                            }
                        }
                    }

                    mn.instructions = insList;

                    for (AbstractInsnNode ain3 : insList.toArray()) {
                        if (ain3 instanceof VarInsnNode) {
                            VarInsnNode var = (VarInsnNode) ain3;
                            if (var.var != counter) {
                                if (var.var >= this.getVarSpace(t.getArgumentTypes()) + (((mn.access & Opcodes.ACC_STATIC) == 0) ? 1 : 0)) {
                                    if (var.getOpcode() == Opcodes.ILOAD || var.getOpcode() == Opcodes.ISTORE) {
                                        if (intMap.containsKey(var.var)) {
                                            var.var = intMap.get(var.var);
                                        } else {
                                            intMap.put(var.var, ++mn.maxLocals);
                                            var.var = mn.maxLocals;
                                        }
                                    }
                                    if (var.getOpcode() == Opcodes.LLOAD || var.getOpcode() == Opcodes.LSTORE) {
                                        if (longMap.containsKey(var.var)) {
                                            var.var = longMap.get(var.var);
                                        } else {
                                            longMap.put(var.var, ++mn.maxLocals);
                                            var.var = mn.maxLocals++;
                                        }
                                    }
                                    if (var.getOpcode() == Opcodes.DLOAD || var.getOpcode() == Opcodes.DSTORE) {
                                        if (doubleMap.containsKey(var.var)) {
                                            var.var = doubleMap.get(var.var);
                                        } else {
                                            doubleMap.put(var.var, ++mn.maxLocals);
                                            var.var = mn.maxLocals++;
                                        }
                                    }
                                    if (var.getOpcode() == Opcodes.FLOAD || var.getOpcode() == Opcodes.FSTORE) {
                                        if (floatMap.containsKey(var.var)) {
                                            var.var = floatMap.get(var.var);
                                        } else {
                                            floatMap.put(var.var, ++mn.maxLocals);
                                            var.var = mn.maxLocals;
                                        }
                                    }
                                }
                            }
                        } else if (ain3 instanceof IincInsnNode) {
                            IincInsnNode iinc = (IincInsnNode) ain3;
                            if (iinc.var != counter) {
                                if (iinc.var >= this.getVarSpace(t.getArgumentTypes()) + (((mn.access & Opcodes.ACC_STATIC) == 0) ? 1 : 0)) {
                                    if (intMap.containsKey(iinc.var)) {
                                        iinc.var = intMap.get(iinc.var);
                                    } else {
                                        intMap.put(iinc.var, ++mn.maxLocals);
                                        iinc.var = mn.maxLocals;
                                    }
                                }
                            }
                        }
                    }
                    for (int var2 : intMap.values()) {
                        insList.insertBefore(insList.getFirst(), new VarInsnNode(Opcodes.ISTORE, var2));
                        insList.insertBefore(insList.getFirst(), getIntPush(0));
                    }
                    for (int var2 : longMap.values()) {
                        insList.insertBefore(insList.getFirst(), new VarInsnNode(Opcodes.LSTORE, var2));
                        insList.insertBefore(insList.getFirst(), getIntPush(0L));
                    }
                    for (int var2 : doubleMap.values()) {
                        insList.insertBefore(insList.getFirst(), new VarInsnNode(Opcodes.DSTORE, var2));
                        insList.insertBefore(insList.getFirst(), getIntPush(0.0));
                    }
                    for (int var2 : floatMap.values()) {
                        insList.insertBefore(insList.getFirst(), new VarInsnNode(Opcodes.FSTORE, var2));
                        insList.insertBefore(insList.getFirst(), getIntPush(0f));
                    }

                    for (TryCatchBlockNode tr : mn.tryCatchBlocks) {
                        boolean search = false;
                        for (AbstractInsnNode ain4 : insList.toArray()) {
                            if (ain4.equals(tr.handler)) {
                                search = true;
                            }
                            if (search && ain4 instanceof VarInsnNode && ain4.getOpcode() == Opcodes.ASTORE) {
                                int last = ((VarInsnNode) ain4).var;
                                ((VarInsnNode) ain4).var = ++mn.maxLocals;
                                insList.insert(ain4, ain4 = new VarInsnNode(Opcodes.ALOAD, mn.maxLocals));
                                insList.insert(ain4, ain4 = new VarInsnNode(Opcodes.ASTORE, last));
                                break;
                            }
                        }
                    }
                }
                if (mn.instructions.getFirst() != null && false) {

                    mn.instructions.insert(new MethodInsnNode(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));
                    mn.instructions.insert(new LdcInsnNode(cn.name + "." + mn.name + mn.desc));
                    mn.instructions.insert(new FieldInsnNode(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));

                }
            }
        }
    }

    public int getVarSpace(Type[] types) {
        int ammount = 0;
        for (Type t : types) {
            ammount += t.getSize();
        }
        return ammount;
    }

    public boolean isJump(AbstractInsnNode ain) {
        if (ain instanceof LabelNode || ain instanceof FrameNode || ain instanceof LineNumberNode) {
            return this.isJump(ain.getNext());
        }
        return ain instanceof JumpInsnNode || (ain != null && ain.getOpcode() >= 172 && ain.getOpcode() <= 177);
    }

    public boolean isLabelInUse(AbstractInsnNode ln, MethodNode mn, boolean ignoreLineNumbers) {
        if (!(ln instanceof LabelNode)) {
            return false;
        }
        for (TryCatchBlockNode n : mn.tryCatchBlocks) {
            if (n.handler.equals(ln) || n.end.equals(ln) || n.start.equals(ln)) {
                return true;
            }
        }
        for (AbstractInsnNode ain : mn.instructions.toArray()) {
            if (ain instanceof JumpInsnNode && ((JumpInsnNode) ain).label.equals(ln)) {
                return true;
            }
            if (ain instanceof TableSwitchInsnNode) {
                TableSwitchInsnNode ts = (TableSwitchInsnNode) ain;
                if (ts.dflt.equals(ln) || ts.labels.contains(ln)) {
                    return true;
                }
            }
            if (ain instanceof LookupSwitchInsnNode) {
                LookupSwitchInsnNode ts2 = (LookupSwitchInsnNode) ain;
                if (ts2.dflt.equals(ln) || ts2.labels.contains(ln)) {
                    return true;
                }
            }
            if (!ignoreLineNumbers && ain instanceof LineNumberNode) {
                LineNumberNode lnn = (LineNumberNode) ain;
                if (lnn.start.equals(ln)) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getName() {
        return null;
    }
}
