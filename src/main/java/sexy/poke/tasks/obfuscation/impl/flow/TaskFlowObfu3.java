package sexy.poke.tasks.obfuscation.impl.flow;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.tree.analysis.*;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.mappings.SmallFieldNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Flo on 10.08.2018
 */
public class TaskFlowObfu3 extends AbstractObfuscationTask {

    public TaskFlowObfu3(boolean extraAggressive) {
        this.extraAggressive = extraAggressive;
    }

    private boolean extraAggressive;

    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        //HashMap<String, SmallFieldNode> staticFields = new HashMap<>();

        List<SmallFieldNode> staticFields = new ArrayList<>();

        for (ClassNode cn : nodes) {

            for(int i = 0; i < rnd.nextInt(4) + 1; i++) {
                if ((cn.access & Opcodes.ACC_ENUM) != 0 ||
                        (cn.access & Opcodes.ACC_INTERFACE) != 0 ||
                        cn.name.contains("$"))
                    continue;

                String name = generateString(rnd, 10, true);


                FieldNode fn = null;
                switch (rnd.nextInt(7)) {
                    case 0:
                        cn.fields.add(fn = new FieldNode(ACC_PUBLIC | ACC_STATIC, name, "B", null, null));
                        break;
                    case 1:
                        cn.fields.add(fn = new FieldNode(ACC_PUBLIC | ACC_STATIC, name, "C", null, null));
                        break;
                    case 2:
                        cn.fields.add(fn = new FieldNode(ACC_PUBLIC | ACC_STATIC, name, "I", null, null));
                        break;
                    case 3:
                        cn.fields.add(fn = new FieldNode(ACC_PUBLIC | ACC_STATIC, name, "I", null, null));
                        break;
                    case 4:
                        cn.fields.add(fn = new FieldNode(ACC_PUBLIC | ACC_STATIC, name, "I", null, null));
                        break;
                    case 5:
                        cn.fields.add(fn = new FieldNode(ACC_PUBLIC | ACC_STATIC, name, "S", null, null));
                        break;
                    case 6:
                        cn.fields.add(fn = new FieldNode(ACC_PUBLIC | ACC_STATIC, name, "Z", null, null));
                        break;
                }



                staticFields.add(new SmallFieldNode(new SmallClassNode(cn, true,false),fn));
                //staticFields.put(cn.name, new SmallFieldNode(new SmallClassNode(cn),fn));
            }
        }

        nodes.parallelStream().forEach(cn -> {
            if ((cn.access & Opcodes.ACC_ENUM) != 0 ||
                    (cn.access & Opcodes.ACC_INTERFACE) != 0)
                return;

            int fields = rnd.nextInt(4) + 3;

            //Set<Map.Entry<String, SmallFieldNode>> ExcludeGui = staticFields.entrySet();
            //Object[] thisFileds = ExcludeGui.toArray();

            cn.methods.parallelStream().forEach(mn -> {
                if(mn.instructions.getFirst() == null)
                    return;

                if(body != null)
                    Platform.runLater(() -> body.setText("Applying FlowObfuscation in " + cn.name));

                //List<AbstractInsnNode> initLocalsStack = new ArrayList<>();
                List<AbstractInsnNode> putBelow = new ArrayList<>();

                AbstractInsnNode[] origInstructs = mn.instructions.toArray();

                boolean calledSuper = false;


                int indextmp = rnd.nextInt(staticFields.size());
                SmallFieldNode sfn = staticFields.get(indextmp);

                int var = mn.maxLocals;


                if(sfn.getDesk().equals("D")) {
                    mn.instructions.insertBefore(mn.instructions
                            .getFirst(), new VarInsnNode(DSTORE, mn.maxLocals++));
                } else if(sfn.getDesk().equals("F")) {
                    mn.instructions.insertBefore(mn.instructions
                            .getFirst(), new VarInsnNode(FSTORE, mn.maxLocals++));
                } else if(sfn.getDesk().equals("J")) {
                    mn.instructions.insertBefore(mn.instructions
                            .getFirst(), new VarInsnNode(LSTORE, mn.maxLocals++));
                } else {
                    mn.instructions.insertBefore(mn.instructions
                            .getFirst(), new VarInsnNode(ISTORE, mn.maxLocals++));
                }





                mn.instructions.insertBefore(mn.instructions
                        .getFirst(), new FieldInsnNode(GETSTATIC,
                        sfn.getOwner().getClassName(), sfn.getName(), sfn.getDesk()));
                mn.instructions.insertBefore(mn.instructions
                        .getFirst(), new LabelNode());


                Analyzer<BasicValue> analyzer = new Analyzer<>(new BasicInterpreter());

                try {
                    analyzer.analyze(cn.name, mn);
                } catch (AnalyzerException e) {
                    e.printStackTrace();
                }

                Frame<BasicValue>[] frames = analyzer.getFrames();

                //StackEmulator analyzer = new StackEmulator(mn);


                for (AbstractInsnNode ain : origInstructs) {

                    if (mn.name.equals("<init>")) {
                        if (ain instanceof MethodInsnNode) {
                            if (ain.getOpcode() == INVOKESPECIAL
                                    && ain.getPrevious() instanceof VarInsnNode
                                    && ((VarInsnNode) ain.getPrevious()).var == 0) {
                                calledSuper = true;
                                continue;
                            }
                        }
                    }

                    //if (ain != origInstructs[0] && ain instanceof FrameNode)
                    //    break;

                    if (mn.name.equals("<init>") && !calledSuper)
                        continue;

                    //StackAnalyzer analyzer = new StackAnalyzer(mn, ain);
                    //if (analyzer.returnStackAtBreak().isEmpty())
                    //    initLocalsStack.add(ain);
                }

                //if (initLocalsStack.isEmpty()) {
                //    continue;
                //}


                //List<Integer> vars = new ArrayList<>();


                int locals = mn.maxLocals += fields;

                List<LabelNode> labels = Stream.of(mn.instructions.toArray()).filter(o -> o instanceof LabelNode).map(o -> (LabelNode) o).collect(Collectors.toList());


                /*for(int i = 0; i < fields; i++) {
                    int index = rnd.nextInt(thisFileds.length);

                    InsnList list = new InsnList();

                    //list.add(new LabelNode());
                    list.add(new FieldInsnNode(GETSTATIC,
                            ((Map.Entry<String, String>)thisFileds[index]).getKey(), ((Map.Entry<String, String>)thisFileds[index]).getValue(), "I"));
                    list.add(new VarInsnNode(ISTORE, locals));

                    mn.instructions.insertBefore(initLocalsStack.get(rnd.nextInt(initLocalsStack.size())),list);

                    //System.out.println("added to " + mn.name + " " + cn.name);

                    vars.add(locals--);
                }*/


                //boolean inFirst;
                calledSuper = false;

                //LabelNode exit = exitLabel(mn);
                LabelNode lastFrame = null;

                //LabelNode first = new LabelNode();

                //StackAnalyzer a = new StackAnalyzer(mn, labels.get(rnd.nextInt(labels.size())));

                HashMap<AbstractInsnNode, InsnList> addAfter = new HashMap<>();

                int iAin = -1;
                for (AbstractInsnNode ain : origInstructs) {
                    iAin++;

                    if (mn.name.equals("<init>")) {
                        if (ain instanceof MethodInsnNode) {
                            if (ain.getOpcode() == INVOKESPECIAL
                                    && ain.getPrevious() instanceof VarInsnNode
                                    && ((VarInsnNode) ain.getPrevious()).var == 0) {
                                calledSuper = true;
                                continue;
                            }
                        }
                    }

                    if (mn.name.equals("<init>") && !calledSuper)
                        continue;

                    /*if(ain instanceof FrameNode) {
                        while (!(ain.getPrevious() instanceof LabelNode))
                            ain = ain.getPrevious();

                        lastFrame = (LabelNode) ain.getPrevious();
                    }*/


                    if (frames[iAin].getStackSize() != 0) {

                        /*mv.visitFieldInsn(GETSTATIC, "sexy/poke/ToFlowObfu", "test", "I");
                        mv.visitInsn(ICONST_2);
                        mv.visitJumpInsn(IF_ICMPLE, l2);*/

                        if (ain instanceof LineNumberNode)
                            continue;

                        InsnList list = new InsnList();

                        LabelNode caseone = new LabelNode();
                        LabelNode casetwo = new LabelNode();
                        list.add(caseone);


                        int index = rnd.nextInt(staticFields.size());
                        sfn = staticFields.get(indextmp);

                        list.add(new FieldInsnNode(GETSTATIC, sfn.getOwner().getClassName(), sfn.getName(), sfn.getDesk()));

                        list.add(casetwo);
                        list.add(getIntPush(getByte(rnd)));
                        list.add(lastFrame = new LabelNode());


                        if (!(ain.getPrevious() instanceof FrameNode) || true) {

                            /*Label l7 = new Label();
                            mv.visitLabel(l7);
                            mv.visitLineNumber(28, l7);
                            mv.visitFieldInsn(GETSTATIC, "sexy/poke/ToFlowObfu", "test", "I");
                            mv.visitInsn(ICONST_2);
                            Label l8 = new Label();
                            mv.visitJumpInsn(IF_ICMPLE, l8);
                            Label l9 = new Label();
                            mv.visitLabel(l9);
                            mv.visitLineNumber(29, l9);
                            mv.visitJumpInsn(GOTO, l5);
                            mv.visitLabel(l8);
                            mv.visitLineNumber(31, l8);*/


                            LabelNode skip = new LabelNode();
                            LabelNode trash = new LabelNode();
                            list.add(new JumpInsnNode(IF_ICMPGE, trash));

                            list.add(new JumpInsnNode(GOTO, skip));

                            list.add(trash);

                            //list.add(new JumpInsnNode(GOTO, trash));


                            /*list.add(new FieldInsnNode(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
                            list.add(new LdcInsnNode(cn.name + " " + mn.name));
                            list.add(new MethodInsnNode(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));*/

                            //list.add(exitList(mn));


                            /*LabelNode nextNode = null;
                            int skips = 0;
                            int skipdex = 0;
                            while (ain.getNext() != null) {
                                ain = ain.getNext();

                                if(ain instanceof LabelNode) {
                                    nextNode = (LabelNode) ain;
                                    if(skips == skipdex++)
                                    break;
                                }
                            }*/


                            List<Integer> stored = new ArrayList<>();
                            int tmp = -1;

                            LabelNode outside = new LabelNode();
                            list.add(outside);


                            if(true) {
                                int s = rnd.nextInt(2);

                                switch (s) {
                                    case 0:
                                        list.add(getTrashCode(stored, mn, nodes, staticFields, rnd.nextInt(4)));
                                        break;
                                    case 1:

                                        InsnList tryCatch = new InsnList();
                                        LabelNode start = new LabelNode();
                                        LabelNode end = new LabelNode();

                                        LabelNode label1 = new LabelNode();
                                        LabelNode label2 = new LabelNode();
                                        LabelNode label3 = new LabelNode();

                                        tryCatch.add(start);
                                        if (rnd.nextBoolean()) {
                                            tryCatch.add(new InsnNode(ICONST_0));
                                            tryCatch.add(new JumpInsnNode(IFNE, end));
                                        } else {
                                            tryCatch.add(new InsnNode(ICONST_1));
                                            tryCatch.add(new JumpInsnNode(IFEQ, end));
                                        }
                                        tryCatch.add(label2);

                                        tryCatch.add(getTrashCode(stored, mn, nodes, staticFields, rnd.nextInt(4)));

                                        if (rnd.nextBoolean()) {
                                            tryCatch.add(new InsnNode(ICONST_0));
                                            tryCatch.add(new JumpInsnNode(IFNE, label1));
                                        } else {
                                            tryCatch.add(new InsnNode(ICONST_1));
                                            tryCatch.add(new JumpInsnNode(IFEQ, label1));
                                        }

                                        tryCatch.add(label3);

                                        tryCatch.add(getTrashCode(stored, mn, nodes, staticFields, rnd.nextInt(4)));


                                        if (rnd.nextBoolean()) {
                                            tryCatch.add(new InsnNode(ICONST_0));
                                            tryCatch.add(new JumpInsnNode(IFNE, label2));
                                        } else {
                                            tryCatch.add(new InsnNode(ICONST_1));
                                            tryCatch.add(new JumpInsnNode(IFEQ, label2));
                                        }

                                        tryCatch.add(getTrashCode(stored, mn, nodes, staticFields, rnd.nextInt(4)));

                                        tryCatch.add(new JumpInsnNode(GOTO, label3));
                                        tryCatch.add(label1);

                                        tryCatch.add(getTrashCode(stored, mn, nodes, staticFields, rnd.nextInt(4)));

                                        tryCatch.add(new JumpInsnNode(GOTO, outside));//label1

                                        tryCatch.add(end);

                                        list.add(tryCatch);
                                        break;
                                }
                            }


                            //mv.visitLdcInsn(Type.getType("Lsexy/poke/ToFlowObfu;"));
                            //mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName", "()Ljava/lang/String;", false);

                            //list.add(new LdcInsnNode(Type.getType("L" + generateString(rnd, 10, false) + ";")));
                            //list.add(new VarInsnNode(ASTORE, rnd.nextInt(50) + mn.maxLocals + 1));

                            //TODO extra aggresiv
                            if (extraAggressive) {


                                //TODO added 4 performace
                                list.add(new JumpInsnNode(GOTO, trash));

                                list.add(getIntPush(getByte(rnd)));
                                list.add(getIntPush(getByte(rnd)));


                                LabelNode loop = new LabelNode();

                                list.add(new JumpInsnNode(IF_ICMPGE, loop));

                                list.add(new JumpInsnNode(GOTO, caseone));

                                list.add(loop);

                                list.add(getTrashCode(stored, mn, nodes, staticFields, rnd.nextInt(2)));

                                list.add(getIntPush(getByte(rnd)));


                                LabelNode gotosyso = new LabelNode();

                                list.add(getIntPush(getByte(rnd)));

                                list.add(new JumpInsnNode(159 + rnd.nextInt(8), gotosyso));

                                list.add(getIntPush(getByte(rnd)));
                                list.add(new JumpInsnNode(GOTO, casetwo));

                                list.add(gotosyso);

                                list.add(getTrashCode(stored, mn, nodes, staticFields, rnd.nextInt(3)));


                            /*list.add(new FieldInsnNode(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
                            list.add(new LdcInsnNode(cn.name + " " + mn.name));
                            list.add(new MethodInsnNode(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));*/
                                list.add(getIntPush(getByte(rnd)));
                                list.add(getIntPush(getByte(rnd)));
                                list.add(new JumpInsnNode(GOTO, lastFrame));


                                //list.add(new JumpInsnNode(GOTO, lastFrame));

                            /*if(nextNode != null) {
                                list.add(new JumpInsnNode(GOTO, l9));
                            } else {
                                list.add(new JumpInsnNode(GOTO, l9));
                            }*/

                            }

                            list.add(skip);


                        } else {
                            list.add(new JumpInsnNode(IF_ICMPLE, lastFrame));
                        }

                        //System.out.println("added jump to " + mn.name + " " + cn.name);

                        //System.out.println(ain + " " + ain.getPrevious());


                        addAfter.put(ain, list);


                        //list.add(new JumpInsnNode(GOTO,gotobehindsyso));
                    }

                    /*if (false) {
                        if (ain instanceof JumpInsnNode) {
                            if (ain.getOpcode() == GOTO) {

                                mn.instructions.insertBefore(ain,
                                        new VarInsnNode(ILOAD, var));
                                mn.instructions.insertBefore(ain,
                                        new InsnNode(ICONST_0));
                                mn.instructions.insert(ain,
                                        new InsnNode(ATHROW));
                                mn.instructions.insert(ain,
                                        new InsnNode(ACONST_NULL));
                                mn.instructions.set(ain,
                                        new JumpInsnNode(IF_ICMPEQ,
                                                ((JumpInsnNode) ain).label));
                            } else if (ain.getOpcode() >= IFEQ
                                    || ain.getOpcode() <= IF_ICMPLE) {

                                mn.instructions.insert(ain, new JumpInsnNode(IFNE, ((JumpInsnNode) ain).label));
                                mn.instructions.insert(ain, new VarInsnNode(ILOAD, var));
                                //counter.incrementAndGet();
                            }
                        }
                    }*/
                }

                addAfter.forEach((abstractInsnNode, insnList) -> mn.instructions.insertBefore(abstractInsnNode, insnList));



                //mn.instructions.insert(mn.instructions.getFirst(),first);




                //System.out.println(initLocalsStack.size());
            });



        });

    }

    @Override
    public String getName() {
        return "Flow Obfuscation";
    }

    /**
     * Inserts an "exit" label into the start of the method node which is used
     * by the transformer to branch the stack into a false jump.
     *
     * @param methodNode current {@link MethodNode} to insert an exit code
     *                   block into.
     * @return a {@link LabelNode} used to branch the stack with a false
     * conditional.
     */
    private LabelNode exitLabel(MethodNode methodNode) {
        LabelNode lb = new LabelNode();
        LabelNode escapeNode = new LabelNode();
        AbstractInsnNode target = methodNode.instructions.getFirst();
        methodNode.instructions.insertBefore(target, new JumpInsnNode(GOTO, escapeNode));
        methodNode.instructions.insertBefore(target, lb);
        Type returnType = Type.getReturnType(methodNode.desc);
        switch (returnType.getSort()) {
            case Type.VOID:
                methodNode.instructions.insertBefore(target, new InsnNode(RETURN));
                break;
            case Type.BOOLEAN:
            case Type.CHAR:
            case Type.BYTE:
            case Type.SHORT:
            case Type.INT:
                methodNode.instructions.insertBefore(target, new InsnNode(ICONST_0));
                methodNode.instructions.insertBefore(target, new InsnNode(IRETURN));
                break;
            case Type.LONG:
                methodNode.instructions.insertBefore(target, new InsnNode(LCONST_0));
                methodNode.instructions.insertBefore(target, new InsnNode(LRETURN));
                break;
            case Type.FLOAT:
                methodNode.instructions.insertBefore(target, new InsnNode(FCONST_0));
                methodNode.instructions.insertBefore(target, new InsnNode(FRETURN));
                break;
            case Type.DOUBLE:
                methodNode.instructions.insertBefore(target, new InsnNode(DCONST_0));
                methodNode.instructions.insertBefore(target, new InsnNode(DRETURN));
                break;
            default:
                methodNode.instructions.insertBefore(target, new InsnNode(ACONST_NULL));
                methodNode.instructions.insertBefore(target, new InsnNode(ARETURN));
                break;
        }
        methodNode.instructions.insertBefore(target, escapeNode);

        return lb;
    }

    public InsnList getTrashCode(List<Integer> stored, MethodNode mn, List<ClassNode> nodes, List<SmallFieldNode> staticFields,int amount) {
        InsnList list = new InsnList();

        LabelNode outside = new LabelNode();
        list.add(outside);

        int tmp, indextmp = -1;
        SmallFieldNode sfn;

        for (int i = 0; i < amount; i++) {

            int s = rnd.nextInt(6);

            switch (s) {
                case 0:
                    list.add(getIntPush(rnd.nextInt(1000)));
                    tmp = rnd.nextInt(5) + mn.maxLocals;
                    list.add(new VarInsnNode(ISTORE, tmp));
                    stored.add(tmp);
                    break;
                case 1:
                    list.add(new LdcInsnNode(Type.getType("L" + generateString(rnd, 5, false) + ";")));
                    tmp = rnd.nextInt(5) + mn.maxLocals;
                    list.add(new VarInsnNode(ASTORE, tmp));
                    stored.add(tmp);
                    break;
                case 2:
                    list.add(new LdcInsnNode(Type.getType("L" + nodes.get(rnd.nextInt(nodes.size())).name + ";")));
                    tmp = rnd.nextInt(5) + mn.maxLocals;
                    list.add(new VarInsnNode(ASTORE, tmp));
                    stored.add(tmp);
                    break;
                case 3:
                    list.add(new LdcInsnNode(generateString(rnd, 10, false)));
                    tmp = rnd.nextInt(5) + mn.maxLocals;
                    list.add(new VarInsnNode(ASTORE, tmp));
                    stored.add(tmp);
                    break;
                case 4:

                    indextmp = rnd.nextInt(staticFields.size());
                    sfn = staticFields.get(indextmp);

                    list.add(new FieldInsnNode(GETSTATIC, sfn.getOwner().getClassName(),sfn.getName(),sfn.getDesk()));
                    list.add(new VarInsnNode(ISTORE, ++mn.maxStack));
                    list.add(new IincInsnNode(mn.maxStack, getByte(rnd) * 2 - 127));
                    list.add(new VarInsnNode(ILOAD, mn.maxStack));
                    list.add(new FieldInsnNode(PUTSTATIC, sfn.getOwner().getClassName(),sfn.getName(),sfn.getDesk()));
                    //list.add(new InsnNode(ACONST_NULL));
                    //list.add(new InsnNode(ATHROW));
                    break ;
                case 5:
                    indextmp = rnd.nextInt(staticFields.size());
                    sfn = staticFields.get(indextmp);
                    list.add(getIntPush(rnd.nextInt(1000)));
                    list.add(new FieldInsnNode(PUTSTATIC, sfn.getOwner().getClassName(),sfn.getName(),sfn.getDesk()));
                    break;
            }
        }

        return list;
    }
}
