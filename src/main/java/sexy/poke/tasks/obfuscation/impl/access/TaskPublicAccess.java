package sexy.poke.tasks.obfuscation.impl.access;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 07.08.2018
 */
public class TaskPublicAccess extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        Platform.runLater(() -> body.setText(""));

        if (Main.settings.publicClasses) {
            nodes.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_PRIVATE) != 0 ? node.access & ~Opcodes.ACC_PRIVATE : node.access);
            nodes.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_PUBLIC) != 0 ? node.access : node.access | Opcodes.ACC_PUBLIC);
        }
        if (Main.settings.publicMethods) {
            nodes.parallelStream().forEach(n -> n.methods.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_PRIVATE) != 0 ? node.access & ~Opcodes.ACC_PRIVATE : node.access));
            nodes.parallelStream().forEach(n -> n.methods.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_PROTECTED) != 0 ? node.access & ~Opcodes.ACC_PROTECTED : node.access));
            nodes.parallelStream().forEach(n -> n.methods.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_PUBLIC) != 0 ? node.access : node.access | Opcodes.ACC_PUBLIC));
        }
        if (Main.settings.publicFields) {
            nodes.parallelStream().forEach(n -> n.fields.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_PRIVATE) != 0 ? node.access & ~Opcodes.ACC_PRIVATE : node.access));
            nodes.parallelStream().forEach(n -> n.fields.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_PROTECTED) != 0 ? node.access & ~Opcodes.ACC_PROTECTED : node.access));

            nodes.parallelStream().forEach(n -> n.fields.parallelStream().forEach(node -> node.access = (node.access & Opcodes.ACC_PUBLIC) != 0 ? node.access : node.access | Opcodes.ACC_PUBLIC));
        }
    }

    @Override
    public String getName() {
        return "Public Access";
    }
}
