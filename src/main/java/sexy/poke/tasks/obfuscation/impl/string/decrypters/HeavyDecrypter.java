package sexy.poke.tasks.obfuscation.impl.string.decrypters;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import sexy.poke.Main;
import sexy.poke.tasks.obfuscation.impl.string.AbstractDecrypter;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by Flo on 07.08.2018
 */
public class HeavyDecrypter extends AbstractDecrypter {
    public HeavyDecrypter(ClassNode node) {
        super(node);

        key = new byte[50 + rnd.nextInt(200)];

        rnd.nextBytes(key);

        //methodName = "decrypt";
        methodDesk = "(Ljava/lang/" + (rnd.nextBoolean() ? "String" : "Object") + ";" + "I" + ")Ljava/lang/" + (rnd.nextBoolean() ? "String" : "Object") + ";";

        methodName = generateString(rnd, 10, true, Main.settings.dictionaryMethods);

        String fieldName = generateString(rnd, 10, true, Main.settings.dictionaryFields);
        addByteArray(fieldName);
        addDecrypter(fieldName);

        System.out.println("Added decrypter to class " + node.name);
    }

    @Override
    protected void process(ClassNode node, MethodNode methodNode, LabelNode prevLabel, LdcInsnNode ldc) {
        int offset = rnd.nextInt(1000);
        int linenumber = rnd.nextInt(1000);

        if (node.sourceFile.isEmpty())
            node.sourceFile = generateString(rnd, pool_abcABC, 20, false);

        try {

            String str = getString(ldc.cst.toString(), offset, methodNode.name, node.sourceFile, linenumber);

            if (str.getBytes().length >= 65535)
                return;

            ldc.cst = str;
            LabelNode nlabel = new LabelNode();
            methodNode.instructions.insertBefore(ldc, nlabel);
            methodNode.instructions.insertBefore(ldc, new LineNumberNode(linenumber, nlabel));
            AbstractInsnNode prev;
            methodNode.instructions.insert(ldc, prev = new IntInsnNode(offset <= 127 ? BIPUSH : SIPUSH, offset));
            methodNode.instructions.insert(prev, prev = new MethodInsnNode(INVOKESTATIC, this.node.name, methodName, methodDesk, false));

            if (methodDesk.endsWith("Ljava/lang/Object;"))
                methodNode.instructions.insert(prev, new MethodInsnNode(INVOKEVIRTUAL, "java/lang/Object", "toString", "()Ljava/lang/String;", false));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    byte[] key = new byte[200];

    public String getString(String in, int offset, String methodname, String filename, int index) throws UnsupportedEncodingException {
        boolean obfu = true;
        //StackTraceElement element = new Exception().getStackTrace()[1];
        StackTraceElement element = new Exception().getStackTrace()[1];
        //int index = 20;
        //String methodname = "main";
        //String filename = "Main.java";
        byte[] a = in.getBytes();
        byte[] fbytes = filename.getBytes("ISO_8859_1");
        byte[] out = new byte[a.length + (obfu ? offset / 8 : -offset / 8)];

        if (!obfu) {
            a = new byte[a.length - offset / 1];
            System.arraycopy(in, 0, a, 0, a.length);
        }

        for (int i = 0; i < a.length; i++) {

            byte b = (byte) (a[i] ^ ((key[(i + offset) % key.length] + index) ^ index / 2));

            for (int i2 = 0; i2 < methodname.length(); i2++) {
                b = (byte) (b ^ methodname.getBytes()[i2] ^ fbytes[(i + (offset / (i + 1))) % fbytes.length]);
            }

            out[i] = b;
        }

        if (obfu) {
            byte[] b = new byte[offset / 8];
            new Random().nextBytes(b);
            System.arraycopy(b, 0, out, out.length - offset / 8, offset / 8);
        }


        return new String(out, "ISO-8859-1");
    }

    private void addByteArray(String fieldname) {

        node.fields.add(new FieldNode(Opcodes.ACC_STATIC | Opcodes.ACC_PRIVATE, fieldname, "[B", null, null));


        final boolean[] found = new boolean[1];

        node.methods.forEach(m -> {
            if (m.name.equals("<clinit>")) {
                found[0] = true;
                injectArray(m, fieldname);
            }
        });

        if (!found[0]) {
            MethodNode mn;
            node.methods.add(mn = new MethodNode(ACC_STATIC, "<clinit>", "()V", null, null));
            mn.instructions.add(new InsnNode(RETURN));
            injectArray(mn, fieldname);
        }
    }

    private void injectArray(MethodNode m, String fieldname) {
        LabelNode l = new LabelNode();
        InsnList newList = new InsnList();
        newList.add(l);
        newList.add(new IntInsnNode(Opcodes.SIPUSH, key.length));
        newList.add(new IntInsnNode(Opcodes.NEWARRAY, Opcodes.T_BYTE));

        int i = 0;

        List<AbstractMap.SimpleEntry<Integer, Byte>> pairs = new ArrayList<>();

        for (byte b : key) {
            pairs.add(new AbstractMap.SimpleEntry<>(i++, b));
        }

        Collections.shuffle(pairs);

        for (AbstractMap.SimpleEntry<Integer, Byte> pair : pairs) {

            int key = pair.getKey();
            byte value = pair.getValue();

            newList.add(new InsnNode(Opcodes.DUP));

            if (key <= 5) {
                newList.add(new InsnNode(Opcodes.ICONST_0 + key));
            } else {
                newList.add(new IntInsnNode(key <= 127 ? Opcodes.BIPUSH : Opcodes.SIPUSH, key));
            }

            newList.add(new IntInsnNode(Opcodes.BIPUSH, value));
            newList.add(new InsnNode(Opcodes.BASTORE));
        }
/*
            for (byte b : key) {


                newList.add(new InsnNode(Opcodes.DUP));

                if(i <= 5) {
                    newList.add(new InsnNode(Opcodes.ICONST_0 + i));
                } else {
                    newList.add(new IntInsnNode(i <= 127 ? Opcodes.BIPUSH : Opcodes.SIPUSH, i));
                }

                i++;

                newList.add(new IntInsnNode(Opcodes.BIPUSH, b));
                newList.add(new InsnNode(Opcodes.BASTORE));
            }*/

        newList.add(new FieldInsnNode(Opcodes.PUTSTATIC, node.name, fieldname, "[B"));

        m.instructions.insert(newList);
    }

    private void addDecrypter(String fieldname) {


        String fieldOne = "0";
        String fieldTwo = "1";

        fieldOne = generateString(rnd, 10, true);
        fieldTwo = generateString(rnd, 10, true);

        node.fields.add(new FieldNode(Opcodes.ACC_STATIC | Opcodes.ACC_PRIVATE, fieldOne, "I", null, null));
        node.fields.add(new FieldNode(Opcodes.ACC_STATIC | Opcodes.ACC_PRIVATE, fieldTwo, "I", null, null));


        Label tmpLabel1 = new Label();
        Label tmpLabel2 = new Label();
        Label tmpLabel3 = new Label();
        Label tmpLabel4 = new Label();

        MethodVisitor methodVisitor = node.visitMethod(ACC_PUBLIC | ACC_STATIC, methodName, methodDesk, null, null);
        methodVisitor.visitCode();

        /*methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        methodVisitor.visitLdcInsn("Called decrypt");
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);*/

        methodVisitor.visitTypeInsn(NEW, "java/lang/Exception");
        methodVisitor.visitInsn(DUP);
        methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Exception", "<init>", "()V", false);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Exception", "getStackTrace", "()[Ljava/lang/StackTraceElement;", false);
        methodVisitor.visitInsn(ICONST_1);
        methodVisitor.visitInsn(AALOAD);
        methodVisitor.visitVarInsn(ASTORE, 4);

        //methodVisitor.visitLabel(tmpLabel4);

        methodVisitor.visitFieldInsn(GETSTATIC, node.name, fieldTwo, "I");
        methodVisitor.visitVarInsn(ALOAD, 4);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StackTraceElement", "getFileName", "()Ljava/lang/String;", false);
        methodVisitor.visitVarInsn(ASTORE, 5);

        /*methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        methodVisitor.visitVarInsn(ALOAD, 5);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);*/

        methodVisitor.visitVarInsn(ALOAD, 0);
        methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/String");
        methodVisitor.visitLdcInsn("ISO-8859-1");
        methodVisitor.visitMethodInsn(INVOKESTATIC, "java/nio/charset/Charset", "forName", "(Ljava/lang/String;)Ljava/nio/charset/Charset;", false);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "(Ljava/nio/charset/Charset;)[B", false);
        methodVisitor.visitVarInsn(ASTORE, 6);
        methodVisitor.visitVarInsn(ISTORE, 2);


/*
        methodVisitor.visitLabel(tmpLabel1);
        methodVisitor.visitVarInsn(ALOAD, 4);
        methodVisitor.visitIntInsn(BIPUSH, 8);
        methodVisitor.visitJumpInsn(IF_ICMPGE, tmpLabel2);
        methodVisitor.visitLabel(tmpLabel3);

        methodVisitor.visitJumpInsn(GOTO,tmpLabel4);



        methodVisitor.visitLabel(tmpLabel2);*/




        methodVisitor.visitVarInsn(ALOAD, 5);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "()[B", false);
        methodVisitor.visitVarInsn(ASTORE, 7);
        methodVisitor.visitVarInsn(ALOAD, 6);
        methodVisitor.visitInsn(ARRAYLENGTH);
        methodVisitor.visitVarInsn(ILOAD, 1);
        methodVisitor.visitInsn(INEG);
        methodVisitor.visitIntInsn(BIPUSH, 8);
        methodVisitor.visitInsn(IDIV);
        methodVisitor.visitInsn(IADD);
        methodVisitor.visitIntInsn(NEWARRAY, T_BYTE);
        methodVisitor.visitVarInsn(ASTORE, 8);
        methodVisitor.visitVarInsn(ALOAD, 6);
        methodVisitor.visitVarInsn(ASTORE, 9);
        methodVisitor.visitVarInsn(ALOAD, 4);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StackTraceElement", "getLineNumber", "()I", false);
        methodVisitor.visitVarInsn(ISTORE, 10);
/*
        methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        methodVisitor.visitVarInsn(ILOAD, 10);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(I)V", false);*/

        methodVisitor.visitVarInsn(ALOAD, 6);
        methodVisitor.visitInsn(ARRAYLENGTH);
        methodVisitor.visitVarInsn(ILOAD, 1);
        methodVisitor.visitIntInsn(BIPUSH, 8);
        methodVisitor.visitInsn(IDIV);
        methodVisitor.visitInsn(ISUB);
        methodVisitor.visitIntInsn(NEWARRAY, T_BYTE);
        methodVisitor.visitVarInsn(ASTORE, 6);
        methodVisitor.visitVarInsn(ALOAD, 9);
        methodVisitor.visitInsn(ICONST_0);
        methodVisitor.visitVarInsn(ALOAD, 6);
        methodVisitor.visitInsn(ICONST_0);
        methodVisitor.visitVarInsn(ALOAD, 6);
        methodVisitor.visitInsn(ARRAYLENGTH);
        methodVisitor.visitMethodInsn(INVOKESTATIC, "java/lang/System", "arraycopy", "(Ljava/lang/Object;ILjava/lang/Object;II)V", false);
        methodVisitor.visitVarInsn(ALOAD, 4);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StackTraceElement", "getMethodName", "()Ljava/lang/String;", false);
        methodVisitor.visitVarInsn(ASTORE, 11);

        /*methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        methodVisitor.visitVarInsn(ALOAD, 11);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);*/

        methodVisitor.visitInsn(ICONST_0);
        methodVisitor.visitVarInsn(ISTORE, 12);

        //methodVisitor.visitLabel(tmpLabel4);

        Label label0 = new Label();
        methodVisitor.visitLabel(label0);
        methodVisitor.visitFrame(Opcodes.F_NEW, 13, new Object[] {"java/lang/Object", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.TOP, "java/lang/StackTraceElement", "java/lang/String", "[B", "[B", "[B", "[B", Opcodes.INTEGER, "java/lang/String", Opcodes.INTEGER}, 0, new Object[] {});
        methodVisitor.visitVarInsn(ILOAD, 12);
        methodVisitor.visitVarInsn(ALOAD, 6);
        methodVisitor.visitInsn(ARRAYLENGTH);
        Label label1 = new Label();
        methodVisitor.visitJumpInsn(IF_ICMPGE, label1);
        methodVisitor.visitVarInsn(ALOAD, 6);
        methodVisitor.visitVarInsn(ILOAD, 12);
        methodVisitor.visitInsn(BALOAD);
        methodVisitor.visitFieldInsn(GETSTATIC, node.name, fieldname, "[B");
        methodVisitor.visitVarInsn(ILOAD, 12);
        methodVisitor.visitVarInsn(ILOAD, 1);
        methodVisitor.visitInsn(IADD);
        methodVisitor.visitFieldInsn(GETSTATIC, node.name, fieldname, "[B");
        methodVisitor.visitInsn(ARRAYLENGTH);
        methodVisitor.visitInsn(IREM);
        methodVisitor.visitInsn(BALOAD);
        methodVisitor.visitVarInsn(ILOAD, 10);
        methodVisitor.visitInsn(IADD);
        methodVisitor.visitVarInsn(ILOAD, 10);
        methodVisitor.visitInsn(ICONST_2);
        methodVisitor.visitInsn(IDIV);
        methodVisitor.visitInsn(IXOR);
        methodVisitor.visitInsn(IXOR);
        methodVisitor.visitInsn(I2B);
        methodVisitor.visitVarInsn(ISTORE, 13);
        methodVisitor.visitInsn(ICONST_0);
        methodVisitor.visitVarInsn(ISTORE, 14);
        Label label2 = new Label();
        methodVisitor.visitLabel(label2);
        methodVisitor.visitFrame(Opcodes.F_NEW, 15, new Object[] {"java/lang/Object", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.TOP, "java/lang/StackTraceElement", "java/lang/String", "[B", "[B", "[B", "[B", Opcodes.INTEGER, "java/lang/String", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
        methodVisitor.visitVarInsn(ILOAD, 14);
        methodVisitor.visitVarInsn(ALOAD, 11);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "length", "()I", false);
        Label label3 = new Label();
        methodVisitor.visitJumpInsn(IF_ICMPGE, label3);
        methodVisitor.visitVarInsn(ILOAD, 13);
        methodVisitor.visitVarInsn(ALOAD, 11);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "()[B", false);
        methodVisitor.visitVarInsn(ILOAD, 14);
        methodVisitor.visitInsn(BALOAD);
        methodVisitor.visitInsn(IXOR);
        methodVisitor.visitVarInsn(ALOAD, 7);
        methodVisitor.visitVarInsn(ILOAD, 12);
        methodVisitor.visitVarInsn(ILOAD, 1);
        methodVisitor.visitVarInsn(ILOAD, 12);
        methodVisitor.visitInsn(ICONST_1);
        methodVisitor.visitInsn(IADD);
        methodVisitor.visitInsn(IDIV);
        methodVisitor.visitInsn(IADD);
        methodVisitor.visitVarInsn(ALOAD, 7);
        methodVisitor.visitInsn(ARRAYLENGTH);
        methodVisitor.visitInsn(IREM);
        methodVisitor.visitInsn(BALOAD);
        methodVisitor.visitInsn(IXOR);
        methodVisitor.visitInsn(I2B);
        methodVisitor.visitVarInsn(ISTORE, 13);
        methodVisitor.visitIincInsn(14, 1);
        methodVisitor.visitVarInsn(ILOAD, 2);
        Label label4 = new Label();
        methodVisitor.visitJumpInsn(IFNE, label4);
        methodVisitor.visitVarInsn(ILOAD, 2);
        methodVisitor.visitJumpInsn(IFEQ, label2);
        methodVisitor.visitFieldInsn(GETSTATIC, node.name, fieldOne, "I");
        methodVisitor.visitVarInsn(ISTORE, 3);
        methodVisitor.visitIincInsn(3, 1);
        methodVisitor.visitVarInsn(ILOAD, 3);
        methodVisitor.visitFieldInsn(PUTSTATIC, node.name, fieldOne, "I");
        methodVisitor.visitLabel(label3);
        methodVisitor.visitFrame(Opcodes.F_NEW, 15, new Object[] {"java/lang/Object", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.TOP, "java/lang/StackTraceElement", "java/lang/String", "[B", "[B", "[B", "[B", Opcodes.INTEGER, "java/lang/String", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
        methodVisitor.visitVarInsn(ALOAD, 8);
        methodVisitor.visitVarInsn(ILOAD, 12);
        methodVisitor.visitVarInsn(ILOAD, 13);
        methodVisitor.visitInsn(BASTORE);
        methodVisitor.visitIincInsn(12, 1);
        methodVisitor.visitLabel(label4);
        methodVisitor.visitFrame(Opcodes.F_NEW, 15, new Object[] {"java/lang/Object", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.TOP, "java/lang/StackTraceElement", "java/lang/String", "[B", "[B", "[B", "[B", Opcodes.INTEGER, "java/lang/String", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.INTEGER}, 0, new Object[] {});
        methodVisitor.visitVarInsn(ILOAD, 2);
        methodVisitor.visitJumpInsn(IFEQ, label0);
        methodVisitor.visitLabel(label1);
        methodVisitor.visitFrame(Opcodes.F_NEW, 13, new Object[] {"java/lang/Object", Opcodes.INTEGER, Opcodes.INTEGER, Opcodes.TOP, "java/lang/StackTraceElement", "java/lang/String", "[B", "[B", "[B", "[B", Opcodes.INTEGER, "java/lang/String", Opcodes.INTEGER}, 0, new Object[] {});

        /*methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        methodVisitor.visitLdcInsn("finished decrypt");
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);*/

        methodVisitor.visitTypeInsn(NEW, "java/lang/String");
        methodVisitor.visitInsn(DUP);
        methodVisitor.visitVarInsn(ALOAD, 8);
        methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/String", "<init>", "([B)V", false);
        methodVisitor.visitInsn(ARETURN);
        methodVisitor.visitMaxs(6, 15);
        methodVisitor.visitEnd();

            /*MethodVisitor mv = node.visitMethod(ACC_PUBLIC + ACC_STATIC, name, "(Ljava/lang/Object;I)Ljava/lang/Object;", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(18, l0);
            mv.visitTypeInsn(NEW, "java/lang/Exception");
            mv.visitInsn(DUP);
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Exception", "<init>", "()V", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Exception", "getStackTrace", "()[Ljava/lang/StackTraceElement;", false);
            mv.visitInsn(ICONST_1);
            mv.visitInsn(AALOAD);
            mv.visitVarInsn(ASTORE, 2);
            Label l1 = new Label();
            mv.visitLabel(l1);
            mv.visitLineNumber(20, l1);
            mv.visitVarInsn(ALOAD, 2);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StackTraceElement", "getFileName", "()Ljava/lang/String;", false);
            mv.visitVarInsn(ASTORE, 3);
            Label l2 = new Label();
            mv.visitLabel(l2);
            mv.visitLineNumber(21, l2);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitTypeInsn(CHECKCAST, "java/lang/String");
            mv.visitLdcInsn("ISO-8859-1");
            mv.visitMethodInsn(INVOKESTATIC, "java/nio/charset/Charset", "forName", "(Ljava/lang/String;)Ljava/nio/charset/Charset;", false);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "(Ljava/nio/charset/Charset;)[B", false);
            mv.visitVarInsn(ASTORE, 4);
            Label l3 = new Label();
            mv.visitLabel(l3);
            mv.visitLineNumber(23, l3);
            mv.visitVarInsn(ALOAD, 3);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "()[B", false);
            mv.visitVarInsn(ASTORE, 5);
            Label l4 = new Label();
            mv.visitLabel(l4);
            mv.visitLineNumber(24, l4);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ARRAYLENGTH);
            mv.visitVarInsn(ILOAD, 1);
            mv.visitInsn(INEG);
            mv.visitIntInsn(BIPUSH, 8);
            mv.visitInsn(IDIV);
            mv.visitInsn(IADD);
            mv.visitIntInsn(NEWARRAY, T_BYTE);
            mv.visitVarInsn(ASTORE, 6);
            Label l5 = new Label();
            mv.visitLabel(l5);
            mv.visitLineNumber(26, l5);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitVarInsn(ASTORE, 7);
            Label l6 = new Label();
            mv.visitLabel(l6);
            mv.visitLineNumber(28, l6);
            mv.visitVarInsn(ALOAD, 2);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StackTraceElement", "getLineNumber", "()I", false);
            mv.visitVarInsn(ISTORE, 8);
            Label l7 = new Label();
            mv.visitLabel(l7);
            mv.visitLineNumber(30, l7);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ARRAYLENGTH);
            mv.visitVarInsn(ILOAD, 1);
            mv.visitIntInsn(BIPUSH, 8);
            mv.visitInsn(IDIV);
            mv.visitInsn(ISUB);
            mv.visitIntInsn(NEWARRAY, T_BYTE);
            mv.visitVarInsn(ASTORE, 4);
            Label l8 = new Label();
            mv.visitLabel(l8);
            mv.visitLineNumber(31, l8);
            mv.visitVarInsn(ALOAD, 7);
            mv.visitInsn(ICONST_0);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ICONST_0);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ARRAYLENGTH);
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "arraycopy", "(Ljava/lang/Object;ILjava/lang/Object;II)V", false);
            Label l9 = new Label();
            mv.visitLabel(l9);
            mv.visitLineNumber(33, l9);
            mv.visitVarInsn(ALOAD, 2);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StackTraceElement", "getMethodName", "()Ljava/lang/String;", false);
            mv.visitVarInsn(ASTORE, 9);
            Label l10 = new Label();
            mv.visitLabel(l10);
            mv.visitLineNumber(34, l10);
            mv.visitInsn(ICONST_0);
            mv.visitVarInsn(ISTORE, 10);
            Label l11 = new Label();
            mv.visitLabel(l11);
            mv.visitFrame(Opcodes.F_FULL, 11, new Object[]{"java/lang/Object", Opcodes.INTEGER, "java/lang/StackTraceElement", "java/lang/String", "[B", "[B", "[B", "[B", Opcodes.INTEGER, "java/lang/String", Opcodes.INTEGER}, 0, new Object[]{});
            mv.visitVarInsn(ILOAD, 10);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitInsn(ARRAYLENGTH);
            Label l12 = new Label();
            mv.visitJumpInsn(IF_ICMPGE, l12);
            Label l13 = new Label();
            mv.visitLabel(l13);
            mv.visitLineNumber(36, l13);
            mv.visitVarInsn(ALOAD, 4);
            mv.visitVarInsn(ILOAD, 10);
            mv.visitInsn(BALOAD);
            mv.visitFieldInsn(GETSTATIC, node.name, fieldname, "[B");
            mv.visitVarInsn(ILOAD, 10);
            mv.visitVarInsn(ILOAD, 1);
            mv.visitInsn(IADD);
            mv.visitFieldInsn(GETSTATIC, node.name, fieldname, "[B");
            mv.visitInsn(ARRAYLENGTH);
            mv.visitInsn(IREM);
            mv.visitInsn(BALOAD);
            mv.visitVarInsn(ILOAD, 8);
            mv.visitInsn(IADD);
            mv.visitVarInsn(ILOAD, 8);
            mv.visitInsn(ICONST_2);
            mv.visitInsn(IDIV);
            mv.visitInsn(IXOR);
            mv.visitInsn(IXOR);
            mv.visitInsn(I2B);
            mv.visitVarInsn(ISTORE, 11);
            Label l14 = new Label();
            mv.visitLabel(l14);
            mv.visitLineNumber(38, l14);
            mv.visitInsn(ICONST_0);
            mv.visitVarInsn(ISTORE, 12);
            Label l15 = new Label();
            mv.visitLabel(l15);
            mv.visitFrame(Opcodes.F_APPEND, 2, new Object[]{Opcodes.INTEGER, Opcodes.INTEGER}, 0, null);
            mv.visitVarInsn(ILOAD, 12);
            mv.visitVarInsn(ALOAD, 9);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "length", "()I", false);
            Label l16 = new Label();
            mv.visitJumpInsn(IF_ICMPGE, l16);
            Label l17 = new Label();
            mv.visitLabel(l17);
            mv.visitLineNumber(39, l17);
            mv.visitVarInsn(ILOAD, 11);
            mv.visitVarInsn(ALOAD, 9);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/String", "getBytes", "()[B", false);
            mv.visitVarInsn(ILOAD, 12);
            mv.visitInsn(BALOAD);
            mv.visitInsn(IXOR);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitVarInsn(ILOAD, 10);
            mv.visitVarInsn(ILOAD, 1);
            mv.visitVarInsn(ILOAD, 10);
            mv.visitInsn(ICONST_1);
            mv.visitInsn(IADD);
            mv.visitInsn(IDIV);
            mv.visitInsn(IADD);
            mv.visitVarInsn(ALOAD, 5);
            mv.visitInsn(ARRAYLENGTH);
            mv.visitInsn(IREM);
            mv.visitInsn(BALOAD);
            mv.visitInsn(IXOR);
            mv.visitInsn(I2B);
            mv.visitVarInsn(ISTORE, 11);
            Label l18 = new Label();
            mv.visitLabel(l18);
            mv.visitLineNumber(38, l18);
            mv.visitIincInsn(12, 1);
            mv.visitJumpInsn(GOTO, l15);
            mv.visitLabel(l16);
            mv.visitLineNumber(42, l16);
            mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitVarInsn(ILOAD, 10);
            mv.visitVarInsn(ILOAD, 11);
            mv.visitInsn(BASTORE);
            Label l19 = new Label();
            mv.visitLabel(l19);
            mv.visitLineNumber(34, l19);
            mv.visitIincInsn(10, 1);
            mv.visitJumpInsn(GOTO, l11);
            mv.visitLabel(l12);
            mv.visitLineNumber(45, l12);
            mv.visitFrame(Opcodes.F_CHOP, 2, null, 0, null);
            mv.visitTypeInsn(NEW, "java/lang/String");
            mv.visitInsn(DUP);
            mv.visitVarInsn(ALOAD, 6);
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/String", "<init>", "([B)V", false);
            mv.visitInsn(ARETURN);
            Label l20 = new Label();
            mv.visitLabel(l20);
            mv.visitMaxs(6, 13);
            mv.visitEnd();*/
    }
}