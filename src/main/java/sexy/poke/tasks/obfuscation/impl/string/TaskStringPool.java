package sexy.poke.tasks.obfuscation.impl.string;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.*;
import java.util.stream.Stream;

/**
 * Created by Flo on 09.08.2018
 */
public class TaskStringPool extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        for (ClassNode cn : nodes) {

            if((cn.access & ACC_INTERFACE) != 0)
                continue;

            Platform.runLater(() -> body.setText("Merging strings in" + cn.name));


            String fieldName = generateString(rnd, 10, true);

            cn.fields.add(new FieldNode((rnd.nextBoolean() ? ACC_PUBLIC : ACC_PRIVATE) | ACC_STATIC, fieldName, "[Ljava/lang/String;", null, null));

            List<Integer> intlist = new ArrayList<>();
            int[] i = new int[1];

            cn.methods.forEach(mn -> Stream.of(mn.instructions.toArray()).filter(ain -> ain instanceof LdcInsnNode).map(ain -> (LdcInsnNode) ain).filter(ldc -> ldc.cst instanceof String).forEach(ldc -> {
                intlist.add(i[0]++);
            }));

            int ldcsize = i[0];

            Collections.shuffle(intlist);
            i[0] = 0;
            HashMap<Integer, String> map = new HashMap<>();

            for (MethodNode mn : cn.methods) {
                Stream.of(mn.instructions.toArray()).filter(ain -> ain instanceof LdcInsnNode).map(ain -> (LdcInsnNode) ain).filter(ldc -> ldc.cst instanceof String).forEach(ldc -> {
                    map.put(intlist.get(i[0]), ldc.cst.toString());


                    //TODO add method which returns the string, array in locale vars aúslagern

                    InsnList list = new InsnList();
                    list.add(new FieldInsnNode(GETSTATIC, cn.name, fieldName, "[Ljava/lang/String;"));
                    list.add(getIntPush(intlist.get(i[0]++)));
                    list.add(new InsnNode(AALOAD));

                    mn.instructions.insertBefore(ldc, list);
                    mn.instructions.remove(ldc);
                });
            }

            MethodNode clinit = cn.methods.stream().filter(mn -> mn.name.equals("<clinit>")).findFirst().orElseGet(() -> {
                MethodNode mn = new MethodNode(ACC_STATIC, "<clinit>", "()V", null, null);
                mn.instructions.add(new InsnNode(RETURN));
                cn.methods.add(mn);
                return mn;
            });

            injectArray(cn, clinit, fieldName, ldcsize, map);
        }
    }

    @Override
    public String getName() {
        return "String Pool";
    }

    private void injectArray(ClassNode node, MethodNode m, String fieldname, int ldcs, HashMap<Integer, String> strings) {
        LabelNode l = new LabelNode();
        InsnList newList = new InsnList();
        newList.add(l);
        newList.add(getIntPush(ldcs));
        newList.add(new TypeInsnNode(ANEWARRAY, "java/lang/String"));

        for(int i = 0; i < ldcs; i++) {
            newList.add(new InsnNode(DUP));
            newList.add(getIntPush(i));
            newList.add(new LdcInsnNode(strings.get(i)));
            newList.add(new InsnNode(AASTORE));
        }


        newList.add(new FieldInsnNode(Opcodes.PUTSTATIC, node.name, fieldname, "[Ljava/lang/String;"));

        m.instructions.insert(newList);
    }
}
