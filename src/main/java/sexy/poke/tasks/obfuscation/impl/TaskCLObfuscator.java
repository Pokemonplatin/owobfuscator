package sexy.poke.tasks.obfuscation.impl;

import javafx.scene.text.Text;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TaskCLObfuscator extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        for (ClassNode cn : nodes) {
            if ((cn.access & ACC_INTERFACE) != 0)
                continue;

            if (Main.settings.clIdsRemoval == Settings.RemoveState.SHUFFLE) {
                boolean hasObfid = false;

                for (FieldNode fn : cn.fields) {
                    if (fn.value != null && fn.value.toString().startsWith("CL_")) {
                        fn.value = "CL_" + getRandomID();
                        hasObfid = true;
                    }
                }
                if (!hasObfid) {
                    FieldNode fn = new FieldNode(ACC_PRIVATE | ACC_STATIC | ACC_FINAL, "__OBFID", "Ljava/lang/String;", null, "CL_" + getRandomID());
                    cn.fields.add(fn);
                }
            } else {
                cn.fields.removeIf(fn -> {
                    if (fn.value instanceof String) {
                        return ((String) fn.value).startsWith("CL_");
                    }
                    return false;
                });
            }
        }
    }

    private String getRandomID() {
        return String.format("%08d", ThreadLocalRandom.current().nextInt(9999));
    }

    @Override
    public String getName() {
        return "TaskCLObfuscator";
    }
}
