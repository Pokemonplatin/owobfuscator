package sexy.poke.tasks.obfuscation.impl.access;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 07.08.2018
 */
public class TaskSyntheticAccess extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {

        Platform.runLater(() -> body.setText(""));

        if (Main.settings.syntheticMethods)
            nodes.parallelStream().forEach(n -> n.methods.parallelStream().filter(methodNode -> !methodNode.name.contains("<")).forEach(mn -> mn.access = getAccess(mn.access, true)));
        if (Main.settings.syntheticFields)
            nodes.parallelStream().forEach(n -> n.fields.parallelStream().forEach(fn -> fn.access = getAccess(fn.access, false)));
        if (Main.settings.syntheticClasses)
            nodes.parallelStream().forEach(n -> n.access = getAccess(n.access, false));
    }

    @Override
    public String getName() {
        return "Synthetic Access";
    }

    public int getAccess(int access, boolean bridge) {
        if (bridge)
            access = (access & Opcodes.ACC_BRIDGE) != 0 ? access : access | Opcodes.ACC_BRIDGE;
        access = (access & Opcodes.ACC_SYNTHETIC) != 0 ? access : access | Opcodes.ACC_SYNTHETIC;
        return access;
    }
}
