package sexy.poke.tasks.obfuscation.impl.removeal;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 11.10.2018
 */
public class TaskSignatureRemover extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        nodes.parallelStream().forEach(cn -> {

            Platform.runLater(() -> body.setText("Removing signature in class " + cn.name));

            cn.signature = null;

            cn.methods.parallelStream().forEach(mn -> {
                Platform.runLater(() -> body.setText("Removing method signature in class " + cn.name));

                mn.signature = null;
            });

            cn.fields.parallelStream().forEach(fn -> {
                Platform.runLater(() -> body.setText("Removing field signature in class " + cn.name));

                fn.signature = null;
            });
        });
    }

    @Override
    public String getName() {
        return "Signature Remover";
    }
}
