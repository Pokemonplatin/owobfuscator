package sexy.poke.tasks.obfuscation.impl.string;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;
import sexy.poke.tasks.obfuscation.impl.string.decrypters.HeavyDecrypter;
import sexy.poke.tasks.obfuscation.impl.string.decrypters.VeryLightDecrypter;

import java.util.*;

/**
 * Created by Flo on 06.08.2018
 */
public class TaskStringEncryption extends AbstractObfuscationTask {

    public TaskStringEncryption(int decrypters) {
        this.decrypters = decrypters;
    }

    private int decrypters;

    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {

        HashMap<ClassNode, AbstractDecrypter> decrypters = new HashMap<>();

        if (nodes.size() == 1) {
            if(Main.settings.stringEncryption == Settings.StringEncryption.HEAVY) {
                decrypters.put(nodes.get(0),new HeavyDecrypter(nodes.get(0)));
            }
            if(Main.settings.stringEncryption == Settings.StringEncryption.VERYLIGHT) {
                decrypters.put(nodes.get(0),new VeryLightDecrypter(nodes.get(0)));
            }
        } else {
            for(int i = 0; i <= this.decrypters; i++) {
                ClassNode cn = nodes.get(rnd.nextInt(nodes.size()));
                if((cn.access & ACC_INTERFACE) != 0 || (cn.name.contains("$"))) {
                    i--;
                    continue;
                }
                if(!decrypters.containsKey(cn)) {
                    //stringBuilder = new StringBuilder();
                    //appendAccessFlags(cn.access | 262144);
                    //System.out.println(stringBuilder.toString() + " " + cn.name);
                    if(Main.settings.stringEncryption == Settings.StringEncryption.HEAVY) {
                        decrypters.put(nodes.get(0),new HeavyDecrypter(cn));
                    }
                    if(Main.settings.stringEncryption == Settings.StringEncryption.VERYLIGHT) {
                        decrypters.put(nodes.get(0),new VeryLightDecrypter(cn));
                    }
                }
            }
        }

        /*for(int i = 0; i < this.decrypters; i++) {
            ClassNode cn = new ClassNode();


            String s = nodes.get(rnd.nextInt(nodes.size())).name;
            String packageName = "";
            if(s.contains("/"))
                packageName = s.substring(0,s.lastIndexOf("/") +1);

            s = packageName + generateString(rnd,10,true);

            System.out.println(s);

            cn.visit(52, ACC_PUBLIC + ACC_SUPER, s, null, "java/lang/Object", null);
            decrypters.put(cn, new VeryLightDecrypter(cn));
        }*/

        //nodes.addAll(decrypters.keySet());

                nodes.parallelStream().forEach(node -> {

                    if(body != null)
                        Platform.runLater(() -> body.setText("Obfuscating strings in" + node.name));

                    if((node.access & ACC_ENUM) != 0)
                        return;
                    node.methods.parallelStream().forEach(m -> {

                        for (AbstractDecrypter d : decrypters.values()) {
                            if (d.node.name.equals(node.name) &&
                                    m.name.equals(d.methodName) && m.desc.equals(d.methodDesk))
                                return;
                        }

                        LabelNode prevLabel = null;

                        m.instructions.resetLabels();

                        for (AbstractInsnNode ain : m.instructions.toArray()) {

                            if (ain instanceof LabelNode) {
                                prevLabel = (LabelNode) ain;
                            }


                            //Replace ints with chars
                            if(ain instanceof MethodInsnNode) {
                                MethodInsnNode min = (MethodInsnNode)ain;
                                if(min.name.equals("append") && min.getOpcode() == INVOKEVIRTUAL && min.owner.equals("java/lang/StringBuilder")) {
                                    if(min.desc.startsWith("(C)")) {
                                        int opcode = min.getPrevious().getOpcode();
                                        if((opcode >= Opcodes.ICONST_M1 && opcode <= Opcodes.ICONST_5) || min.getPrevious() instanceof IntInsnNode) {
                                            m.instructions.insertBefore(min, new InsnNode(I2C));
                                        }
                                    }
                                }
                            }

                            if (ain instanceof LdcInsnNode) {
                                LdcInsnNode ldc = (LdcInsnNode) ain;
                                if (ldc.cst instanceof String) {

                                    int dec = rnd.nextInt(decrypters.size());
                                    AbstractDecrypter decrypter = (AbstractDecrypter) decrypters.values().toArray()[dec];

                                    decrypter.process(node,m,prevLabel,ldc);
                                    //System.out.println("Process Strings");

                            /*int offset = rnd.nextInt(1000);
                            int linenumber = rnd.nextInt(1000);
                            try {
                                int dec = rnd.nextInt(decrypters.size());
                                Decrypter decrypter = (Decrypter)decrypters.values().toArray()[dec];
                                Main.getString(ldc.cst.toString().getBytes(), offset, true);
                                String str = decrypter.getString(ldc.cst.toString(), offset, m.name, node.sourceFile, linenumber);

                                if (str.getBytes().length >= 65535)
                                    continue;


                                ldc.cst = str;
                                m.instructions.insertBefore(ain, new LineNumberNode(linenumber, prevLabel));
                                AbstractInsnNode prev;
                                m.instructions.insert(ain, prev = new IntInsnNode(offset <= 127 ? BIPUSH : SIPUSH, offset));
                                m.instructions.insert(prev, prev = new MethodInsnNode(INVOKESTATIC, decrypter.node.name, decrypter.methodName, decrypter.methodDesk, false));
                                m.instructions.insert(prev, new MethodInsnNode(INVOKEVIRTUAL, "java/lang/Object", "toString", "()Ljava/lang/String;", false));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/
                                }
                            }
                        }
                    });
                });
    }

    @Override
    public String getName() {
        return "String encryption";
    }

    StringBuilder stringBuilder;

    private void appendAccessFlags(int accessFlags) {
        boolean isEmpty = true;
        if ((accessFlags & 1) != 0) {
            this.stringBuilder.append("ACC_PUBLIC");
            isEmpty = false;
        }

        if ((accessFlags & 2) != 0) {
            this.stringBuilder.append("ACC_PRIVATE");
            isEmpty = false;
        }

        if ((accessFlags & 4) != 0) {
            this.stringBuilder.append("ACC_PROTECTED");
            isEmpty = false;
        }

        if ((accessFlags & 16) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            if ((accessFlags & 2097152) == 0) {
                this.stringBuilder.append("ACC_FINAL");
            } else {
                this.stringBuilder.append("ACC_TRANSITIVE");
            }

            isEmpty = false;
        }

        if ((accessFlags & 8) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_STATIC");
            isEmpty = false;
        }

        if ((accessFlags & 32) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            if ((accessFlags & 262144) == 0) {
                if ((accessFlags & 2097152) == 0) {
                    this.stringBuilder.append("ACC_SYNCHRONIZED");
                } else {
                    this.stringBuilder.append("ACC_TRANSITIVE");
                }
            } else {
                this.stringBuilder.append("ACC_SUPER");
            }

            isEmpty = false;
        }

        if ((accessFlags & 64) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            if ((accessFlags & 524288) == 0) {
                if ((accessFlags & 2097152) == 0) {
                    this.stringBuilder.append("ACC_BRIDGE");
                } else {
                    this.stringBuilder.append("ACC_STATIC_PHASE");
                }
            } else {
                this.stringBuilder.append("ACC_VOLATILE");
            }

            isEmpty = false;
        }

        if ((accessFlags & 128) != 0 && (accessFlags & 786432) == 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_VARARGS");
            isEmpty = false;
        }

        if ((accessFlags & 128) != 0 && (accessFlags & 524288) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_TRANSIENT");
            isEmpty = false;
        }

        if ((accessFlags & 256) != 0 && (accessFlags & 786432) == 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_NATIVE");
            isEmpty = false;
        }

        if ((accessFlags & 16384) != 0 && (accessFlags & 1835008) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_ENUM");
            isEmpty = false;
        }

        if ((accessFlags & 8192) != 0 && (accessFlags & 1310720) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_ANNOTATION");
            isEmpty = false;
        }

        if ((accessFlags & 1024) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_ABSTRACT");
            isEmpty = false;
        }

        if ((accessFlags & 512) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_INTERFACE");
            isEmpty = false;
        }

        if ((accessFlags & 2048) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_STRICT");
            isEmpty = false;
        }

        if ((accessFlags & 4096) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_SYNTHETIC");
            isEmpty = false;
        }

        if ((accessFlags & 131072) != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            this.stringBuilder.append("ACC_DEPRECATED");
            isEmpty = false;
        }

        /*if ((accessFlags & '耀') != 0) {
            if (!isEmpty) {
                this.stringBuilder.append(" | ");
            }

            if ((accessFlags & 262144) == 0) {
                this.stringBuilder.append("ACC_MANDATED");
            } else {
                this.stringBuilder.append("ACC_MODULE");
            }

            isEmpty = false;
        }*/

        if (isEmpty) {
            this.stringBuilder.append('0');
        }

    }

}

