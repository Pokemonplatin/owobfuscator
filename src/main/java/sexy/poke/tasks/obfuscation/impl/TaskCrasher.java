package sexy.poke.tasks.obfuscation.impl;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import sexy.poke.Main;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 11.08.2018
 */
public class TaskCrasher extends AbstractObfuscationTask {
    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        for (ClassNode cn : nodes) {
            if ((cn.access & ACC_INTERFACE) != 0)
                continue;

            Platform.runLater(() -> body.setText("Adding Crasher " + cn.name));


            for (MethodNode mn : cn.methods) {
                if (mn.instructions.getFirst() == null)
                    continue;

                InsnList tryCatch = new InsnList();

                LabelNode start = new LabelNode();
                LabelNode startTryCatch = new LabelNode();
                LabelNode end = new LabelNode();


                //Fernflower/CFR
                if (Main.settings.crasherOne) {
                    LabelNode label0 = new LabelNode();
                    LabelNode label1 = new LabelNode();
                    LabelNode label2 = new LabelNode();
                    LabelNode label3 = new LabelNode();
                    LabelNode label4 = new LabelNode();

                    tryCatch.add(start);

                    tryCatch.add(getIntPush(1));
                    tryCatch.add(new VarInsnNode(ISTORE, ++mn.maxLocals));
                    tryCatch.add(new VarInsnNode(ILOAD, mn.maxLocals));
                    tryCatch.add(getIntPush(2));

                    tryCatch.add(new JumpInsnNode(IF_ICMPLE, end));
                    tryCatch.add(startTryCatch);

                    tryCatch.add(new InsnNode(ACONST_NULL));

                    tryCatch.add(label1);
                    tryCatch.add(new InsnNode(NOP));
                    tryCatch.add(label2);
                    tryCatch.add(exitList(mn));

                    tryCatch.add(label3);
                    tryCatch.add(new InsnNode(NOP));
                    tryCatch.add(label4);
                    tryCatch.add(exitList(mn));

                    //tryCatch.add(new JumpInsnNode(GOTO, end));
                    tryCatch.add(end);


                    mn.tryCatchBlocks.add(new TryCatchBlockNode(label1, label2, label3, "java/lang/Throwable"));
                    mn.tryCatchBlocks.add(new TryCatchBlockNode(label3, label4, label1, "java/lang/Throwable"));

                    mn.instructions.insertBefore(mn.instructions.getFirst(), tryCatch);

                }


                /*if(true)
                {
                    LabelNode label0 = new LabelNode();
                    LabelNode label1 = new LabelNode();
                    LabelNode label2 = new LabelNode();
                    LabelNode label3 = new LabelNode();
                    LabelNode label4 = new LabelNode();


                    LabelNode l0 = new LabelNode();
                    LabelNode l1 = new LabelNode();
                    LabelNode l2 = new LabelNode();
                    LabelNode l3 = new LabelNode();
                    LabelNode l7 = new LabelNode();

                    tryCatch.add(start);

                    tryCatch.add(getIntPush(1));
                    tryCatch.add(new VarInsnNode(ISTORE, ++mn.maxLocals));
                    tryCatch.add(new VarInsnNode(ILOAD, mn.maxLocals));
                    tryCatch.add(getIntPush(2));

                    tryCatch.add(new JumpInsnNode(IF_ICMPLE, end));
                    tryCatch.add(startTryCatch);

                    int ins1 = ++mn.maxStack;
                    int ins2 = ++mn.maxStack;
                    int ins3 = ++mn.maxStack;


                    tryCatch.add(l0);

                    tryCatch.add(new FieldInsnNode(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
                    tryCatch.add(new LdcInsnNode(cn.name + " " + mn.name));
                    tryCatch.add(new MethodInsnNode(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));

                    tryCatch.add(l3);
                    tryCatch.add(new VarInsnNode(ALOAD, ins3));
                    tryCatch.add(new InsnNode(ATHROW));



                    tryCatch.add(new InsnNode(ACONST_NULL));

                    tryCatch.add(label1);
                    tryCatch.add(new InsnNode(NOP));
                    tryCatch.add(label2);
                    tryCatch.add(exitList(mn));

                    tryCatch.add(label3);
                    tryCatch.add(new InsnNode(NOP));
                    tryCatch.add(label4);
                    tryCatch.add(exitList(mn));




                    //tryCatch.add(new JumpInsnNode(GOTO, end));
                    tryCatch.add(end);





                    mn.tryCatchBlocks.add(new TryCatchBlockNode(label1, label2, label3, "java/lang/Throwable"));
                    mn.tryCatchBlocks.add(new TryCatchBlockNode(label3, label4, label1, "java/lang/Throwable"));

                    mn.instructions.insertBefore(mn.instructions.getFirst(), tryCatch);



                }*/

                //Krakatau/Procyon/JDGui?
                if (Main.settings.crasherTwo) {

                    start = new LabelNode();
                    end = new LabelNode();

                    LabelNode label1 = new LabelNode();
                    LabelNode label2 = new LabelNode();

                    tryCatch.add(start);
                    tryCatch.add(label1);
                    //tryCatch.add(new JumpInsnNode(GOTO, end));
                    //tryCatch.add(new InsnNode(ACONST_NULL));
                    //tryCatch.add(end);

                    //mn.tryCatchBlocks.add(new TryCatchBlockNode(start,start,end, "java/lang/Throwable"));

                    mn.instructions.insertBefore(mn.instructions.getFirst(), tryCatch);

                    mn.tryCatchBlocks.add(new TryCatchBlockNode(start, label1, start, "java/lang/Throwable"));
                }

                //FernFlower and JDGui
                if (Main.settings.crasherThree) {
                    start = new LabelNode();
                    end = new LabelNode();

                    LabelNode label1 = new LabelNode();
                    LabelNode label2 = new LabelNode();
                    LabelNode label3 = new LabelNode();

                    tryCatch.add(start);
                    tryCatch.add(new InsnNode(ICONST_0));
                    tryCatch.add(new JumpInsnNode(IFEQ, end));

                    tryCatch.add(label2);
                    tryCatch.add(new InsnNode(ICONST_0));
                    tryCatch.add(new JumpInsnNode(IFEQ, label1));

                    tryCatch.add(label3);
                    tryCatch.add(new InsnNode(ICONST_0));
                    tryCatch.add(new JumpInsnNode(IFEQ, label2));

                    tryCatch.add(new JumpInsnNode(GOTO, label3));
                    tryCatch.add(label1);
                    tryCatch.add(new JumpInsnNode(GOTO, label1));

                    tryCatch.add(end);

                    mn.instructions.insertBefore(mn.instructions.getFirst(), tryCatch);
                }

                if (mn.attrs == null)
                    mn.attrs = new ArrayList<>();

                mn.attrs.add(new TestAtt("AnnotationDefault"));

            }

            if (Main.settings.crasherASM) {
                if (cn.version <= Opcodes.V1_8) {
                    //cn.module = new ModuleNode("", ACC_MANDATED, "");
                    cn.nestHostClass = generateString(rnd, 5, false);
                    cn.nestMembers = new ArrayList<>();
                    for (int i = 0; i < 3; i++) {
                        cn.nestMembers.add(generateString(rnd, 5, false));
                    }


                    //cn.attrs = new ArrayList<>();
                    //cn.attrs.add(new TestAtt("NestHost"));
                }
            }
        }
    }

    class TestAtt extends Attribute {
        public TestAtt(String type) {
            super(type);
            content = "1".getBytes();
        }
    }

    @Override
    public String getName() {
        return "Crasher";
    }
}
