package sexy.poke.tasks.obfuscation.impl.removeal;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Flo on 08.08.2018
 */
public class TaskSourceFile extends AbstractObfuscationTask {

    public TaskSourceFile() {
    }

    @Override
    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {

        Platform.runLater(() -> body.setText(""));

        if (Main.settings.sourceFileRemoval == Settings.RemoveState.SHUFFLE) {
            List<String> sourceNames = new ArrayList<>();
            nodes.forEach(node -> sourceNames.add(node.sourceFile));
            Collections.shuffle(sourceNames);
            final int[] index = {0};
            nodes.forEach(node -> node.sourceFile = sourceNames.get(index[0]++));
        } else if (Main.settings.sourceFileRemoval == Settings.RemoveState.REPLACE) {
            nodes.parallelStream().forEach(node -> node.sourceFile = generateString(rnd, 10, false));
        } else if (Main.settings.sourceFileRemoval == Settings.RemoveState.REMOVE) {
            nodes.parallelStream().forEach(node -> node.sourceFile = "");
        }
    }

    @Override
    public String getName() {
        return "SourceFile";
    }
}
