package sexy.poke.tasks.obfuscation.impl.string;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.MethodNode;
import sexy.poke.util.RandomProvider;

import java.util.Random;

/**
 * Created by Flo on 07.08.2018
 */
public abstract class AbstractDecrypter extends RandomProvider implements Opcodes {
    public ClassNode node;
    protected Random rnd = new Random();
    protected String methodName;
    protected String methodDesk;

    public AbstractDecrypter(ClassNode node) {
        this.node = node;
    }

    protected abstract void process(ClassNode node, MethodNode methodNode, LabelNode prevLabel, LdcInsnNode ldc);
}
