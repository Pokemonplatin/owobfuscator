package sexy.poke.tasks.obfuscation.impl.removeal;

import sexy.poke.tasks.obfuscation.*;
import sexy.poke.mappings.*;
import javafx.scene.text.*;
import java.util.*;
import org.objectweb.asm.tree.*;

public class TaskRemoveCLString extends AbstractObfuscationTask {
    public void runTask(final List<ClassNode> nodes, final HashMap<String, SmallClassNode> mappings, final Text body) {
        for (final ClassNode cn : nodes) {
            cn.fields.removeIf(fn -> fn.value instanceof String && ((String) fn.value).startsWith("CL_"));
        }
    }

    public String getName() {
        return "CL String removal";
    }
}