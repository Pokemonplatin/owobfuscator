package sexy.poke.tasks.obfuscation;

import javafx.scene.text.Text;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;
import sexy.poke.Main;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.mappings.SmallPackageNode;
import sexy.poke.util.AsmUtil;
import sexy.poke.util.ExcludeElement;
import sexy.poke.util.RandomProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Flo on 04.08.2018
 */
public abstract class AbstractObfuscationTask extends AsmUtil implements Opcodes {

    protected static Random rnd = new Random();

    protected PostponeAction action;

    public void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings) {
        runTask(nodes, mappings, null);
    }

    public void setPostponeAction(PostponeAction action) {
        this.action = action;
    }

    public boolean validClass(ClassNode cn) {
        for (ExcludeElement ee : Main.settings.excludeList) {
            if (ee.type == ExcludeElement.Type.PACKAGE) {
                if (cn.name.startsWith(ee.name)) {
                    return false;
                }
            }
            if (ee.type == ExcludeElement.Type.CLASS) {
                if (cn.name.equals(ee.name)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validClass(String clazz) {
        //System.out.println(clazz);
        for (ExcludeElement ee : Main.settings.excludeList) {
            /*if (ee.type == ExcludeElement.Type.PACKAGE) {
                if (clazz.startsWith(ee.name)) {
                    return false;
                }
            }*/
            if (ee.type == ExcludeElement.Type.CLASS) {
                if (clazz.equals(ee.name)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validPackage(SmallPackageNode pcke) {
        for (ExcludeElement ee : Main.settings.excludeList) {
            if (ee.type == ExcludeElement.Type.PACKAGE) {
                if (pcke.getName().equals(ee.name) && pcke.getCompleteName().equals(ee.getCompleteName())) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validField(ClassNode owner, FieldNode fn) {
        for (ExcludeElement ee : Main.settings.excludeList) {
            if (ee.type == ExcludeElement.Type.FIELD) {
                if ((owner.name + fn.name).equals(ee.name)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validField(String owner, String field) {
        for (ExcludeElement ee : Main.settings.excludeList) {
            if (ee.type == ExcludeElement.Type.FIELD) {
                if ((owner + field).equals(ee.name)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validMethod(ClassNode owner, MethodNode mn) {
        for (ExcludeElement ee : Main.settings.excludeList) {
            if (ee.type == ExcludeElement.Type.METHOD) {
                if ((owner.name + mn.name + mn.desc).equals(ee.name)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean validMethod(String owner, String method, String desk) {
        for (ExcludeElement ee : Main.settings.excludeList) {
            if (ee.type == ExcludeElement.Type.METHOD) {

                if ((owner + method + desk).equals(ee.name)) {
                    return false;
                }
            }
        }
        return true;
    }

    public abstract void runTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body);

    public void postTask(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body) {
        if(action != null)
            action.execute(nodes,mappings,body);
    }

    public abstract String getName();

    public InsnList exitList(MethodNode methodNode) {

        InsnList list = new InsnList();
        Type returnType = Type.getReturnType(methodNode.desc);

        switch (returnType.getSort()) {
            case Type.VOID:
                list.add(new InsnNode(RETURN));
                break;
            case Type.BOOLEAN:
                list.add(getIntPush(rnd.nextInt(2)));
                list.add(new InsnNode(IRETURN));
                break;
            case Type.CHAR:
                list.add(getIntPush(rnd.nextInt(Character.MAX_VALUE + 1)));
                list.add(new InsnNode(IRETURN));
                break;
            case Type.BYTE:
                list.add(getIntPush(rnd.nextInt(Byte.MAX_VALUE + 1)));
                list.add(new InsnNode(IRETURN));
                break;
            case Type.SHORT:
                list.add(getIntPush(rnd.nextInt(Short.MAX_VALUE + 1)));
                list.add(new InsnNode(IRETURN));
                break;
            case Type.INT:
                list.add(getIntPush(rnd.nextInt()));
                list.add(new InsnNode(IRETURN));
                break;
            case Type.LONG:
                list.add(getIntPush(rnd.nextLong()));
                list.add(new InsnNode(LRETURN));
                break;
            case Type.FLOAT:
                list.add(getIntPush(rnd.nextFloat()));
                list.add(new InsnNode(FRETURN));
                break;
            case Type.DOUBLE:
                list.add(getIntPush(rnd.nextDouble()));
                list.add(new InsnNode(DRETURN));
                break;
            default:
                list.add(new InsnNode(ACONST_NULL));
                list.add(new InsnNode(ARETURN));
                break;
        }

        return list;
    }

    protected abstract class PostponeAction {
        public abstract void execute(List<ClassNode> nodes, HashMap<String, SmallClassNode> mappings, Text body);
    }

}
