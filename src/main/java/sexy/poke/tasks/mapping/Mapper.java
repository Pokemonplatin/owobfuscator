package sexy.poke.tasks.mapping;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.mappings.SmallFieldNode;
import sexy.poke.mappings.SmallMethodNode;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Flo on 04.08.2018
 */
public class Mapper {

    public static HashMap<String, SmallClassNode> generateMappings(ArrayList<ClassNode> target, ArrayList<ClassNode> libs) {
        return generateMappings(target,libs,null);
    }

    public static HashMap<String, SmallClassNode> generateMappings(ArrayList<ClassNode> target, ArrayList<ClassNode> libs, Text body) {
        HashMap<String, SmallClassNode> mapping = new HashMap<>();

        //map libs
        for (ClassNode c : libs) {
            if(body != null) {
                Platform.runLater(() -> body.setText("Mapping lib " + c.name));
            }
            mapping.put(c.name, new SmallClassNode(c, true));
        }

        //map target Jar
        for (ClassNode c : target) {
            if(body != null) {
                Platform.runLater(() -> body.setText("Mapping jar " + c.name));
            }
            mapping.put(c.name, new SmallClassNode(c, false));
        }

        if(body != null) {
            Platform.runLater(() -> body.setText("Linking classes"));
        }

        //link the classes
        mapping.forEach((name, map) -> {
            map.linkClasses(mapping);
        });

        if(body != null) {
            Platform.runLater(() -> body.setText("Linking Methods"));
        }

        //link the methods

        int[] i = new int[]{0};

        mapping.forEach((name, map) -> {
            map.linkMethods();
            map.linkFields();
            i[0]++;

            //System.out.println(mapping.size() + " -> " + i[0]);
        });

        return mapping;
    }

    public static void printMappings(HashMap<String, SmallClassNode> mapping) throws IOException {
        StringBuilder map = new StringBuilder();

        for (SmallClassNode scn : mapping.values()) {
            if (!scn.isLib()) {
                map.append(scn.getClassName()).append(" -> ").append(scn.hasNewName() ? scn.getClassName() : scn.getNewName()).append("\n");

                map.append(" fields\n");

                for (SmallFieldNode sfn : scn.getFiledNodes()) {
                    map.append("    ").append(sfn.getCombined()).append(" -> ").append(sfn.getNewName() == null ? sfn.getName() : sfn.getNewName()).append("\n");
                }

                map.append(" methods\n");

                for (SmallMethodNode smn : scn.getMethodNodes()) {
                    map.append("    ").append(smn.getCombined()).append(" -> ").append(smn.getNewName() == null ? smn.getName() : smn.getNewName()).append("\n");
                }
            }
        }

        Files.write(new File("mapping.txt").toPath(), map.toString().getBytes());
    }
}
