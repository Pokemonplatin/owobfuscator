package sexy.poke.tasks;

import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.Settings;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.mapping.Mapper;
import sexy.poke.tasks.obfuscation.AbstractObfuscationTask;
import sexy.poke.tasks.obfuscation.impl.*;
import sexy.poke.tasks.obfuscation.impl.access.TaskPublicAccess;
import sexy.poke.tasks.obfuscation.impl.access.TaskSyntheticAccess;
import sexy.poke.tasks.obfuscation.impl.flow.TaskFlowObfu3;
import sexy.poke.tasks.obfuscation.impl.flow.TaskSwitchCase;
import sexy.poke.tasks.obfuscation.impl.removeal.*;
import sexy.poke.tasks.obfuscation.impl.string.TaskStringEncryption;
import sexy.poke.tasks.obfuscation.impl.string.TaskStringPool;
import sexy.poke.util.JarUtils;
import sexy.poke.util.RandomProvider;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Flo on 26.08.2018
 */
public class Obfuscator {

    static {
        init();
    }

    public static void init() {
        alert = new JFXAlert(Main.stage);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setOverlayClose(false);
        layout = new JFXDialogLayout();
        layout.setHeading(new Label("Processing"));

        VBox vbox = new VBox();
        vbox.setSpacing(10);

        body = new Text();
        subBody = new Text();

        vbox.getChildren().add(body);
        vbox.getChildren().add(subBody);

        layout.setBody(vbox);

        alert.setContent(layout);
    }

    public static JFXAlert alert;
    public static JFXDialogLayout layout;
    public static Text body;
    public static Text subBody;

    public static boolean reloadClasses = false;

    private static List<AbstractObfuscationTask> executedtasks = new ArrayList<>();

    public static void obfuscate() {

        alert.show();

        if (Main.settings.inputFile == null) {

            Platform.runLater(() -> {
                JFXButton closeButton = new JFXButton("Close");
                closeButton.getStyleClass().add("dialog-accept");
                closeButton.setOnAction(event -> {
                    alert.hideWithAnimation();
                    layout.setActions();
                });
                layout.setActions(closeButton);
                subBody.setText("");
                body.setText("No input file selected");
            });

            return;
        }

        if (reloadClasses) {
            try {
                Main.settings.nodes.clear();
                Main.settings.cleanNodes.clear();
                JarUtils.loadClasses(Main.settings.inputFile, Obfuscator.subBody).forEach((name, c) -> {
                    Main.settings.nodes.add(c);
                });
                Main.settings.cleanNodes.addAll(Main.settings.nodes);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        reloadClasses = true;

        //popup.show(Main.stage, JFXPopup.PopupVPosition.TOP, PopupHPosition.LEFT);


        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    Settings settings = Main.settings;

                    ArrayList<ClassNode> libClasses = new ArrayList<ClassNode>();
                    ArrayList<ClassNode> targetClasses = Main.settings.nodes;
                    HashMap<String, byte[]> out = new HashMap<>();

                    Platform.runLater(() -> {
                        body.setText("Load Jar");
                    });


                    addSoftwareLibrary(Main.settings.inputFile);

                    Platform.runLater(() -> {
                        body.setText("Load Libs");
                    });


                    out.putAll(JarUtils.loadNonClassEntries(Main.settings.inputFile));


                    Platform.runLater(() -> {
                        body.setText("Generate Mappings");
                    });

                    HashMap<String, SmallClassNode> mapping = new HashMap<>();

                    RandomProvider.setupCustomPool();

                    executedtasks.clear();

                    if (settings.clIdsRemoval != Settings.RemoveState.OFF) {
                        process(new TaskCLObfuscator(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.dictionaryClasses != Settings.ObfuscationDictionary.OFF || settings.dictionaryFields != Settings.ObfuscationDictionary.OFF || settings.dictionaryMethods != Settings.ObfuscationDictionary.OFF) {

                        for (File file : Main.settings.libs) {
                            addSoftwareLibrary(file);
                            JarUtils.loadClasses(file, subBody).forEach((name, c) -> {
                                libClasses.add(c);
                            });
                        }

                        mapping.putAll(Mapper.generateMappings(targetClasses, libClasses, subBody));

                        process(new TaskRemapper(), targetClasses, mapping, body, subBody);


                        Platform.runLater(() -> body.setText("Fix Bundles"));

                        HashMap<String, byte[]> remapped = new HashMap<>();
                        for (Map.Entry<String, byte[]> e : out.entrySet()) {
                            String name = e.getKey();
                            if (name.endsWith(".properties")) {
                                name = name.replace(".properties", "");
                                if (name.contains("_")) {

                                    String[] subs = name.split("_");

                                    SmallClassNode scn = mapping.get(subs[0]);
                                    if (scn.hasNewName())
                                        name = scn.getNewName();

                                    boolean skip = true;
                                    for (String str : subs) {
                                        if (skip) {
                                            skip = false;
                                            continue;
                                        }
                                        name += "_" + str;
                                    }
                                } else {
                                    SmallClassNode scn = mapping.get(name);
                                    if (scn != null && scn.hasNewName())
                                        name = scn.getNewName();
                                }

                                name += ".properties";
                            }

                            remapped.put(name, e.getValue());
                        }

                        out.clear();
                        out.putAll(remapped);

                    }

                    if (settings.sourceFileRemoval != Settings.RemoveState.OFF) {
                        process(new TaskSourceFile(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.sourceDebugRemoval != Settings.RemoveState.OFF) {
                        process(new TaskDebugFile(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.localVariablesRemoval != Settings.RemoveState.OFF) {
                        process(new TaskLocalVariableRemover(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.visibleAnnotationsRemoval != Settings.RemoveState.OFF || settings.invisibleAnnotationsRemoval != Settings.RemoveState.OFF) {
                        process(new TaskAnnotationRemover(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.innerClassesRemoval != Settings.RemoveState.OFF ||
                            settings.outerClassesRemoval != Settings.RemoveState.OFF ||
                            settings.outerMethodsRemoval != Settings.RemoveState.OFF) {
                        process(new TaskInnerOuterRemover(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.signatureRemoval != Settings.RemoveState.OFF) {
                        process(new TaskSignatureRemover(), targetClasses, mapping, body, subBody);
                    }

                    /*if (settings.localVariablesSignaturesRemoval != Settings.RemoveState.OFF) {
                        process(new TaskLocalVariableRemover(),targetClasses,mapping,body,subBody);
                    }*/

                    /*if (settings.signatureFucker) {
                        process(new TaskSignatureFucker(),targetClasses,mapping,body,subBody);
                    }*/

                    //process(new TaskCrasher(), targetClasses, mapping, body, subBody);

                    //process(new TaskFlowObfu3(), targetClasses, mapping, body, subBody);


                    if (settings.stringPool) {
                        process(new TaskStringPool(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.stringEncryption != Settings.StringEncryption.OFF) {
                        process(new TaskStringEncryption(settings.stringEncryptionDecrypters), targetClasses, mapping, body, subBody);

                        for (ListIterator<ClassNode> iterator = targetClasses.listIterator(); iterator.hasNext(); ) {
                            iterator.set(JarUtils.getNode(JarUtils.getNodeBytes(iterator.next(), mapping)));
                        }
                    }

                    if (Main.settings.shuffleMethods || Main.settings.shuffleFields) {
                        process(new TaskShuffle(), targetClasses, mapping, body, subBody);
                    }

                    if (Main.settings.invokeDynamic_Methods || Main.settings.invokeDynamic_Fields) {
                        process(new TaskInvokeDynamic(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.flowObfuscation) {
                        process(new TaskFlowObfu3(settings.flowObfuscation_ExtraAggressive), targetClasses, mapping, body, subBody);
                    }

                    if (Main.settings.crasherOne || Main.settings.crasherTwo || Main.settings.crasherThree || Main.settings.crasherASM) {
                        process(new TaskCrasher(), targetClasses, mapping, body, subBody);

                        /*for (ListIterator<ClassNode> iterator = targetClasses.listIterator(); iterator.hasNext(); ) {
                            iterator.set(JarUtils.getNode(JarUtils.getNodeBytes(iterator.next(), mapping)));
                        }*/
                    }

                    if (settings.flowSwitchCase) {
                        process(new TaskSwitchCase(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.publicClasses || settings.publicMethods || settings.publicFields) {
                        process(new TaskPublicAccess(), targetClasses, mapping, body, subBody);
                    }

                    if (settings.syntheticClasses || settings.syntheticMethods || settings.syntheticFields) {
                        process(new TaskSyntheticAccess(), targetClasses, mapping, body, subBody);
                    }


                    /*if (Main.settings.invokeDynamic_Methods || Main.settings.invokeDynamic_Fields) {
                        process(new TaskInvokeDynamic(), targetClasses, mapping, body, subBody);
                    }*/
                    //process(new TaskCrasher(), targetClasses, mapping, body, subBody);
                    //process(new TaskGotoObfu(), targetClasses, mapping, body, subBody);

                    postPorcess(targetClasses, mapping, body, subBody);


                    Platform.runLater(() -> body.setText("Finishing Jar"));


                    targetClasses.forEach(c1 -> {

                        Platform.runLater(() -> subBody.setText("Process " + c1.name));

                        byte[] b = JarUtils.getNodeBytes(c1, mapping);
                        if (b != null)
                            out.put(c1.name, b);
                    });

                    /*targetClasses.parallelStream().forEach(c1 -> {

                        Platform.runLater(() -> subBody.setText("Process " + c1.name));


                        byte[] b = JarUtils.getNodeBytes(c1, mapping);
                        if (b != null)
                            out.put(c1.name, b);
                        //c1.accept(new TraceClassVisitor(null, new ASMifier(), new PrintWriter(System.out)));
                    });*/

                    JarUtils.saveAsJar(out, Main.settings.outputFile.getAbsolutePath());

                    Platform.runLater(() -> {
                        JFXButton closeButton = new JFXButton("Close");
                        closeButton.getStyleClass().add("dialog-accept");
                        closeButton.setOnAction(event -> {
                            alert.hideWithAnimation();
                            layout.setActions();
                        });
                        layout.setActions(closeButton);
                        subBody.setText("");
                        body.setText("Finished");
                    });
                } catch (Exception e) {
                    e.printStackTrace();

                    Platform.runLater(() -> {
                        JFXButton closeButton = new JFXButton("Close");
                        closeButton.getStyleClass().add("dialog-accept");
                        closeButton.setOnAction(event -> {
                            alert.hideWithAnimation();
                            layout.setActions();
                        });
                        layout.setActions(closeButton);
                        subBody.setText("");
                        body.setText("Finished with exception");
                    });
                }
            }
        });

        t.start();
    }

    private static void process(AbstractObfuscationTask task, ArrayList<ClassNode> targetClasses, HashMap<String, SmallClassNode> mapping, Text body, Text subBody) {
        Platform.runLater(() -> body.setText(task.getName()));
        task.runTask(targetClasses, mapping, subBody);
        executedtasks.add(task);
    }

    private static void postPorcess(ArrayList<ClassNode> targetClasses, HashMap<String, SmallClassNode> mapping, Text body, Text subBody) {
        Platform.runLater(() -> body.setText("Post process"));
        for (AbstractObfuscationTask task : executedtasks) {
            task.postTask(targetClasses, mapping, subBody);
        }
    }

    private static void addSoftwareLibrary(File file) {
        try {
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
            method.invoke(ClassLoader.getSystemClassLoader(), file.toURI().toURL());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
