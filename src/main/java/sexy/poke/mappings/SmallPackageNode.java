package sexy.poke.mappings;

import java.util.function.Supplier;

public class SmallPackageNode {
    public SmallPackageNode(SmallPackageNode parent, String name) {
        this.name = name;
        this.newName = name;
        this.parent = parent;
    }

    private String name;
    private String newName;
    private SmallPackageNode parent;

    public String getName() {
        return name;
    }

    public String getNewName() {
        return newName;
    }

    public String getCompleteName() {
        String parent = this.parent != null ? (this.parent.getCompleteName() + "/") : "";

        return parent +  name;
    }

    public String getCompleteNewName() {
        String parent = this.parent != null ? (this.parent.getCompleteNewName() + "/") : "";

        return parent +  newName;
    }

    public void setName(String name) {
        this.newName = name;
    }

    public boolean hasNewName() {
        return newName != null;
    }

    public SmallPackageNode getParent() {
        return parent;
    }
}
