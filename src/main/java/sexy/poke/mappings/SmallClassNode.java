package sexy.poke.mappings;


import org.objectweb.asm.tree.ClassNode;
import sexy.poke.util.AccessHelper;
import sexy.poke.util.AsmUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Flo on 03.08.2018
 */
public class SmallClassNode {
    public SmallClassNode(ClassNode node, boolean lib, boolean map) {
        this.lib = lib;

        superClassName = node.superName;

        className = node.name;
        outerClassName = node.outerClass;

        isEnum = AccessHelper.isEnum(node.access);

        if (map) {
            interfaceNames.addAll(node.interfaces);
            node.methods.forEach(mn -> methodNodes.add(new SmallMethodNode(this, mn)));
            node.fields.forEach(fn -> filedNodes.add(new SmallFieldNode(this, fn)));
        }
    }

    public SmallClassNode(ClassNode node) {
        this(node, false, true);
    }

    public SmallClassNode(ClassNode node, boolean lib) {
        this(node, lib, true);
        if (!lib) {
            String[] packages = node.name.split("\\/");

            SmallPackageNode parent = null;

            for (int i = 0; i < packages.length - 1; i++) {
                int finalI = i;
                SmallPackageNode lastP = packeges.parallelStream().filter(p -> p.getName().equals(packages[finalI])).findFirst().orElseGet(() -> null);
                if (lastP == null) {
                    packeges.add(lastP = new SmallPackageNode(parent, packages[finalI]));
                }
                parent = lastP;
            }

            this.parent = parent;

            className = packages[packages.length - 1];
        }
    }

    public static ArrayList<SmallPackageNode> packeges = new ArrayList<>();

    private boolean lib;
    private boolean isEnum;
    private SmallPackageNode parent;
    private String className;
    private String newClassName;

    private Set<SmallClassNode> interfaces = new HashSet<>();
    private Set<SmallClassNode> implemented = new HashSet<>();
    private SmallClassNode superClass;
    private SmallClassNode outerClass;

    private Set<SmallMethodNode> methodNodes = new HashSet<>();
    private Set<SmallFieldNode> filedNodes = new HashSet<>();

    private HashMap<String, SmallMethodNode> methods = new HashMap<>();
    //private HashMap<String, SmallMethodNode> methodsWithoutDesk = new HashMap<>();
    private HashMap<String, SmallFieldNode> fields = new HashMap<>();

    private final String superClassName;
    private final String outerClassName;
    private final Set<String> interfaceNames = new HashSet<>();

    public void linkClasses(HashMap<String, SmallClassNode> mappings) {
        superClass = mappings.getOrDefault(superClassName, null);

        for (String interf : interfaceNames) {
            SmallClassNode smn = mappings.get(interf);
            if (smn != null) {
                smn.implemented.add(this);
                interfaces.add(smn);
            }
        }

        outerClass = mappings.getOrDefault(outerClassName, null);
    }

    public void linkMethods() {
        Set<SmallMethodNode> keep = new HashSet<>();
        for (SmallMethodNode smn : methodNodes) {

            SmallMethodNode tmp = smn;
            SmallMethodNode newNode = checkMethod(smn.getCombined(), true);

            if (newNode != null) {
                tmp = newNode;
            }

            keep.add(tmp);
            methods.put(tmp.getCombined(), tmp);
            //methodsWithoutDesk.put(ExcludeGui.getName(), ExcludeGui);


            for (SmallClassNode smc : implemented) {
                SmallMethodNode libNode = smc.checkMethodInLibs(smn.getCombined(), false);
                if (libNode != null)
                    tmp.forceNewName(tmp.getName());
            }
        }
        methodNodes = keep;
    }

    public void linkFields() {
        for (SmallFieldNode sfn : filedNodes) {
            fields.put(sfn.getCombined(), sfn);
        }
    }

    public SmallFieldNode getField(String combined) {
        return fields.getOrDefault(combined, null);
    }

    public SmallMethodNode getMethod(String combined) {
        return methods.getOrDefault(combined, null);
    }

    /*public SmallMethodNode getMethodWithoutDesk(String name) {
        return methodsWithoutDesk.getOrDefault(name, null);
    }*/

    public SmallClassNode getSuperClass() {
        return superClass;
    }

    /*
        Checks if the combined methodname (name + desk) is available in the class or its super/interface classes
     */
    public SmallMethodNode checkMethod(String combined, boolean ignoreOwnMethods) {
        for (SmallClassNode scn : interfaces) {
            SmallMethodNode smn = scn.checkMethod(combined, false);
            if (smn != null)
                return smn;
        }

        if (superClass != null) {
            SmallMethodNode smn = superClass.checkMethod(combined, false);
            if (smn != null)
                return smn;
        }

        if (!ignoreOwnMethods) {
            for (SmallMethodNode smn : methodNodes) {
                if (smn.getCombined().equals(combined))
                    return smn;
            }
        }

        return null;
    }

    /*
    Checks if the combined fieldName (name + desk) is available in the class or its super/interface classes
    */
    public SmallFieldNode checkField(String combined, boolean ignoreOwnMethods) {
        for (SmallClassNode scn : interfaces) {
            SmallFieldNode sfn = scn.checkField(combined, false);
            if (sfn != null)
                return sfn;
        }

        if (superClass != null) {
            SmallFieldNode sfn = superClass.checkField(combined, false);
            if (sfn != null)
                return sfn;
        }

        if (!ignoreOwnMethods) {
            for (SmallFieldNode sfn : filedNodes) {
                if (sfn.getCombined().equals(combined))
                    return sfn;
            }
        }

        return null;
    }

    /*
        Checks if the combined methodname (name + desk) is available in the class or its super/interface classes
    */
    public SmallMethodNode checkMethodWithoutDesk(String name, boolean ignoreOwnMethods) {
        if (!ignoreOwnMethods) {
            for (SmallMethodNode smn : methodNodes) {
                if (smn.getName().equals(name))
                    return smn;
            }
        }

        for (SmallClassNode scn : interfaces) {
            SmallMethodNode smn = scn.checkMethodWithoutDesk(name, false);
            if (smn != null)
                return smn;
        }

        if (superClass != null)
            return superClass.checkMethodWithoutDesk(name, false);

        return null;
    }

    /*
    Checks if the combined methodname (name + desk) is available in the class or its super/interface classes
    */
    public SmallMethodNode checkMethodInLibs(String combined, boolean ignoreOwnMethods) {
        if (!ignoreOwnMethods) {
            if (isLib()) {
                for (SmallMethodNode smn : methodNodes) {
                    if (smn.getCombined().equals(combined))
                        return smn;
                }
            }
        }

        for (SmallClassNode scn : interfaces) {
            SmallMethodNode smn = scn.checkMethodInLibs(combined, false);
            if (smn != null)
                return smn;
        }

        if (superClass != null)
            return superClass.checkMethodInLibs(combined, false);

        return null;
    }

    public String getClassName() {
        /*StringBuilder pck = new StringBuilder();
        SmallPackageNode p = parent;
        while(p != null) {
            pck.append(p.getName()).append("/");
            p = p.getParent();
        }*/

        String parent = this.parent != null ? (this.parent.getCompleteName() + "/") : "";

        return parent + className;

        //return pck.toString() + className;
    }

    public boolean isLib() {
        return lib;
    }

    public void setLib(boolean isLib) {
        this.lib = isLib;
    }

    public Set<SmallMethodNode> getMethodNodes() {
        return methodNodes;
    }

    public Set<SmallFieldNode> getFiledNodes() {
        return filedNodes;
    }

    public String getNewName() {

        /*StringBuilder pck = new StringBuilder();
        SmallPackageNode p = parent;
        while(p != null) {
            pck.append(p.getNewName()).append("/");
            p = p.getParent();
        }*/
        if (newClassName == null)
            return null;

        String parent = this.parent != null ? (this.parent.getCompleteNewName() + "/") : "";

        return parent + newClassName;

        // return pck.toString() + newClassName;
    }

    public String getSimpleName() {
        return className;
    }

    public String getSimpleNewName() {
        return newClassName;
    }

    public boolean hasNewName() {
        return newClassName != null;
    }

    public void setNewName(String name) {
        newClassName = name;
    }

    public boolean isEnum() {
        return isEnum;
    }
}
