package sexy.poke.mappings;


import org.objectweb.asm.tree.MethodNode;
import sexy.poke.util.AccessHelper;

/**
 * Created by Flo on 03.08.2018
 */
public class SmallMethodNode {
    public SmallMethodNode(SmallClassNode owner, MethodNode mn) {
        this(owner, mn.name, mn.desc, AccessHelper.isStatic(mn.access));
    }

    SmallMethodNode(SmallClassNode owner, String name, String desk, Boolean isStatic) {
        this.name = name;
        this.desk = desk;
        this.owner = owner;
        combined = name + desk;

        this.isStatic = isStatic;



        /*if (name.equals("main")) {
            owner.setLib(true);
        }*/
    }

    private boolean isStatic;
    private String name;
    private String desk;

    private SmallClassNode owner;

    private String combined;

    private String newName;

    public String getName() {
        return name;
    }

    public String getDesk() {
        return desk;
    }

    public String getCombined() {
        return combined;
    }

    public SmallClassNode getOwner() {
        return owner;
    }

    public String getNewName() {
        return newName;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setNewName(String newName) {
        if (owner.isLib()) {
            throw new IllegalStateException("Cannot redefine lib method name");
        }
        this.newName = newName;
    }

    @Deprecated
    public void forceNewName(String newName) {
        this.newName = newName;
    }
}
