package sexy.poke.mappings;

import org.objectweb.asm.tree.FieldNode;
import sexy.poke.util.AccessHelper;

/**
 * Created by Flo on 03.08.2018
 */
public class SmallFieldNode extends SmallMethodNode {
    public SmallFieldNode(SmallClassNode owner, FieldNode fn) {
        super(owner, fn.name, fn.desc, AccessHelper.isStatic(fn.access));
    }
}
