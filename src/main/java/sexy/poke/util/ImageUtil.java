package sexy.poke.util;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Flo on 07.10.2018
 */
public class ImageUtil {

    public static HashMap<Integer, Image> methodIcons = new HashMap<>();
    public static HashMap<Integer, Image> fieldIcons = new HashMap<>();

    public static Image pack;

    public static Image clazz;
    public static Image clazzInterface;
    public static Image clazzEnum;

    public static BufferedImage mpub;
    public static BufferedImage mpri;
    public static BufferedImage mpro;
    public static BufferedImage mdef;

    public static BufferedImage fpub;
    public static BufferedImage fpri;
    public static BufferedImage fpro;
    public static BufferedImage fdef;

    public static BufferedImage abs;
    public static BufferedImage fin;
    public static BufferedImage nat;

    public static BufferedImage stat;
    public static BufferedImage syn;


    static {
        try {
            pack = SwingFXUtils.toFXImage(ImageIO.read(ImageUtil.class.getResourceAsStream("/img/package.png")), null);
            clazz = SwingFXUtils.toFXImage(ImageIO.read(ImageUtil.class.getResourceAsStream("/img/class.png")), null);
            clazzInterface = SwingFXUtils.toFXImage(ImageIO.read(ImageUtil.class.getResourceAsStream("/img/interface.png")), null);
            clazzEnum = SwingFXUtils.toFXImage(ImageIO.read(ImageUtil.class.getResourceAsStream("/img/enum.png")), null);

            mpub = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/mpub.png"));
            mpri = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/mpri.png"));
            mpro = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/mpro.png"));
            mdef = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/mdef.png"));

            fpub = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/fpub.png"));
            fpri = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/fpri.png"));
            fpro = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/fpro.png"));
            fdef = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/fdef.png"));

            stat = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/static.png"));

            abs = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/abstract.png"));
            fin = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/final.png"));
            nat = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/native.png"));
            syn = ImageIO.read(ImageUtil.class.getResourceAsStream("/img/synthetic.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Image generateIcon(int access, boolean method) {

        if(method) {
            Image i = methodIcons.get(access);
            if(i != null)
                return i;
        } else {
            Image i = fieldIcons.get(access);
            if (i != null)
                return i;
        }

        BufferedImage template;
        if (method) {
            template = AccessHelper.isPublic(access) ? mpub : (AccessHelper.isPrivate(access) ? mpri : (AccessHelper.isProtected(access) ? mpro : mdef));
        } else {
            template = AccessHelper.isPublic(access) ? fpub : (AccessHelper.isPrivate(access) ? fpri : (AccessHelper.isProtected(access) ? fpro : fdef));
        }

        if (AccessHelper.isAbstract(access)) {
            template = combineAccess(template, abs, true);
        } else {
            boolean scndRight = true;
            if (AccessHelper.isFinal(access)) {
                template = combineAccess(template, fin, true);
                scndRight = false;
            } else if (AccessHelper.isNative(access)) {
                template = combineAccess(template, nat, true);
                scndRight = false;
            }
            if (AccessHelper.isStatic(access)) {
                template = combineAccess(template, stat, scndRight);
            } else if (AccessHelper.isSynthetic(access)) {
                template = combineAccess(template, syn, scndRight);
            }
        }

        Image img = SwingFXUtils.toFXImage(template, null);

        if (method) {
            methodIcons.put(access, img);
        } else {
            fieldIcons.put(access, img);
        }

        return img;
    }

    private static BufferedImage combineAccess(BufferedImage icon1, BufferedImage icon2, boolean right) {
        int w = icon1.getWidth();
        int h = icon1.getHeight();
        BufferedImage image = new BufferedImage(w, h, 2);
        Graphics2D g2 = image.createGraphics();
        g2.drawImage(icon1, 0, 0, null);
        g2.drawImage(icon2, right ? w / 4 : w / -4, h / -4, null);
        g2.dispose();
        return image;
    }
}
