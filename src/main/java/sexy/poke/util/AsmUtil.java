package sexy.poke.util;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Flo on 15.08.2018
 */
public class AsmUtil extends RandomProvider implements Opcodes {
    public static AbstractInsnNode getIntPush(int i) {
        if (i <= 5 && i >= -1) {
            return new InsnNode(i + 3);
        } else if (i <= 127 && i >= -128) {
            return new IntInsnNode(BIPUSH, i);
        } else if (i <= 32767 && i >= -32768) {
            return new IntInsnNode(SIPUSH, i);
        }
        return new LdcInsnNode(i);
    }

    public static AbstractInsnNode getIntPush(long number) {
        if (number >= 0 && number <= 1) {
            return new InsnNode((int) (number + 9));
        } else {
            return new LdcInsnNode(number);
        }
    }

    public static AbstractInsnNode getIntPush(float number) {
        if (number >= 0 && number <= 2) {
            return new InsnNode((int) (number + 11));
        } else {
            return new LdcInsnNode(number);
        }
    }

    public static AbstractInsnNode getIntPush(double number) {
        if (number >= 0 && number <= 1) {
            return new InsnNode((int) (number + 14));
        } else {
            return new LdcInsnNode(number);
        }
    }

    public static AbstractInsnNode getIntPush(Number number, int valueType) {
        switch (valueType) {
            case 0:
                return getIntPush(number.intValue());
            case 1:
                return getIntPush(number.shortValue());
            case 2:
                return getIntPush(number.byteValue());
            case 3:
                return getIntPush(number.floatValue());
            case 4:
                return getIntPush(number.doubleValue());
            case 5:
                return getIntPush(number.longValue());
        }
        return getIntPush(-1);
    }

    /**
     * Generates a random goto that will never reach the input Label
     * @param trash the input label
     * @param field field to check in if state
     * @param fieldOwner owner of the field
     * @return List of generated if clause
     */
    public static InsnList generateRandomGoto(LabelNode trash, FieldNode field, String fieldOwner) {
        InsnList listA = new InsnList();
        InsnList listB = new InsnList();

        Random rnd = new Random();

        boolean fieldFirst = rnd.nextBoolean();
        int valueAType = rnd.nextInt(5);//Int 0 , Short 1, Byte 2, Float 3, Double 4, Long 5
        int valueBType = rnd.nextInt(5);//Int 0 , Short 1, Byte 2, Float 3, Double 4, Long 5

        Number valueA = getRandomNumber(valueAType);

        Number valueB;
        if (field == null) {

            if (rnd.nextInt(100) < 100 / 3) {
                valueB = valueA;
                valueBType = valueAType;
            } else {
                valueB = getRandomNumber(valueBType);
            }

            listB.add(getIntPush(valueB, valueBType));
        } else {
            valueB = (Number) field.value;
            //TODO change valueType

            if (rnd.nextInt(100) < 100 / 3) {
                valueA = valueB;
                valueAType = valueBType;
            }

            listB.add(new FieldInsnNode((field.access & Opcodes.ACC_STATIC) != 0 ? GETSTATIC : GETFIELD, fieldOwner, field.name, field.desc));
        }

        listA.add(getIntPush(valueA, valueAType));

        if (fieldFirst) {
            Number valueTmp = valueB;
            valueB = valueA;
            valueA = valueTmp;

            int valueTypeTmp = valueBType;
            valueBType = valueAType;
            valueAType = valueTypeTmp;

            InsnList listTmp = listB;
            listB = listA;
            listA = listTmp;
        }

        //generate cast
        int cast = getCastOpCode(valueAType, valueBType);
        if (cast != -1)
            listA.add(new InsnNode(cast));

        listA.add(listB);

        //calc difference
        int compare = compare(valueAType, valueA, valueBType, valueB);

        //generate compare nodes
        listA.add(getCompareOpCode(valueBType, compare, trash));

        return listA;
    }

    /**
     * compares two Numbers
     * @param valueAType the type of the first number
     * @param valueA the first number
     * @param valueBType the type of the second number
     * @param valueB the second number
     * @return returns -1 if value one is smaler and 1 if its bigger
     */
    private static int compare(int valueAType, Number valueA, int valueBType, Number valueB) {
        int compare = 0;
        if (valueAType <= 2) {
            int v1 = valueA.intValue();

            if (valueBType <= 2) {
                int v2 = valueB.intValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
            if (valueBType == 3 || valueBType == 4) {
                double v2 = valueB.doubleValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
            if (valueBType == 5) {
                long v2 = valueB.longValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
        }
        if (valueAType == 3 || valueAType == 4) {
            double v1 = valueA.doubleValue();
            if (valueBType <= 2) {
                int v2 = valueB.intValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
            if (valueBType == 3 || valueBType == 4) {
                double v2 = valueB.doubleValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
            if (valueBType == 5) {
                long v2 = valueB.longValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
        }
        if (valueAType == 5) {
            long v1 = valueA.longValue();
            if (valueBType <= 2) {
                int v2 = valueB.intValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
            if (valueBType == 3 || valueBType == 4) {
                double v2 = valueB.doubleValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
            if (valueBType == 5) {
                long v2 = valueB.longValue();
                compare = v1 != v2 ? (v1 > v2 ? 1 : -1) : 0;
            }
        }
        return compare;
    }

    /**
     * returns a random number of the given type
     * @param index number type
     * @return random number
     */
    private static Number getRandomNumber(int index) {
        switch (index) {
            case 0:
                return ThreadLocalRandom.current().nextInt();
            case 1:
                return (short) ThreadLocalRandom.current().nextInt();
            case 2:
                return (byte) ThreadLocalRandom.current().nextInt();
            case 3:
                return ThreadLocalRandom.current().nextFloat() + ThreadLocalRandom.current().nextInt();
            case 4:
                return ThreadLocalRandom.current().nextDouble() + ThreadLocalRandom.current().nextInt();
            case 5:
                return ThreadLocalRandom.current().nextLong();
        }
        return -1;

    }

    private static int getCastOpCode(int valueA, int valueB) {
        switch (valueA) {
            case 0:
                //int
                switch (valueB) {
                    case 0:
                    case 1:
                    case 2:
                        break;
                    case 3:
                        return Opcodes.I2F;
                    case 4:
                        return Opcodes.I2D;
                    case 5:
                        return Opcodes.I2L;
                }
                break;
            case 1:
                //short
                switch (valueB) {
                    case 0:
                    case 1:
                    case 2:
                        break;
                    case 3:
                        return Opcodes.I2F;
                    case 4:
                        return Opcodes.I2D;
                    case 5:
                        return Opcodes.I2L;
                }
                break;
            case 2:
                //byte
                switch (valueB) {
                    case 0:
                    case 1:
                    case 2:
                        break;
                    case 3:
                        return Opcodes.I2F;
                    case 4:
                        return Opcodes.I2D;
                    case 5:
                        return Opcodes.I2L;
                }
                break;
            case 3:
                //float
                switch (valueB) {
                    case 0:
                    case 1:
                    case 2:
                        return Opcodes.F2I;
                    case 3:
                        break;
                    case 4:
                        return Opcodes.F2D;
                    case 5:
                        return Opcodes.F2L;
                }
                break;
            case 4:
                //double
                switch (valueB) {
                    case 0:
                    case 1:
                    case 2:
                        return Opcodes.D2I;
                    case 3:
                        return Opcodes.D2F;
                    case 4:
                        break;
                    case 5:
                        return Opcodes.D2L;
                }
                break;
            case 5:
                //long
                switch (valueB) {
                    case 0:
                    case 1:
                    case 2:
                        return Opcodes.L2I;
                    case 3:
                        return Opcodes.L2F;
                    case 4:
                        return Opcodes.L2D;
                    case 5:
                        break;
                }
                break;
        }
        return -1;
    }

    private static InsnList getCompareOpCode(int valueType, int operator, LabelNode endNode) {

        InsnList compare = new InsnList();

        int innerScramble = ThreadLocalRandom.current().nextInt(3);

        switch (valueType) {
            case 0:
                //int
            case 1:
                //short
            case 2:
                //byte
                switch (operator) {
                    case -1:

                        //IF_ICMPEQ;

                        //IF_ICMPGT;

                        //compare.add(new JumpInsnNode(Opcodes.IF_ICMPGE, endNode));
                        compare.add(new JumpInsnNode(innerScramble == 0 ? Opcodes.IF_ICMPEQ : innerScramble == 1 ? Opcodes.IF_ICMPGT : Opcodes.IF_ICMPGE, endNode));
                        break;
                    case 0:
                        //IF_ICMPGT;
                        //IF_ICMPLT
                        //compare.add(new JumpInsnNode(Opcodes.IF_ICMPNE, endNode));
                        compare.add(new JumpInsnNode(innerScramble == 0 ? Opcodes.IF_ICMPGT : innerScramble == 1 ? Opcodes.IF_ICMPLT : Opcodes.IF_ICMPNE, endNode));
                        break;
                    case 1:
                        //IF_ICMPEQ;

                        //IF_ICMPLT
                        //compare.add(new JumpInsnNode(Opcodes.IF_ICMPLE, endNode));
                        compare.add(new JumpInsnNode(innerScramble == 0 ? Opcodes.IF_ICMPEQ : innerScramble == 1 ? Opcodes.IF_ICMPLT : Opcodes.IF_ICMPLE, endNode));
                        break;
                }
                return compare;
            case 3:
                //float
                switch (operator) {
                    case -1:
                        compare.add(new InsnNode(Opcodes.FCMPG));
                        break;
                    case 0:
                        compare.add(new InsnNode(Opcodes.FCMPL));
                        break;
                    case 1:
                        compare.add(new InsnNode(Opcodes.FCMPL));
                        break;
                }
                break;
            case 4:
                //double
                switch (operator) {
                    case -1:
                        compare.add(new InsnNode(Opcodes.DCMPG));
                        break;
                    case 0:
                        compare.add(new InsnNode(Opcodes.DCMPL));
                        break;
                    case 1:
                        compare.add(new InsnNode(Opcodes.DCMPL));
                        break;
                }
                break;
            case 5:
                compare.add(new InsnNode(Opcodes.LCMP));
                //long
                break;
        }

        switch (operator) {
            case -1:


                //IFGT

                //IFEQ

                //compare.add(new JumpInsnNode(Opcodes.IFGE, endNode));
                compare.add(new JumpInsnNode(innerScramble == 0 ? Opcodes.IFGT : innerScramble == 1 ? Opcodes.IFEQ : Opcodes.IFGE, endNode));
                break;
            case 0:
                //IFGT
                //IFLT
                //compare.add(new JumpInsnNode(Opcodes.IFNE, endNode));
                compare.add(new JumpInsnNode(innerScramble == 0 ? Opcodes.IFGT : innerScramble == 1 ? Opcodes.IFLT : Opcodes.IFNE, endNode));
                break;
            case 1:
                //IFLT

                //IFEQ
                //compare.add(new JumpInsnNode(Opcodes.IFLE, endNode));
                compare.add(new JumpInsnNode(innerScramble == 0 ? Opcodes.IFLT : innerScramble == 1 ? Opcodes.IFEQ : Opcodes.IFLE, endNode));
                break;
        }

        return compare;
    }
}
