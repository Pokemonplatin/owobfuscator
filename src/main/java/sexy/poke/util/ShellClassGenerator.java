package sexy.poke.util;

import org.objectweb.asm.*;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.mappings.SmallClassNode;

import java.util.HashMap;

/**
 * Created by Flo on 07.10.2018
 */
public class ShellClassGenerator {

    public static ClassLoader loader;

    private static HashMap<String, SmallClassNode> mapping;

    public static void init(HashMap<String, SmallClassNode> mapping) {
        ShellClassGenerator.mapping = mapping;
        loader = new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                SmallClassNode scn = getNodeFromObfName(name);

                if (scn == null) {
                    return super.loadClass(name);
                }

                /*String newName = scn.getNewName();
                if (newName == null)
                    newName = scn.getClassName();

                String newSuperName = scn.getSuperClass().getNewName();
                if (newSuperName == null)
                    newSuperName = scn.getSuperClass().getClassName();*/

                String newName;
                if(scn.hasNewName()) {
                    newName = scn.getNewName();
                } else {
                    newName = scn.getClassName();
                }

                String newSuperName;
                if(scn.getSuperClass().hasNewName()) {
                    newSuperName = scn.getSuperClass().getNewName();
                } else {
                    newSuperName = scn.getSuperClass().getClassName();
                }

                byte[] bytes = genClass(newName, newSuperName);

                return defineClass(name, bytes, 0, bytes.length);
            }
        };
    }

    private static SmallClassNode getNodeFromObfName(String obf) {
        for (SmallClassNode scn : mapping.values()) {
            if (scn.getNewName() != null && scn.getNewName().equals(obf))
                return scn;
        }
        return null;
    }

    private static byte[] genClass(String name, String owner) {

        ClassNode node = new ClassNode();

        if (owner == null)
            owner = Object.class.getName().replace(".", "/");

        node.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, name, null, owner, null);

        return JarUtils.getNodeBytesUnsafe(node);
    }
}
