package sexy.poke.util;

import sexy.poke.Main;
import sexy.poke.Settings;

import java.util.*;

/**
 * Created by Flo on 07.08.2018
 */
public class RandomProvider {

    static {
        new RandomProvider();
    }

    protected RandomProvider() {
        pool_iii = new ArrayList<>();
        pool_iii.addAll(Arrays.asList("i", "I", "l", "|"));

        pool_numbers = new ArrayList<>();
        pool_numbers.addAll(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));

        pool_spaces = new ArrayList<>();
        pool_spaces.addAll(Arrays.asList(" ", " ", " ", " ", " ", " ", " "/*, "\uDB40\uDC20"*/, " ", /*"\u200B",*/ " "));

        pool_blocks = new ArrayList<>();
        pool_blocks.addAll(Arrays.asList("▀", "▁", "▂", "▃", "▄", "▅", "▆", "▇", "█", "▉",
                "▊", "▋", "▌", "▍", "▎", "▏", "▐", "░", "▒", "▒", "▔", "▕"));

        pool_abc = new ArrayList<>();
        pool_abc.addAll(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"));

        pool_abcABC = new ArrayList<>();
        pool_ABC = new ArrayList<>();

        pool_abcABC.addAll(pool_abc);
        for (String str : pool_abc) {
            pool_ABC.add(str.toUpperCase());
            pool_abcABC.add(str.toUpperCase());
        }

        pool_alphaNumeric = new ArrayList<>();
        pool_alphaNumeric.addAll(pool_numbers);
        pool_alphaNumeric.addAll(pool_abcABC);

        currentPool = pool_abcABC;
    }


    protected static List<String> pool_iii;
    protected static List<String> pool_numbers;
    protected static List<String> pool_spaces;
    protected static List<String> pool_blocks;
    protected static List<String> pool_abc;
    protected static List<String> pool_ABC;
    protected static List<String> pool_abcABC;
    protected static List<String> pool_alphaNumeric;

    protected static List<String> currentPool;

    protected static List<String> customPool;

    private static Map<String, String> used = new HashMap<>();

    public static void setupCustomPool() {
        customPool = new ArrayList<>();
        String[] pool = Main.settings.customStringPool.split("\\,");
        customPool.addAll(Arrays.asList(pool));
        used = new HashMap<>();
    }

    public String generateString(Random rng, int length, boolean unique, Settings.ObfuscationDictionary dic) {
        return generateString(rng, length, unique, dic, null);
    }

    public String generateString(Random rng, int length, boolean unique, Settings.ObfuscationDictionary dic, String original) {
        List<String> current = currentPool;

        switch (dic) {
            case ABC_SMALL:
                current = pool_abc;
                break;
            case ABC_BIG:
                current = pool_ABC;
                break;
            case ABCABC:
                current = pool_abcABC;
                break;
            case III:
                current = pool_iii;
                break;
            case ALPHANUMERIC:
                current = pool_alphaNumeric;
                break;
            case BLOCKS:
                current = pool_blocks;
                break;
            case CUSTOM:
                current = customPool;
                break;
            case SPACES:
                current = pool_spaces;
                break;
            case NUMERIC:
                current = pool_numbers;
                break;
            case OFF:
                break;
        }

        return generateString(rng, current, length, "", "", unique, original);
    }

    public String generateString(Random rng, int length, boolean unique) {
        return generateString(rng, currentPool, length, "", "", unique, null);
    }

    public String generateString(Random rng, List<String> pool, int length, boolean unique) {
        return generateString(rng, pool, length, "", "", unique, null);
    }

    /**
     * Generates a String with custom prefix, suffix and selected length, out of the provided Strings
     *
     * @param rng    Instance of a Randomizer
     * @param names  List of available strings
     * @param length Length of the String with prefix and suffix
     * @param prefix The Prefix
     * @param suffix The Suffix
     * @param unique Should the String be unique
     * @return Random String
     */
    public String generateString(Random rng, List<String> names, int length, String prefix, String suffix, boolean unique, String original) {
        if (original != null && used.get(original) != null)
            return used.get(original);

        StringBuilder out = new StringBuilder(prefix);
        for (int i = 0; i < rng.nextInt(length) + 1; i++) {
            out.append(names.get(rng.nextInt(names.size())));
        }
        out.append(suffix);

        String output = out.toString();

        if (used.containsValue(output) && unique)
            return generateString(rng, names, length, prefix, suffix, unique, original);

        if (original == null)
            original = output;

        used.put(original, output);
        return output;
    }

    /**
     * Returns a random positiv byte
     *
     * @param rnd
     * @return
     */
    public static byte getByte(Random rnd) {
        return (byte) (rnd.nextInt(127) + 1);
    }
}
