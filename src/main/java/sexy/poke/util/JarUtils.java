package sexy.poke.util;

import javafx.application.Platform;
import javafx.scene.text.Text;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.mappings.SmallClassNode;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Flo on 03.08.2018
 */
public class JarUtils {
    /**
     * Creates a map of <String(Class name), ClassNode> for a given jar file
     *
     * @param jarFile
     * @author Konloch (Bytecode Viewer)
     * @return
     * @throws IOException
     */
    public static Map<String, ClassNode> loadClasses(File jarFile, Text text) throws IOException {
        Map<String, ClassNode> classes = new HashMap<String, ClassNode>();
        if(!jarFile.exists()) {
            System.err.println(jarFile.getAbsolutePath() + " not present");
        }
        JarFile jar = new JarFile(jarFile);
        Stream<JarEntry> str = jar.stream();
        // For some reason streaming = entries in messy jars
        // enumeration = no entries
        // Or if the jar is really big, enumeration = infinite hang
        // ...
        // Whatever. It works now!
        str.forEach(z -> readJar(jar, z, classes, null, text));
        jar.close();
        return classes;
    }

    /**
     * Creates a map of <String(entry name), byte[]> for a given jar file
     *
     *
     * @param jarFile
     * @return
     * @throws IOException
     */
    public static Map<String, byte[]> loadNonClassEntries(File jarFile) throws IOException {
        Map<String, byte[]> entries = new HashMap<String, byte[]>();
        ZipInputStream jis = new ZipInputStream(new FileInputStream(jarFile));
        ZipEntry entry;
        while ((entry = jis.getNextEntry()) != null) {
            try {
                String name = entry.getName();
                if (!name.endsWith(".class") && !entry.isDirectory()) {
                    byte[] bytes = readBytes(jis);
                    if(!name.contains("."))
                        name += ".\n";
                    entries.put(name, bytes);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                jis.closeEntry();
            }
        }
        jis.close();
        return entries;
    }

    private static Map<String, ClassNode> readJar(JarFile jar, JarEntry en, Map<String, ClassNode> classes, List<String> ignored, Text text) {
        String name = en.getName();
        try (InputStream jis = jar.getInputStream(en)) {
            if (name.endsWith(".class")) {
                if (ignored != null) {
                    for (String s : ignored) {
                        if (name.startsWith(s)) {
                            return classes;
                        }
                    }
                }
                byte[] bytes = readBytes(jis);
                String cafebabe = String.format("%02X%02X%02X%02X", bytes[0], bytes[1], bytes[2], bytes[3]);
                if (cafebabe.toLowerCase().equals("cafebabe")) {

                    if(text != null) {
                        Platform.runLater(() -> {
                            text.setText("Load class " + name);
                        });
                    }

                    try {
                        final ClassNode cn = getNode(bytes);
                        if (cn != null && (cn.name.equals("java/lang/Object") ? true : cn.superName != null)) {
                            /*for (MethodNode mn : cn.methods) {
                                mn.owner = cn.name;
                            }*/
                            classes.put(cn.name, cn);
                            //cn.preLoad = bytes;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return classes;
    }

    public static ClassNode getNode(final byte[] bytez) {
        ClassReader cr = new ClassReader(bytez);
        ClassNode cn = new ClassNode();
        try {
            cr.accept(cn, ClassReader.EXPAND_FRAMES);
        } catch (Exception e) {
            try {
                cr.accept(cn, ClassReader.SKIP_FRAMES | ClassReader.SKIP_DEBUG);
            } catch (Exception e2) {
                // e2.printStackTrace();
            }
        }
        cr = null;
        return cn;
    }

    public static File[] getRT() {
        File base = new File(System.getProperty("java.home") + File.separator + "lib");
        return new File[] { new File(base, "rt.jar"), new File(base, "jfxswt.jar"),
                new File(base, "jce.jar"), new File(base, "/ext/jfxrt.jar"), new File(base, "jfr.jar"), new File(base, "jsse.jar")};
    }

    public static byte[] readBytes(InputStream is) throws IOException
    {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream();)
        {
            byte[] buffer = new byte[0xFFFF];

            for (int len; (len = is.read(buffer)) != -1;)
                os.write(buffer, 0, len);

            os.flush();

            return os.toByteArray();
        }
    }

    public static void saveAsJar(Map<String, byte[]> outBytes, String fileName) {
        try {
            JarOutputStream out = new JarOutputStream(new java.io.FileOutputStream(fileName));
            for (String entry : outBytes.keySet()) {
                String ext = entry.contains(".") ? "" : ".class";
                out.putNextEntry(new ZipEntry(entry.replace(".\n","") + ext));
                out.write(outBytes.get(entry));
                out.closeEntry();
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] getNodeBytes(ClassNode cn, HashMap<String, SmallClassNode> mapping) {
        try {
            ClassWriter cw = new ShellClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS, mapping);
            cn.accept(cw);
            return cw.toByteArray();
        } catch (Throwable e) {
            e.printStackTrace();
            ClassWriter cw = new ShellClassWriter(ClassWriter.COMPUTE_MAXS, mapping);
            cn.accept(cw);
            return cw.toByteArray();
        }
    }

    public static byte[] getNodeBytesUnsafe(ClassNode cn) {
        try {
            ClassWriter cw = new ClassWriter(0);
            cn.accept(cw);
            return cw.toByteArray();
        } catch (Exception e) {
                e.printStackTrace();
        }
        return null;
    }
}
