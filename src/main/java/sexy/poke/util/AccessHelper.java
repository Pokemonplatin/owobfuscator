package sexy.poke.util;

/**
 * Created by Flo on 07.10.2018
 */
public class AccessHelper {
    public static boolean isPublic(int mod) {
        if ((mod & 1) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isProtected(int mod) {
        if ((mod & 4) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isPrivate(int mod) {
        if ((mod & 2) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isStatic(int mod) {
        if ((mod & 8) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isNative(int mod) {
        if ((mod & 256) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isAbstract(int mod) {
        if ((mod & 1024) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isFinal(int mod) {
        if ((mod & 16) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isSynthetic(int mod) {
        if ((mod & 4096) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isVolatile(int mod) {
        if ((mod & 64) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isBridge(int mod) {
        if ((mod & 64) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isSynchronized(int mod) {
        if ((mod & 32) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isInterface(int mod) {
        if ((mod & 512) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isEnum(int mod) {
        if ((mod & 16384) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isAnnotation(int mod) {
        if ((mod & 8192) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isDeprecated(int mod) {
        if ((mod & 131072) != 0) {
            return true;
        }
        return false;
    }

    public static boolean isVoid(String desc) {
        return desc.endsWith("V");
    }

    public static boolean isBoolean(String desc) {
        return desc.endsWith("Z");
    }

    public static boolean isChar(String desc) {
        return desc.endsWith("C");
    }

    public static boolean isByte(String desc) {
        return desc.endsWith("B");
    }

    public static boolean isShort(String desc) {
        return desc.endsWith("S");
    }

    public static boolean isInt(String desc) {
        return desc.endsWith("I");
    }

    public static boolean isFloat(String desc) {
        return desc.endsWith("F");
    }

    public static boolean isLong(String desc) {
        return desc.endsWith("J");
    }

    public static boolean isDouble(String desc) {
        return desc.endsWith("D");
    }

    public static boolean isArray(String desc) {
        return desc.startsWith("[");
    }

    public static boolean isObject(String desc) {
        return desc.endsWith(";");
    }

    public static boolean isMethodReturnTypeGeneric(String desc) {
        return desc.contains((CharSequence)")T");
    }

    public static boolean isFieldGeneric(String desc, String signature) {
        if (signature != null && desc != null && signature.startsWith("T") && signature.endsWith(";") && Character.isUpperCase((char)signature.charAt(1)) && desc.contains((CharSequence)"java/lang/Object")) {
            return true;
        }
        return false;
    }
}
