package sexy.poke.util;

/**
 * Created by Flo on 05.10.2018
 */
public class ExcludeElement {

    public ExcludeElement(String name, String displayName, Type type, int access) {
        this.name = name;
        this.displayName = displayName;
        this.type = type;
        this.access = access;
    }

    public ExcludeElement(String name, String displayName, Type type, int access, ExcludeElement parent) {
        this.name = name;
        this.displayName = displayName;
        this.type = type;
        this.access = access;
        this.parent = parent;
    }

    public ExcludeElement parent;

    public final String name, displayName;
    public final int access;
    public final Type type;

    public String getCompleteName() {
        String parent = this.parent != null ? (this.parent.getCompleteName() + "/") : "";

        return parent +  name;
    }

    @Override
    public String toString() {
        return displayName;
    }

    public enum Type {
        PACKAGE, CLASS, FIELD, METHOD, NONE
    }

}
