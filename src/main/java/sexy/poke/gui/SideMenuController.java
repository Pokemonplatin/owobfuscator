package sexy.poke.gui;

import com.jfoenix.controls.JFXListView;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import io.datafx.controller.util.VetoException;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import org.objectweb.asm.tree.ClassNode;
import sexy.poke.Main;
import sexy.poke.gui.components.*;
import sexy.poke.mappings.SmallClassNode;
import sexy.poke.tasks.Obfuscator;
import sexy.poke.tasks.mapping.Mapper;
import sexy.poke.tasks.obfuscation.impl.string.TaskStringEncryption;
import sexy.poke.util.JarUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Flo on 13.08.2018
 */
@FXMLController(value = "/SideMenu.fxml")
public class SideMenuController {

    @FXMLViewFlowContext
    public ViewFlowContext context;

    public Label files;
    public Label removal;
    //public Label mappings;
    public Label renaming;
    public Label access;
    public Label shuffling;
    public Label stringEncryption;
    public Label flowObfuscation;
    public Label invokeDynamic;
    public Label crasher;
    public Label process;
    public Label exclude;

    @FXML
    private JFXListView<Label> sideList;
    @FXML
    private JFXListView<Label> subList;

    @PostConstruct
    public void init() {
        if (context == null)
            return;

        Objects.requireNonNull(context, "context");
        FlowHandler contentFlowHandler = (FlowHandler) context.getRegisteredObject("ContentFlowHandler");
        sideList.propagateMouseEventsToParent();
        sideList.getSelectionModel().selectedItemProperty().addListener((o, oldVal, newVal) -> {
            new Thread(() -> {
                Platform.runLater(() -> {
                    if (newVal != null && newVal.getId() != null) {
                        try {
                            contentFlowHandler.handle(newVal.getId());
                        } catch (VetoException exc) {
                            exc.printStackTrace();
                        } catch (FlowException exc) {
                            exc.printStackTrace();
                        }
                    }
                });
            }).start();
        });

        subList.getSelectionModel().selectedItemProperty().addListener((o, oldVal, newVal) -> {
            new Thread(() -> {
                Platform.runLater(() -> {
                    if (newVal != null && newVal.getId() != null) {
                        try {
                            contentFlowHandler.handle(newVal.getId());
                        } catch (VetoException exc) {
                            exc.printStackTrace();
                        } catch (FlowException exc) {
                            exc.printStackTrace();
                        }
                    }
                });
            }).start();
        });

        Flow contentFlow = (Flow) context.getRegisteredObject("ContentFlow");

        bindNodeToController(files, SelectFilesController.class, contentFlow, contentFlowHandler);
        //bindNodeToController(mappings, MappingsController.class, contentFlow, contentFlowHandler);
        bindNodeToController(removal, RemovalController.class, contentFlow, contentFlowHandler);
        bindNodeToController(renaming, RenamingController.class, contentFlow, contentFlowHandler);
        bindNodeToController(shuffling, ShuffleController.class, contentFlow, contentFlowHandler);
        bindNodeToController(stringEncryption, StringEncryptionController.class, contentFlow, contentFlowHandler);
        bindNodeToController(flowObfuscation, FlowObfuscationController.class, contentFlow, contentFlowHandler);
        bindNodeToController(access, AccessController.class, contentFlow, contentFlowHandler);
        bindNodeToController(invokeDynamic, InvokeDynamicController.class, contentFlow, contentFlowHandler);
        bindNodeToController(crasher, CrasherController.class, contentFlow, contentFlowHandler);
        bindNodeToController(exclude, ExcludeGuiController.class, contentFlow, contentFlowHandler);
        bindNodeToController(process, null, contentFlow, contentFlowHandler);


        process.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.PRIMARY) {
                    Obfuscator.obfuscate();
                }
            }
        });
    }

    private void bindNodeToController(Node node, Class<?> controllerClass, Flow flow, FlowHandler flowHandler) {
        flow.withGlobalLink(node.getId(), controllerClass);
    }
}
