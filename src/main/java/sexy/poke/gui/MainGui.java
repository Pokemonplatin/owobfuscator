package sexy.poke.gui;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXRippler;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.animation.Transition;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import sexy.poke.ExtendedAnimatedFlowContainer;
import sexy.poke.gui.components.SelectFilesController;

import javax.annotation.PostConstruct;

/**
 * Created by Flo on 14.08.2018
 */
@FXMLController(value = "/MainGui.fxml")
public class MainGui {
    @FXMLViewFlowContext
    private ViewFlowContext context;
    @FXML
    private StackPane root;
    @FXML
    private StackPane titleBurgerContainer;
    @FXML
    private JFXHamburger titleBurger;
    @FXML
    private StackPane optionsBurger;
    @FXML
    private JFXRippler optionsRippler;
    @FXML
    private JFXDrawer drawer;
    private JFXPopup toolbarPopup;

    @PostConstruct
    public void init() throws Exception {
        this.drawer.setOnDrawerOpening(e -> {
            final Transition animation = this.titleBurger.getAnimation();
            animation.setRate(1.0);
            animation.play();
        });
        this.drawer.setOnDrawerClosing(e -> {
            final Transition animation = this.titleBurger.getAnimation();
            animation.setRate(-1.0);
            animation.play();
        });
        this.titleBurgerContainer.setOnMouseClicked(e -> {
            if (this.drawer.isClosed() || this.drawer.isClosing()) {
                this.drawer.open();
            }
            else {
                this.drawer.close();
            }
        });
        this.drawer.open();
        this.context = new ViewFlowContext();
        final Flow innerFlow = new Flow(SelectFilesController.class);
        final FlowHandler flowHandler = innerFlow.createHandler(this.context);
        this.context.register("ContentFlowHandler", flowHandler);
        this.context.register("ContentFlow", innerFlow);
        final Duration containerAnimationDuration = Duration.millis(320.0);
        this.drawer.setContent(flowHandler.start(new ExtendedAnimatedFlowContainer(containerAnimationDuration, ContainerAnimations.SWIPE_LEFT)));
        this.context.register("ContentPane", this.drawer.getContent().get(0));
        final Flow sideMenuFlow = new Flow(SideMenuController.class);
        final FlowHandler sideMenuFlowHandler = sideMenuFlow.createHandler(this.context);
        this.drawer.setSidePane(sideMenuFlowHandler.start(new ExtendedAnimatedFlowContainer(containerAnimationDuration, ContainerAnimations.SWIPE_LEFT)));
    }
}
