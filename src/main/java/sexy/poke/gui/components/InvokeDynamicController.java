package sexy.poke.gui.components;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXSlider;
import io.datafx.controller.FXMLController;
import javafx.scene.control.Label;
import sexy.poke.Main;
import sexy.poke.Settings;

import javax.annotation.PostConstruct;

import static sexy.poke.Settings.StringEncryption.OFF;
import static sexy.poke.Settings.StringEncryption.VERYLIGHT;

/**
 * Created by Flo on 20.08.2018
 */
@FXMLController(value = "/InvokeDynamic.fxml")
public class InvokeDynamicController {
    public JFXCheckBox hideMethodCalls;
    public JFXSlider percentageMethods;
    public JFXCheckBox hideFieldCalls;
    public JFXSlider percentageFields;

    @PostConstruct
    public void init() {

        hideMethodCalls.setSelected(Main.settings.invokeDynamic_Methods);
        hideFieldCalls.setSelected(Main.settings.invokeDynamic_Fields);

        percentageMethods.setValue(Main.settings.invokeDynamic_Methods_Percentage);
        percentageFields.setValue(Main.settings.invokeDynamic_Fields_Percentage);

        hideMethodCalls.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.invokeDynamic_Methods = newValue);
        hideFieldCalls.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.invokeDynamic_Fields = newValue);

        percentageMethods.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.invokeDynamic_Methods_Percentage = newValue.intValue());
        percentageFields.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.invokeDynamic_Fields_Percentage = newValue.intValue());

    }
}
