package sexy.poke.gui.components;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import io.datafx.controller.FXMLController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import sexy.poke.Main;
import sexy.poke.Settings;

import javax.annotation.PostConstruct;

import static sexy.poke.Settings.ObfuscationDictionary.*;

/**
 * Created by Flo on 14.08.2018
 */
@FXMLController(value = "/Renaming.fxml")
public class RenamingController {

    @FXML
    private JFXComboBox<Label> comboboxClasses;
    @FXML
    private JFXComboBox<Label> comboboxFields;
    @FXML
    private JFXComboBox<Label> comboboxMethods;
    @FXML
    private JFXCheckBox checkboxClasses;
    @FXML
    private JFXCheckBox checkboxMethods;
    @FXML
    private JFXCheckBox checkboxFields;
    @FXML
    private JFXCheckBox checkboxRepackage;
    @FXML
    private JFXCheckBox checkboxFixReflection;

    @FXML
    private JFXTextField customStringPool;

    @PostConstruct
    public void init() {

        for(Settings.ObfuscationDictionary od : Settings.ObfuscationDictionary.values()) {
            if(od != Settings.ObfuscationDictionary.OFF) {
                comboboxClasses.getItems().add(new Label(od.getCustomName()));
                comboboxFields.getItems().add(new Label(od.getCustomName()));
                comboboxMethods.getItems().add(new Label(od.getCustomName()));
            }
        }

        checkboxClasses.setSelected(Main.settings.dictionaryClasses != Settings.ObfuscationDictionary.OFF);
        checkboxMethods.setSelected(Main.settings.dictionaryMethods != Settings.ObfuscationDictionary.OFF);
        checkboxFields.setSelected(Main.settings.dictionaryFields != Settings.ObfuscationDictionary.OFF);
        checkboxRepackage.setSelected(Main.settings.repackageClasses);

        comboboxClasses.setDisable(Main.settings.dictionaryClasses == OFF);
        comboboxFields.setDisable(Main.settings.dictionaryFields == OFF);
        comboboxMethods.setDisable(Main.settings.dictionaryMethods == OFF);


        checkboxClasses.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.dictionaryClasses =  newValue ? ABC_SMALL : OFF;
            comboboxClasses.setDisable(!newValue);
            comboboxClasses.getSelectionModel().select(newValue ? 0 : -1);
        });

        checkboxFields.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.dictionaryFields =  newValue ? ABC_SMALL  : OFF;
            comboboxFields.setDisable(!newValue);
            comboboxFields.getSelectionModel().select(newValue ? 0 : -1);
        });

        checkboxMethods.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.dictionaryMethods =  newValue ? ABC_SMALL  : OFF;
            comboboxMethods.setDisable(!newValue);
            comboboxMethods.getSelectionModel().select(newValue ? 0 : -1);
        });

        checkboxRepackage.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.repackageClasses =  newValue;
        });

        checkboxFixReflection.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.fixReflection =  newValue;
        });

        comboboxClasses.getSelectionModel().select(Main.settings.dictionaryClasses.getId());
        comboboxFields.getSelectionModel().select(Main.settings.dictionaryFields.getId());
        comboboxMethods.getSelectionModel().select(Main.settings.dictionaryMethods.getId());

        comboboxClasses.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.dictionaryClasses =  Settings.ObfuscationDictionary.values()[comboboxClasses.getSelectionModel().getSelectedIndex() + 1]);
        comboboxFields.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.dictionaryFields =  Settings.ObfuscationDictionary.values()[comboboxFields.getSelectionModel().getSelectedIndex() + 1]);
        comboboxMethods.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.dictionaryMethods =  Settings.ObfuscationDictionary.values()[comboboxMethods.getSelectionModel().getSelectedIndex() + 1]);


        customStringPool.textProperty().addListener((observable, oldValue, newValue) -> Main.settings.customStringPool = newValue);
        customStringPool.setText(Main.settings.customStringPool);
    }
}
