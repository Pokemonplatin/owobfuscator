package sexy.poke.gui.components;

import com.jfoenix.controls.JFXTreeView;
import io.datafx.controller.FXMLController;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;
import sexy.poke.Main;
import sexy.poke.util.AccessHelper;
import sexy.poke.util.ExcludeElement;
import sexy.poke.util.ImageUtil;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Predicate;

/**
 * Created by Flo on 03.10.2018
 */
@FXMLController(value = "/Exclude.fxml")
public class ExcludeGuiController {

    @FXML
    HBox view;


    //Java is buggy as hell so no inlining
    private static Comparator<Map.Entry<String, TreeItem<ExcludeElement>>> mapComp1 = Comparator.comparing(v -> v.getValue().getValue().type);
    private static Comparator<Map.Entry<String, TreeItem<ExcludeElement>>> mapComp2 = mapComp1.thenComparing(v -> v.getValue().getValue().displayName.toLowerCase());

    private static Comparator<TreeItem<ExcludeElement>> treeComp1 = Comparator.comparing(v -> v.getValue().type);
    private static Comparator<TreeItem<ExcludeElement>> treeComp2 = treeComp1.thenComparing(v -> v.getValue().displayName.toLowerCase());

    private static Comparator<ExcludeElement> listComp1 = Comparator.comparing(v -> v.type);
    private static Comparator<ExcludeElement> listComp2 = listComp1.thenComparing(v -> v.displayName.toLowerCase());



    @PostConstruct
    public void init() throws Exception {


        LinkedHashMap<String, TreeItem<ExcludeElement>> map = new LinkedHashMap<>();
        LinkedHashMap<String, TreeItem<ExcludeElement>> packages = new LinkedHashMap<>();

        FilterableTreeItem<ExcludeElement> rootNode = new FilterableTreeItem<>(new ExcludeElement("", "Select Files", ExcludeElement.Type.NONE,0));//, rootIcon);
        FilterableTreeItem<ExcludeElement> secondRootNode = new FilterableTreeItem<>(new ExcludeElement("", "Excluded Elements", ExcludeElement.Type.NONE,0));//, rootIcon);

        rootNode.setExpanded(true);
        secondRootNode.setExpanded(true);

        /*jf.stream().forEach(zi -> {
            if (!zi.getName().endsWith(".class"))
                return;

            String str = zi.getName().replace(".class", "").replace("/", ".");

            if (!str.contains(".")) {
                str = "default-package." + str;
            }

            int i = str.lastIndexOf(".");

            String sub = str.substring(0, i);
            TreeItem<ExcludeElement> ti = map.get(sub);
            if (ti == null) {
                ti = new TreeItem<>(new ExcludeElement(sub, sub, ExcludeElement.Type.PACKAGE), getImage(ExcludeElement.Type.PACKAGE));
                map.put(sub, ti);
            }

            ti.getChildren().add(new TreeItem<>(new ExcludeElement(str, str.substring(i + 1, str.length()), ExcludeElement.Type.CLASS), getImage(ExcludeElement.Type.CLASS)));
        });*/

        for (ClassNode cn : Main.settings.cleanNodes) {

            /*
            int i = cn.name.lastIndexOf("/");

            String str = cn.name;

            TreeItem<ExcludeElement> clazz = new TreeItem<>(new ExcludeElement(str, str.substring(i + 1, str.length()), ExcludeElement.Type.CLASS, cn.access),
                    getImage(ExcludeElement.Type.CLASS, cn.access));

            if (str.contains("/")) {
                String sub = str.substring(0, i);

                // dssada/dsadas/dsada    /dsada.class


                TreeItem<ExcludeElement> ti = null;

                System.out.println(str);

                int i2 = sub.indexOf("/");
                while(i2 != -1) {

                    String next = sub.substring(i2 +1);
                    String current = sub.substring(0,i2);

                    System.out.println("current - " + current + " - " + str);

                    TreeItem<ExcludeElement> nti = map.get(current);


                    if(ti != null) {
                        for (TreeItem<ExcludeElement> child : ti.getChildren()) {
                            if(child.getValue().name.equals(current)) {
                                nti = child;
                                break;
                            }
                        }
                    }

                    if (nti == null) {
                        nti = new TreeItem<>(new ExcludeElement(current, current, ExcludeElement.Type.PACKAGE, 0), getImage(ExcludeElement.Type.PACKAGE, 0));

                        if(ti != null) {
                            nti.getValue().parent = ti.getValue();
                            ti.getChildren().add(nti);

                            System.out.println("Add " + current);

                        } else {
                            map.put(current, nti);
                        }
                    }
                    ti = nti;
                    sub = next;
                    i2 = sub.indexOf("/");
                }


                /*TreeItem<ExcludeElement> ti = map.get(sub);
                if (ti == null) {
                    ti = new TreeItem<>(new ExcludeElement(sub, sub, ExcludeElement.Type.PACKAGE, 0), getImage(ExcludeElement.Type.PACKAGE, 0));
                    map.put(sub, ti);
                }*

                clazz.getValue().parent = ti.getValue();

                ti.getChildren().add(clazz);
            } else {
                map.put(str, clazz);
            }*/


            String[] wrapped = cn.name.split("/");

            String str = cn.name;

            TreeItem<ExcludeElement> clazz = new TreeItem<>(new ExcludeElement(str, str.substring(cn.name.lastIndexOf("/") + 1), ExcludeElement.Type.CLASS, cn.access),
                    getImage(ExcludeElement.Type.CLASS, cn.access));

            if(wrapped.length != 1) {
                StringBuilder pkg = new StringBuilder();

                for (int i = 0; wrapped.length - 1 > i; i++) {
                    pkg.append(wrapped[i]).append("/");
                }


                TreeItem<ExcludeElement> current = packages.get(pkg.toString());

                if (current == null) {
                    for (int i = 0; i < wrapped.length - 1; i++) {

                        String combind = "";
                        for (int i2 = 0; i2 < i; i2++) {
                            combind += wrapped[i2] + "/";
                        }

                        String parentPackage = combind;

                        combind += wrapped[i] + "/";

                        current = packages.get(combind);
                        if (current == null) {
                            /*String combind2 = "";
                            for (int i2 = 0; i2 < i; i2++) {
                                combind2 += wrapped[i2] + "/";
                            }*/
                            //combind2 += wrapped[i] + "/";

                            //String currentPackName = combind.substring(0,combind.length() -1);

                            current = new TreeItem<>(new ExcludeElement(wrapped[i], wrapped[i], ExcludeElement.Type.PACKAGE, 0), getImage(ExcludeElement.Type.PACKAGE, 0));

                            TreeItem<ExcludeElement> tmpParent = packages.get(parentPackage);

                            if (tmpParent != null) {
                                current.getValue().parent = tmpParent.getValue();
                                tmpParent.getChildren().add(current);
                            } else {
                                map.put(combind, current);
                            }
                            packages.put(combind, current);
                        }
                    }
                }

                clazz.getValue().parent = current.getValue();

                current.getChildren().add(clazz);

            } else {
                map.put(cn.name, clazz);
            }

            for (FieldNode fn : cn.fields) {
                clazz.getChildren().add(new TreeItem<>(new ExcludeElement(cn.name + fn.name, fn.name, ExcludeElement.Type.FIELD, fn.access, clazz.getValue()), getImage(ExcludeElement.Type.FIELD, fn.access)));
            }

            for (MethodNode mn : cn.methods) {
                clazz.getChildren().add(new TreeItem<>(new ExcludeElement(cn.name + mn.name + mn.desc, mn.name, ExcludeElement.Type.METHOD, mn.access, clazz.getValue()), getImage(ExcludeElement.Type.METHOD, mn.access)));
            }
        }

        map = sortByComparator(map);

        rootNode.getInternalChildren().addAll(map.values());

        JFXTreeView<ExcludeElement> view = new JFXTreeView<>(rootNode);
        JFXTreeView<ExcludeElement> view2 = new JFXTreeView<>(secondRootNode);

        view.setShowRoot(false);
        view2.setShowRoot(false);


        MenuItem selectWildcard = new MenuItem("Select Wildcard");
        selectWildcard.setOnAction(event -> {
            TreeItem<ExcludeElement> item = view.getSelectionModel().getSelectedItem();

            if (item == null || item.getValue() == null || item.getValue().type == ExcludeElement.Type.NONE)
                return;

            addRecursive(item);

            generateSelectedView(secondRootNode);
        });

        view.setContextMenu(new ContextMenu(selectWildcard));

        MenuItem removeNode = new MenuItem("Remove Node");
        removeNode.setOnAction(event -> {
            TreeItem<ExcludeElement> item = view2.getSelectionModel().getSelectedItem();

            if (item == null || item.getValue() == null || item.getValue().type == ExcludeElement.Type.NONE)
                return;

            removeRecursive(item);

            generateSelectedView(secondRootNode);
        });

        view2.setContextMenu(new ContextMenu(removeNode));


        view.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                TreeItem<ExcludeElement> item = view.getSelectionModel().getSelectedItem();

                if (item == null || item.getValue() == null || item.getValue().type == ExcludeElement.Type.NONE)
                    return;

                Main.settings.excludeList.add(item.getValue());

                generateSelectedView(secondRootNode);
            }
        });

        view2.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                TreeItem<ExcludeElement> item = view2.getSelectionModel().getSelectedItem();

                if (item.getValue().type == ExcludeElement.Type.NONE)
                    return;

                Main.settings.excludeList.remove(item.getValue());

                generateSelectedView(secondRootNode);
            }
        });


        this.view.getChildren().addAll(view);
        this.view.getChildren().addAll(view2);

        /*TreeItem<String> root = new TreeItem<String>("java.", new ImageView(
                new Image(getClass().getResourceAsStream("/img/package.png"))
        ));

        root.setExpanded(true);

        for(int i = 0; i < 6; i++) {
            TreeItem<String> childOne = new TreeItem<String>("hhh", new ImageView(
                    new Image(getClass().getResourceAsStream("/img/package.png"))
            ));
            for(int i2 = 0; i2 < 6; i2++) {
                TreeItem<String> childOne1 = new TreeItem<String>("ddd",new ImageView(
                        new Image(getClass().getResourceAsStream("/img/package.png"))
                ));
                childOne.getChildren().add(childOne1);
            }
            root.getChildren().add(childOne);
        }

        JFXTreeView<String> view = new JFXTreeView<>(root);

        JFXTreeView<String> view2 = new JFXTreeView<>();*/

        //this.view.getChildren().add(view);
        //this.view.getChildren().add(view2);
    }

    private void addRecursive(TreeItem<ExcludeElement> item) {
        item.getChildren().forEach(c -> {
            Main.settings.excludeList.add(c.getValue());
            addRecursive(c);
        });
        Main.settings.excludeList.add(item.getValue());
    }

    private void removeRecursive(TreeItem<ExcludeElement> item) {
        item.getChildren().forEach(c -> {
            Main.settings.excludeList.remove(c.getValue());
            removeRecursive(c);
        });
        Main.settings.excludeList.remove(item.getValue());
    }

    private void generateSelectedView(FilterableTreeItem<ExcludeElement> rootNode) {
        rootNode.getInternalChildren().clear();

        ArrayList<ExcludeElement> copy = new ArrayList<>((HashSet<ExcludeElement>) Main.settings.excludeList.clone());

        copy.sort(listComp2);

        HashMap<ExcludeElement, TreeItem<ExcludeElement>> items = new HashMap<>();
        ArrayList<TreeItem<ExcludeElement>> linkedList = new ArrayList<>();

        for (ExcludeElement excludeElement : copy) {
            items.put(excludeElement, new TreeItem<>(excludeElement, getImage(excludeElement.type, excludeElement.access)));
            //rootNode.getInternalChildren().add(new TreeItem<>(excludeElement, getImage(excludeElement.type, excludeElement.access)));
        }

        items.forEach((k,v) -> {
            if(k.parent != null && items.get(k.parent) != null) {
                items.get(k.parent).getChildren().add(v);
            } else {
                linkedList.add(v);
            }
        });

        rootNode.getInternalChildren().addAll(linkedList);
    }

    private static LinkedHashMap<String, TreeItem<ExcludeElement>> sortByComparator(LinkedHashMap<String, TreeItem<ExcludeElement>> unsortMap) {
        List<Map.Entry<String, TreeItem<ExcludeElement>>> list = new LinkedList<>(unsortMap.entrySet());

        list.sort(mapComp2);

        // Maintaining insertion order with the help of LinkedList
        LinkedHashMap<String, TreeItem<ExcludeElement>> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, TreeItem<ExcludeElement>> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        //sortedMap.forEach((k, v) -> v.getChildren().sort(treeComp2));
        sortedMap.forEach((k, v) -> sortChildren(v.getChildren()));

        return sortedMap;
    }

    private static void sortChildren(ObservableList<TreeItem<ExcludeElement>> list) {
        list.sort(treeComp2);
        list.forEach(item -> {
            sortChildren(item.getChildren());
        });
    }

    public ImageView getImage(ExcludeElement.Type type, int access) {

        switch (type) {
            case PACKAGE:
                return new ImageView(ImageUtil.pack);
            case CLASS:
                return new ImageView(AccessHelper.isInterface(access) ? ImageUtil.clazzInterface : AccessHelper.isEnum(access) ? ImageUtil.clazzEnum : ImageUtil.clazz);
            case FIELD:
                return new ImageView(ImageUtil.generateIcon(access, false));
            case METHOD:
                return new ImageView(ImageUtil.generateIcon(access, true));
        }
        return null;
    }

    public interface TreeItemPredicate<T> {

        boolean test(TreeItem<T> parent, T value);

        static <T> TreeItemPredicate<T> create(Predicate<T> predicate) {
            return (parent, value) -> predicate.test(value);
        }

    }

    public class FilterableTreeItem<T> extends TreeItem<T> {
        final private ObservableList<TreeItem<T>> sourceList;
        private FilteredList<TreeItem<T>> filteredList;
        private ObjectProperty<TreeItemPredicate<T>> predicate = new SimpleObjectProperty<>();


        public FilterableTreeItem(T value) {
            super(value);
            this.sourceList = FXCollections.observableArrayList();
            this.filteredList = new FilteredList<>(this.sourceList);
            this.filteredList.predicateProperty().bind(Bindings.createObjectBinding(() -> child -> {
                // Set the predicate of child items to force filtering
                if (child instanceof FilterableTreeItem) {
                    FilterableTreeItem<T> filterableChild = (FilterableTreeItem<T>) child;
                    filterableChild.setPredicate(this.predicate.get());
                }
                // If there is no predicate, keep this tree item
                if (this.predicate.get() == null)
                    return true;
                // If there are children, keep this tree item
                if (child.getChildren().size() > 0)
                    return true;
                // Otherwise ask the TreeItemPredicate
                return this.predicate.get().test(this, child.getValue());
            }, this.predicate));
            setHiddenFieldChildren(this.filteredList);
        }

        protected void setHiddenFieldChildren(ObservableList<TreeItem<T>> list) {
            try {
                Field childrenField = TreeItem.class.getDeclaredField("children"); //$NON-NLS-1$
                childrenField.setAccessible(true);
                childrenField.set(this, list);

                Field declaredField = TreeItem.class.getDeclaredField("childrenListener"); //$NON-NLS-1$
                declaredField.setAccessible(true);
                list.addListener((ListChangeListener<? super TreeItem<T>>) declaredField.get(this));
            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
                throw new RuntimeException("Could not set TreeItem.children", e); //$NON-NLS-1$
            }
        }

        public ObservableList<TreeItem<T>> getInternalChildren() {
            return this.sourceList;
        }

        public void setPredicate(TreeItemPredicate<T> predicate) {
            this.predicate.set(predicate);
        }

        public TreeItemPredicate getPredicate() {
            return predicate.get();
        }

        public ObjectProperty<TreeItemPredicate<T>> predicateProperty() {
            return predicate;
        }
    }
}
