package sexy.poke.gui.components;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXToggleButton;
import io.datafx.controller.FXMLController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import sexy.poke.Main;

import javax.annotation.PostConstruct;

/**
 * Created by Flo on 14.08.2018
 */
@FXMLController(value = "/FlowObfuscation.fxml")
public class FlowObfuscationController {

    public JFXCheckBox extraAggressive;
    public JFXToggleButton flowObfu;
    public JFXToggleButton switchCase;

    @PostConstruct
    public void init() throws Exception {
        flowObfu.setSelected(Main.settings.flowObfuscation);

        switchCase.setSelected(Main.settings.flowSwitchCase);

        extraAggressive.setDisable(!Main.settings.flowObfuscation);

        flowObfu.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.flowObfuscation = newValue;
            extraAggressive.setDisable(!Main.settings.flowObfuscation);
        });

        switchCase.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.flowSwitchCase = newValue);

        extraAggressive.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.flowObfuscation_ExtraAggressive = newValue;
        });
    }

}
