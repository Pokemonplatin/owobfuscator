package sexy.poke.gui.components;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXToggleButton;
import io.datafx.controller.FXMLController;
import javafx.scene.control.Label;
import sexy.poke.Main;
import sexy.poke.Settings;

import javax.annotation.PostConstruct;

import static sexy.poke.Settings.StringEncryption.*;

/**
 * Created by Flo on 14.08.2018
 */
@FXMLController(value = "/StringEncryption.fxml")
public class StringEncryptionController {

    public JFXToggleButton encrypt;
    public JFXComboBox encryptionModeCB;
    public JFXCheckBox stringPool;
    public JFXSlider amountDecrypters;

    @PostConstruct
    public void init() {

        for(Settings.StringEncryption od : Settings.StringEncryption.values()) {
            if(od != Settings.StringEncryption.OFF) {
                encryptionModeCB.getItems().add(new Label(od.getCustomName()));
            }
        }

        encrypt.setSelected(Main.settings.stringEncryption != OFF);
        stringPool.setSelected(Main.settings.stringPool);

        amountDecrypters.setValue(Main.settings.stringEncryptionDecrypters);

        encryptionModeCB.setDisable(Main.settings.stringEncryption == OFF);
        amountDecrypters.setDisable(Main.settings.stringEncryption == OFF);

        encrypt.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.stringEncryption =  newValue ? VERYLIGHT  : OFF;
            encryptionModeCB.setDisable(!newValue);
            amountDecrypters.setDisable(!newValue);
            encryptionModeCB.getSelectionModel().select(newValue ? 0 : -1);
        });

        stringPool.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.stringPool = newValue);

        encryptionModeCB.getSelectionModel().select(Main.settings.stringEncryption.getId());

        encryptionModeCB.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.stringEncryption =  Settings.StringEncryption.values()[encryptionModeCB.getSelectionModel().getSelectedIndex() + 1]);

        amountDecrypters.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.stringEncryptionDecrypters = newValue.intValue());

    }
}
