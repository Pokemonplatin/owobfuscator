package sexy.poke.gui.components;

import com.jfoenix.controls.*;
import io.datafx.controller.FXMLController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import sexy.poke.Main;
import sexy.poke.tasks.Obfuscator;
import sexy.poke.util.JarUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * Created by Flo on 13.08.2018
 */
@FXMLController(value = "/SelectFiles.fxml")
public class SelectFilesController {

    @FXML
    private JFXButton addLibs;

    @FXML
    private JFXButton addLibsFolder;

    @FXML
    private JFXButton addInputJar;

    @FXML
    private JFXButton addOutputJar;

    @FXML
    private JFXTextField inputFilePath;

    @FXML
    private JFXTextField outputFilePath;

    @FXML
    private JFXListView<Label> libFiles;


    @PostConstruct
    public void init() {
        final ObservableList<Label> dummyData = libFiles.getItems();

        for(File file : Main.settings.libs) {
            dummyData.add(new Label(file.getAbsolutePath()));
        }

        if(Main.settings.inputFile != null)
            inputFilePath.setText(Main.settings.inputFile.getAbsolutePath());
        if(Main.settings.outputFile != null)
            outputFilePath.setText(Main.settings.outputFile.getAbsolutePath());

        addLibsFolder.setOnMouseClicked((e) -> {
            Platform.runLater(() -> {
                final DirectoryChooser directoryChooser = new DirectoryChooser();
                File folder = directoryChooser.showDialog(null);

                if(folder != null) {
                    try {

                        Files.find(folder.toPath(),
                                Integer.MAX_VALUE,
                                (filePath, fileAttr) -> fileAttr.isRegularFile())
                                .forEach(f -> {
                                    libFiles.getItems().add(new Label(f.toFile().getAbsolutePath()));
                                    Main.settings.libs.add(f.toFile());

                                });
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });
        });

        addLibs.setOnMouseClicked((e) -> {
            Platform.runLater(() -> {
                final FileChooser fileChooserInput = new FileChooser();
                fileChooserInput.getExtensionFilters().add(new FileChooser.ExtensionFilter("Java Application", "*.jar"));
                List<File> f = fileChooserInput.showOpenMultipleDialog(null);
                if (f != null && f.size() != 0) {
                    for(File file : f) {
                        dummyData.add(new Label(file.getAbsolutePath()));
                    }

                    Main.settings.libs.addAll(f);
                }
            });
        });

        addInputJar.setOnMouseClicked((e) -> {
            Platform.runLater(() -> {
                final FileChooser fileChooserInput = new FileChooser();
                fileChooserInput.getExtensionFilters().add(new FileChooser.ExtensionFilter("Java Application", "*.jar"));
                File f = fileChooserInput.showOpenDialog(null);
                if (f != null) {
                    inputFilePath.setText(f.getAbsolutePath());

                    Main.settings.inputFile = f;

                    try {
                        JarUtils.loadClasses(Main.settings.inputFile, Obfuscator.subBody).forEach((name, c) -> {
                            Main.settings.nodes.add(c);
                        });
                        Main.settings.cleanNodes.addAll(Main.settings.nodes);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    if(outputFilePath.getText().isEmpty()) {
                        outputFilePath.setText(f.getAbsolutePath().replace(".jar","-out.jar"));
                        Main.settings.outputFile = new File(f.getAbsolutePath().replace(".jar","-out.jar"));
                    }
                }
            });
        });

        addOutputJar.setOnMouseClicked((e) -> {
            final FileChooser fileChooserInput = new FileChooser();
            fileChooserInput.getExtensionFilters().add(new FileChooser.ExtensionFilter("Java Application", "*.jar"));
            File f = fileChooserInput.showSaveDialog(null);
            if (f != null) {
                outputFilePath.setText(f.getAbsolutePath());
                Main.settings.outputFile = f;
            }
        });

        libFiles.setOnMouseClicked((e) -> {
            if(e.getButton() == MouseButton.SECONDARY) {
                int index = libFiles.getItems().indexOf(libFiles.getSelectionModel().selectedItemProperty().get());
                if(index == -1)
                    return;
                libFiles.getItems().remove(index);
                Main.settings.libs.remove(index);
            }
        });
    }
}
