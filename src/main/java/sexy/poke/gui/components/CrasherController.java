package sexy.poke.gui.components;

import com.jfoenix.controls.JFXCheckBox;
import io.datafx.controller.FXMLController;
import sexy.poke.Main;

import javax.annotation.PostConstruct;

@FXMLController(value = "/CrasherGui.fxml")
public class CrasherController {
    public JFXCheckBox crasherOne;
    public JFXCheckBox crasherTwo;
    public JFXCheckBox crasherThree;
    public JFXCheckBox crasherASM;

    @PostConstruct
    public void init() {
        crasherOne.setSelected(Main.settings.crasherOne);
        crasherTwo.setSelected(Main.settings.crasherTwo);
        crasherThree.setSelected(Main.settings.crasherThree);
        crasherASM.setSelected(Main.settings.crasherASM);

        crasherOne.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.crasherOne = newValue);
        crasherTwo.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.crasherTwo = newValue);
        crasherThree.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.crasherThree = newValue);
        crasherASM.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.crasherASM = newValue);
    }
}
