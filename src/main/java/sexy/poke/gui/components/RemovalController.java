package sexy.poke.gui.components;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXScrollPane;
import io.datafx.controller.FXMLController;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.layout.StackPane;
import sexy.poke.Main;
import sexy.poke.Settings;

import javax.annotation.PostConstruct;

import static sexy.poke.Settings.RemoveState.*;

/**
 * Created by Flo on 14.08.2018
 */

@FXMLController(value = "/RemovalGui.fxml")
public class RemovalController {
    public JFXCheckBox sourceFile;
    public JFXComboBox sourceFileCB;
    public JFXCheckBox sourceDebug;
    public JFXComboBox sourceDebugCB;
    public JFXCheckBox lineNumberTable;
    public JFXComboBox lineNumberTableCB;
    public JFXCheckBox localVariables;
    public JFXComboBox localVariablesCB;
    public JFXCheckBox cLRemoval;
    public JFXComboBox cLRemovalCB;
    //public JFXCheckBox localVariablesSignatures;
    //public JFXComboBox localVariablesSignaturesCB;
    public JFXCheckBox invisibleAnnotations;
    public JFXCheckBox visibleAnnotations;
    public JFXCheckBox signatures;
    //public JFXCheckBox deprecation;

    public JFXCheckBox innerClasses;
    public JFXCheckBox outerClass;
    public JFXCheckBox outerMethods;

    @FXML
    JFXScrollPane scrollPane;

    @PostConstruct
    public void init() {
        StackPane pane = (StackPane)scrollPane.getChildren().get(1);
        scrollPane.setPadding(new Insets(-pane.getPrefHeight(),0,0,0));

        sourceFile.setSelected(Main.settings.sourceFileRemoval != Settings.RemoveState.OFF);
        sourceDebug.setSelected(Main.settings.sourceDebugRemoval != Settings.RemoveState.OFF);
        lineNumberTable.setSelected(Main.settings.lineNumberTableRemoval != Settings.RemoveState.OFF);
        localVariables.setSelected(Main.settings.localVariablesRemoval != Settings.RemoveState.OFF);
        cLRemoval.setSelected(Main.settings.clIdsRemoval != Settings.RemoveState.OFF);
        //localVariablesSignatures.setSelected(Main.settings.localVariablesSignaturesRemoval != Settings.RemoveState.OFF);
        //localVariablesSignatures.setSelected(Main.settings.localVariablesSignaturesRemoval != Settings.RemoveState.OFF);


        sourceFileCB.setDisable(Main.settings.sourceFileRemoval == OFF);
        sourceDebugCB.setDisable(Main.settings.sourceDebugRemoval == OFF);
        lineNumberTableCB.setDisable(Main.settings.lineNumberTableRemoval == OFF);
        localVariablesCB.setDisable(Main.settings.localVariablesRemoval == OFF);
        cLRemovalCB.setDisable(Main.settings.clIdsRemoval == OFF);
        //localVariablesSignaturesCB.setDisable(Main.settings.localVariablesSignaturesRemoval == OFF);

        sourceFile.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.sourceFileRemoval =  newValue ? REMOVE  : OFF;
            sourceFileCB.setDisable(!newValue);
            sourceFileCB.getSelectionModel().select(newValue ? 0 : -1);
        });
        sourceDebug.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.sourceDebugRemoval =  newValue ? REMOVE : OFF;
            sourceDebugCB.setDisable(!newValue);
            sourceDebugCB.getSelectionModel().select(newValue ? 0 : -1);
        });

        lineNumberTable.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.lineNumberTableRemoval =  newValue ? REMOVE : OFF;
            lineNumberTableCB.setDisable(!newValue);
            lineNumberTableCB.getSelectionModel().select(newValue ? 0 : -1);
        });
        localVariables.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.localVariablesRemoval =  newValue ? REMOVE : OFF;
            localVariablesCB.setDisable(!newValue);
            localVariablesCB.getSelectionModel().select(newValue ? 0 : -1);
        });
        cLRemoval.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.clIdsRemoval =  newValue ? REMOVE : OFF;
            cLRemovalCB.setDisable(!newValue);
            cLRemovalCB.getSelectionModel().select(newValue ? 0 : -1);
        });
        /*localVariablesSignatures.selectedProperty().addListener((observable, oldValue, newValue) -> {
            Main.settings.localVariablesSignaturesRemoval =  newValue ? REMOVE : OFF;
            localVariablesSignaturesCB.setDisable(!newValue);
            localVariablesSignaturesCB.getSelectionModel().select(newValue ? 0 : -1);
        });*/

        invisibleAnnotations.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.invisibleAnnotationsRemoval =  newValue ? REMOVE : OFF);
        visibleAnnotations.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.visibleAnnotationsRemoval =  newValue ? REMOVE : OFF);
        //deprecation.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.deprecationRemoval =  newValue ? REMOVE : OFF);
        signatures.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.signatureRemoval =  newValue ? REMOVE : OFF);

        innerClasses.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.innerClassesRemoval =  newValue ? REMOVE : OFF);
        outerClass.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.outerClassesRemoval =  newValue ? REMOVE : OFF);
        outerMethods.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.outerMethodsRemoval =  newValue ? REMOVE : OFF);

        invisibleAnnotations.setSelected(Main.settings.invisibleAnnotationsRemoval != Settings.RemoveState.OFF);
        visibleAnnotations.setSelected(Main.settings.visibleAnnotationsRemoval != Settings.RemoveState.OFF);
        //deprecation.setSelected(Main.settings.deprecationRemoval != Settings.RemoveState.OFF);
        signatures.setSelected(Main.settings.signatureRemoval != Settings.RemoveState.OFF);

        innerClasses.setSelected(Main.settings.innerClassesRemoval != Settings.RemoveState.OFF);
        outerClass.setSelected(Main.settings.outerClassesRemoval != Settings.RemoveState.OFF);
        outerMethods.setSelected(Main.settings.outerMethodsRemoval != Settings.RemoveState.OFF);

        sourceFileCB.getSelectionModel().select(Main.settings.sourceFileRemoval.getId());
        sourceDebugCB.getSelectionModel().select(Main.settings.sourceDebugRemoval.getId());
        lineNumberTableCB.getSelectionModel().select(Main.settings.lineNumberTableRemoval.getId());
        localVariablesCB.getSelectionModel().select(Main.settings.localVariablesRemoval.getId());
        cLRemovalCB.getSelectionModel().select(Main.settings.clIdsRemoval.getId());
        //localVariablesSignaturesCB.getSelectionModel().select(Main.settings.localVariablesSignaturesRemoval.getId());

        sourceFileCB.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.sourceFileRemoval =  Settings.RemoveState.values()[sourceFileCB.getSelectionModel().getSelectedIndex() + 1]);
        sourceDebugCB.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.sourceDebugRemoval =  Settings.RemoveState.values()[sourceDebugCB.getSelectionModel().getSelectedIndex() + 1]);
        lineNumberTableCB.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.lineNumberTableRemoval =  Settings.RemoveState.values()[lineNumberTableCB.getSelectionModel().getSelectedIndex() + 1]);
        localVariablesCB.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.localVariablesRemoval =  Settings.RemoveState.values()[localVariablesCB.getSelectionModel().getSelectedIndex() + 1]);
        cLRemovalCB.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.clIdsRemoval =  Settings.RemoveState.values()[cLRemovalCB.getSelectionModel().getSelectedIndex() + 1]);
        //localVariablesSignaturesCB.valueProperty().addListener((observable, oldValue, newValue) -> Main.settings.localVariablesSignaturesRemoval =  Settings.RemoveState.values()[localVariablesSignaturesCB.getSelectionModel().getSelectedIndex() + 1]);
    }
}
