package sexy.poke.gui.components;

import com.jfoenix.controls.JFXCheckBox;
import io.datafx.controller.FXMLController;
import sexy.poke.Main;

import javax.annotation.PostConstruct;

/**
 * Created by Flo on 14.08.2018
 */
//langsame animations meme x d ez fix
@FXMLController(value = "/Shuffle.fxml")
public class ShuffleController {
    public JFXCheckBox shuffleFields;
    public JFXCheckBox shuffleMethods;

    @PostConstruct
    public void init() {
        shuffleFields.setSelected(Main.settings.shuffleFields);
        shuffleMethods.setSelected(Main.settings.shuffleMethods);

        shuffleFields.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.shuffleFields = newValue);
        shuffleMethods.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.shuffleMethods = newValue);
    }
}
