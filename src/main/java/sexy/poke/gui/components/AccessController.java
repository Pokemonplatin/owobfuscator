package sexy.poke.gui.components;

import com.jfoenix.controls.JFXCheckBox;
import io.datafx.controller.FXMLController;
import sexy.poke.Main;

import javax.annotation.PostConstruct;

/**
 * Created by Flo on 15.08.2018
 */
@FXMLController(value = "/Access.fxml")
public class AccessController {
    public JFXCheckBox syntClasses;
    public JFXCheckBox syntFields;
    public JFXCheckBox syntMethods;

    public JFXCheckBox publicClasses;
    public JFXCheckBox publicFields;
    public JFXCheckBox publicMethods;

    @PostConstruct
    public void init() {
        syntClasses.setSelected(Main.settings.syntheticClasses);
        syntFields.setSelected(Main.settings.syntheticFields);
        syntMethods.setSelected(Main.settings.syntheticMethods);
        publicClasses.setSelected(Main.settings.publicClasses);
        publicFields.setSelected(Main.settings.publicFields);
        publicMethods.setSelected(Main.settings.publicMethods);

        syntClasses.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.syntheticClasses = newValue);
        syntFields.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.syntheticFields = newValue);
        syntMethods.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.syntheticMethods = newValue);

        publicClasses.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.publicClasses = newValue);
        publicFields.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.publicFields = newValue);
        publicMethods.selectedProperty().addListener((observable, oldValue, newValue) -> Main.settings.publicMethods = newValue);
    }
}
